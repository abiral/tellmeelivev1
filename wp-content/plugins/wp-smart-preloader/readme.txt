=== WP Smart Preloader ===
Contributors: ashokmhrj, subedimadhu
Tags: WP Smart Preloader, Site preloader, Wordpress Preloader,posts, preload, preloader, responsive, plugin, preloader
Requires at least: 2.8
Tested up to: 4.4
Stable tag: 1.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A Plugin to add awesome collection of Loaders and Spinners. Delightful and performance-focused Pure CSS animations. 

== Description ==

WP Smart Preloader is a Simple CSS spinners and throbbers made with CSS and minimal HTML markup. It offers visual feedback in the event of content being loaded, thereby managing expectations and reducing the chance of a user abandoning your wordpress website.

You will find following features with this plugin:

* Full Responsive with Cross Broser compatible
* CSS only Preloader no Gif no Image
* Full Customize.
* Faster Loading
* Icon to circle
* No coding necessary. Activate it and play with its settings and you're Good to go


== Installation ==

1. Download and activate the plugin
2. Go to `settings` => `WP Smart Preloader`
3. Choose the Options of your choice
4. Click `Save changes` button.

Or

1. Put the plug-in folder `WP Smart Preloader` into [wordpress_dir]/wp-content/plugins/
2. Go into the WordPress admin interface and activate the plugin
3. Go to `settings` => `WP Smart Preloader`
4. Choose the Options of your choice
5. Click `Save changes` button.

Have fun!!!

== Frequently Asked Questions ==

= What does this plugin do? =

* WP Smart Preloader takes a simple, extendable approach to display Simple CSS spinners and throbbers onto your website

= Still have some questions ? =

Please use support forum or you can directly mail us at
[asokmhrj@gmail.com](mailto:asokmhrj@gmail.com) or [subedimadhukar@gmail.com](mailto:subedimadhukar@gmail.com)

== Screenshots ==
1. 1 **WP Smart Preloader Backend Settings**


== Changelog ==
= 1.5 =
 fixed showing preloader on page loading

= 1.4 =
 fixed js to load

= 1.3 =
 fixed css if only home page is selected (checked)

= 1.2 =
 fixed showing page before preloader

= 1.1 =
 Added function to delete setting after uninstall/deleting plugin

= 1.0 =
Initial version