jQuery(document).ready(function($){

	var loader_image = $('#loader-img'),
		preview_image = $('#loader-preview');
	
	wsp_init();

	loader_image.on('change',function(){
		wsp_init();	
	});

	function wsp_init(){
		var value =loader_image.val(),
			block ='<div class="smart-page-loader">';

		if( value != "" ){
				if(value == "Loader 1") {
					block += '<div class="wp-smart-loader smart-loader-one">Loading...</div>';
				} else if(value == "Loader 2") {
					block += '<div class="wp-smart-loader smart-loader-two"> <span></span> <span></span> <span></span> <span></span> </div>';
				} else if(value == "Loader 3") {
					block += ' <div class="wp-smart-loader smart-loader-three"> <span></span> <span></span> <span></span> <span></span> <span></span> </div>';
				} else if(value == "Loader 4") {
					block += ' <div class="wp-smart-loader smart-loader-four"> <span class="spinner-cube spinner-cube1"></span> <span class="spinner-cube spinner-cube2"></span> <span class="spinner-cube spinner-cube3"></span> <span class="spinner-cube spinner-cube4"></span> <span class="spinner-cube spinner-cube5"></span> <span class="spinner-cube spinner-cube6"></span> <span class="spinner-cube spinner-cube7"></span> <span class="spinner-cube spinner-cube8"></span> <span class="spinner-cube spinner-cube9"></span> </div>';
				} else if(value == "Loader 5") {
					block += ' <div class="wp-smart-loader smart-loader-five"> <span class="spinner-cube-1 spinner-cube"></span> <span class="spinner-cube-2 spinner-cube"></span> <span class="spinner-cube-4 spinner-cube"></span> <span class="spinner-cube-3 spinner-cube"></span> </div>';
				} else if(value == "Loader 6") {
					block += ' <div class="wp-smart-loader smart-loader-six"> <span class=" spinner-cube-1 spinner-cube"></span> <span class=" spinner-cube-2 spinner-cube"></span> </div>';
				} 
				block += "</div>";
				preview_image.html(block);
		}
	}
});