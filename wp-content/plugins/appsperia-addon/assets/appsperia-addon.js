(function() {
    tinymce.PluginManager.add('flask_tc_button', function( editor, url ) {
        editor.addButton( 'flask_tc_button', {
            title: 'Appsperia',
            type: 'menubutton',
            icon: 'wp_code',
            menu: [
                {
                    text: 'Add Button',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Add Button',
                            body: [
                            {
                                type: 'textbox',
                                name: 'title',
                                label: 'Title'
                            },
                            {
                                type: 'textbox',
                                name: 'url',
                                label: 'Url'
                            },
                            {
                                type: 'textbox',
                                name: 'icon',
                                label: 'Icon',
                                id: 'icon-selector',
                            },{
                                type: 'listbox',
                                name: 'target',
                                label: 'Open in',
                                'values': [
                                    {text: 'Same window',  value: '_self'},
                                    {text: 'New window',   value: '_blank'}
                                    ]
                            }                   
                            ],
                            onsubmit: function( e ) {
                                editor.insertContent( '[appsperia_button url="' + e.data.url + '" target="' + e.data.target + '" icon="' + e.data.icon + '" content="' + e.data.title + '"]');
                            }
                        });
                    }
                },
                {
                    text: 'Add Pricing Package',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Add Pricing Package',
                            body: [
                            {   type: 'textbox',
                                name: 'title',
                                label: 'Title'
                            },
                            {
                                type: 'listbox',
                                name: 'currency',
                                label: 'Currency',
                                'values': [
                                    {text: '$', value: '&dollar;'},
                                    {text: '€', value: '&euro;'}
                                ]
                            },
                            {
                                type: 'textbox',
                                name: 'price',
                                label: 'Price'
                            },
                            {
                                type: 'textbox',
                                name: 'url',
                                label: 'Url'
                            },
                            {
                                type: 'listbox',
                                name: 'target',
                                label: 'Open in',
                                'values': [
                                    {text: 'Same window',  value: '_self'},
                                    {text: 'New window',   value: '_blank'}
                                    ]
                            },
                            {
                                type: 'textbox',
                                name: 'button_title',
                                label: 'Button title'
                            },
                            {
                                type: 'textbox',
                                name: 'content',
                                label: 'Content (Separate with commas: feature1, feature2, ...)',
                                multiline: true,
                                minWidth: 500,
                                minHeight: 200
                            }
                            ],
                            onsubmit: function( e ) {
                                editor.insertContent( '[appsperia_pricing title="' + e.data.title + '" currency="' + e.data.currency + '" price="' + e.data.price + '" button_url="' + e.data.url + '" button_target="' + e.data.target + '" button_title="' + e.data.button_title + '" ]' + e.data.content + '[/appsperia_pricing]');
                            }
                        })
                    }
                },
                {
                    text: 'Add Tooltip',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Add Tooltip',
                            body: [
                            {
                                type: 'textbox',
                                name: 'title',
                                label: 'Title'
                            },
                            {
                                type: 'textbox',
                                name: 'icon',
                                label: 'Icon',
                                id: 'icon-selector',
                            },
                            {
                                type: 'textbox',
                                name: 'content',
                                label: 'Content',
                                multiline: true,
                                minWidth: 300,
                                minHeight: 100
                            }],
                            onsubmit: function( e ) {
                                editor.insertContent( '[appsperia_tooltip title="' + e.data.title + '" icon="' + e.data.icon + '" content="' + e.data.content + '"]');
                            }
                        });
                    }
                },
                {
                    text: 'Add Clients',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Add Clients',
                            body: [
                            {
                                type: 'textbox',
                                name: 'name',
                                label: 'Name'
                            },
                            {
                                type: 'textbox',
                                name: 'position',
                                label: 'Position',
                            },
                            {
                                type: 'textbox',
                                name: 'content',
                                label: 'Content',
                                multiline: true,
                                minWidth: 300,
                                minHeight: 100
                            },
                            {
                                type: 'textbox',
                                name: 'rating',
                                label: 'Rating ( 1 to 5 )',
                            }],
                            onsubmit: function( e ) {
                                editor.insertContent( '[appsperia_slide name="' + e.data.name + '" position="' + e.data.position + '" rating="' + e.data.rating + '" content="' + e.data.content + '"]');
                            }
                        });
                    }
                },
                {
                    text: 'Tabs',
                    menu: [
                    {
                        text: 'Add Tabs Group',
                        onclick: function() {
                            editor.insertContent('[appsperia_tabs] [/appsperia_tabs]');
                        }
                    },
                    {
                        text: 'Add Tab',
                        onclick: function() {
                            editor.windowManager.open( {
                                title: 'Add Tab',
                                body: [{
                                    type: 'textbox',
                                    name: 'title',
                                    label: 'Title'
                                },
                                {
                                type: 'textbox',
                                name: 'icon',
                                label: 'Icon',
                                id: 'icon-selector',
                                },
                                {
                                    type: 'textbox',
                                    name: 'content',
                                    label: 'Content',
                                    multiline: true,
                                    minWidth: 500,
                                    minHeight: 200
                                }],
                                onsubmit: function( e ) {
                                    editor.insertContent( '[appsperia_tab title="' + e.data.title + '" icon="' + e.data.icon + '" ]' + e.data.content + '[/appsperia_tab]');
                                }
                            });
                        }
                    }
                    ]
                }
           ]
        });
    });
})();


  jQuery(function($){

    $(document).on("click","#icon-selector",function() {  
 
    $('#icon-selector').parent().append("<div class='select-icon' style='position: fixed;float: left;width: 417px; overflow-y:scroll; margin-left: -200px;background: #fff;border: 1px solid #ccc;padding: 20px;left: 50%;height: 300px;z-index: 999;top: 50%;margin-top: -150px;'><span class='icon flaticon-2x2'></span><span class='icon flaticon-3x3'></span><span class='icon flaticon-adjust2'></span><span class='icon flaticon-adjust3'></span><span class='icon flaticon-archive5'></span><span class='icon flaticon-arrow100'></span><span class='icon flaticon-arrow103'></span><span class='icon flaticon-arrow105'></span><span class='icon flaticon-arrow106'></span><span class='icon flaticon-arrow61'></span><span class='icon flaticon-arrow68'></span><span class='icon flaticon-arrow69'></span><span class='icon flaticon-arrow70'></span><span class='icon flaticon-arrow71'></span><span class='icon flaticon-arrow72'></span><span class='icon flaticon-arrow74'></span><span class='icon flaticon-arrow75'></span><span class='icon flaticon-arrow76'></span><span class='icon flaticon-arrow83'></span><span class='icon flaticon-arrow84'></span><span class='icon flaticon-arrow85'></span><span class='icon flaticon-arrow86'></span><span class='icon flaticon-arrow87'></span><span class='icon flaticon-arrow88'></span><span class='icon flaticon-arrow89'></span><span class='icon flaticon-arrow90'></span><span class='icon flaticon-arrow91'></span><span class='icon flaticon-arrow93'></span><span class='icon flaticon-arrow94'></span><span class='icon flaticon-arrow96'></span><span class='icon flaticon-arrow97'></span><span class='icon flaticon-arrow98'></span><span class='icon flaticon-arrow99'></span><span class='icon flaticon-arrows6'></span><span class='icon flaticon-black33'></span><span class='icon flaticon-black64'></span><span class='icon flaticon-black66'></span><span class='icon flaticon-black67'></span><span class='icon flaticon-black70'></span><span class='icon flaticon-black73'></span><span class='icon flaticon-blocked'></span><span class='icon flaticon-blogger1'></span><span class='icon flaticon-blogger2'></span><span class='icon flaticon-book23'></span><span class='icon flaticon-box9'></span><span class='icon flaticon-camera19'></span><span class='icon flaticon-chat3'></span><span class='icon flaticon-check2'></span><span class='icon flaticon-chevron9'></span><span class='icon flaticon-circle10'></span><span class='icon flaticon-circle11'></span><span class='icon flaticon-circle8'></span><span class='icon flaticon-circular3'></span><span class='icon flaticon-circular4'></span><span class='icon flaticon-circular6'></span><span class='icon flaticon-circular7'></span><span class='icon flaticon-circular8'></span><span class='icon flaticon-circular9'></span><span class='icon flaticon-close11'></span><span class='icon flaticon-close9'></span><span class='icon flaticon-cloud26'></span><span class='icon flaticon-cogs'></span><span class='icon flaticon-comment2'></span><span class='icon flaticon-compass8'></span><span class='icon flaticon-condense'></span><span class='icon flaticon-condensing'></span><span class='icon flaticon-cone1'></span><span class='icon flaticon-cone2'></span><span class='icon flaticon-contacts1'></span><span class='icon flaticon-cross8'></span><span class='icon flaticon-cursor7'></span><span class='icon flaticon-dark9'></span><span class='icon flaticon-delicious1'></span><span class='icon flaticon-desktop1'></span><span class='icon flaticon-document9'></span><span class='icon flaticon-documents2'></span><span class='icon flaticon-documents'></span><span class='icon flaticon-double4'></span><span class='icon flaticon-double5'></span><span class='icon flaticon-double7'></span><span class='icon flaticon-down10'></span><span class='icon flaticon-down11'></span><span class='icon flaticon-down4'></span><span class='icon flaticon-download11'></span><span class='icon flaticon-download7'></span><span class='icon flaticon-drawer1'></span><span class='icon flaticon-dribble'></span><span class='icon flaticon-earphones1'></span><span class='icon flaticon-empty10'></span><span class='icon flaticon-error2'></span><span class='icon flaticon-error3'></span><span class='icon flaticon-error4'></span><span class='icon flaticon-error5'></span><span class='icon flaticon-error6'></span><span class='icon flaticon-exclamation1'></span><span class='icon flaticon-expand9'></span><span class='icon flaticon-facebook6'></span><span class='icon flaticon-facebook7'></span><span class='icon flaticon-film9'></span><span class='icon flaticon-fivepointed'></span><span class='icon flaticon-flickr1'></span><span class='icon flaticon-flickr4'></span><span class='icon flaticon-folder22'></span><span class='icon flaticon-full9'></span><span class='icon flaticon-genius'></span><span class='icon flaticon-google15'></span><span class='icon flaticon-google16'></span><span class='icon flaticon-group2'></span><span class='icon flaticon-half3'></span><span class='icon flaticon-heart19'></span><span class='icon flaticon-high7'></span><span class='icon flaticon-house3'></span><span class='icon flaticon-house4'></span><span class='icon flaticon-icon2'></span><span class='icon flaticon-info9'></span><span class='icon flaticon-interface19'></span><span class='icon flaticon-key9'></span><span class='icon flaticon-laptop3'></span><span class='icon flaticon-left12'></span><span class='icon flaticon-lightbulb'></span><span class='icon flaticon-linkedin2'></span><span class='icon flaticon-linkedin5'></span><span class='icon flaticon-links1'></span><span class='icon flaticon-links2'></span><span class='icon flaticon-list14'></span><span class='icon flaticon-little10'></span><span class='icon flaticon-little11'></span><span class='icon flaticon-little12'></span><span class='icon flaticon-little13'></span><span class='icon flaticon-little14'></span><span class='icon flaticon-little16'></span><span class='icon flaticon-little17'></span><span class='icon flaticon-little18'></span><span class='icon flaticon-little19'></span><span class='icon flaticon-little20'></span><span class='icon flaticon-little21'></span><span class='icon flaticon-little22'></span><span class='icon flaticon-little23'></span><span class='icon flaticon-little26'></span><span class='icon flaticon-little27'></span><span class='icon flaticon-little3'></span><span class='icon flaticon-little4'></span><span class='icon flaticon-little5'></span><span class='icon flaticon-little6'></span><span class='icon flaticon-little7'></span><span class='icon flaticon-little9'></span><span class='icon flaticon-lock11'></span><span class='icon flaticon-lock12'></span><span class='icon flaticon-lock7'></span><span class='icon flaticon-logo3'></span><span class='icon flaticon-logo4'></span><span class='icon flaticon-low6'></span><span class='icon flaticon-mail9'></span><span class='icon flaticon-map5'></span><span class='icon flaticon-map7'></span><span class='icon flaticon-menu7'></span><span class='icon flaticon-menu8'></span><span class='icon flaticon-menu9'></span><span class='icon flaticon-microphone9'></span><span class='icon flaticon-mic'></span><span class='icon flaticon-mini10'></span><span class='icon flaticon-mini11'></span><span class='icon flaticon-mini12'></span><span class='icon flaticon-mini1'></span><span class='icon flaticon-mini2'></span><span class='icon flaticon-mini3'></span><span class='icon flaticon-mini4'></span><span class='icon flaticon-mini5'></span><span class='icon flaticon-mini6'></span><span class='icon flaticon-mini7'></span><span class='icon flaticon-mini8'></span><span class='icon flaticon-mini9'></span><span class='icon flaticon-mini'></span><span class='icon flaticon-minus11'></span><span class='icon flaticon-minus6'></span><span class='icon flaticon-move8'></span><span class='icon flaticon-mute2'></span><span class='icon flaticon-mute4'></span><span class='icon flaticon-myspace1'></span><span class='icon flaticon-myspace2'></span><span class='icon flaticon-myspace3'></span><span class='icon flaticon-new6'></span><span class='icon flaticon-outlined3'></span><span class='icon flaticon-outlined4'></span><span class='icon flaticon-paperclip3'></span><span class='icon flaticon-pause5'></span><span class='icon flaticon-pencil10'></span><span class='icon flaticon-pencil12'></span><span class='icon flaticon-pencil9'></span><span class='icon flaticon-picassa2'></span><span class='icon flaticon-picassa'></span><span class='icon flaticon-pinterest6'></span><span class='icon flaticon-pinterest8'></span><span class='icon flaticon-plus7'></span><span class='icon flaticon-present1'></span><span class='icon flaticon-profile'></span><span class='icon flaticon-prohibited1'></span><span class='icon flaticon-pushpin1'></span><span class='icon flaticon-question3'></span><span class='icon flaticon-quotations1'></span><span class='icon flaticon-quotations2'></span><span class='icon flaticon-quotations'></span><span class='icon flaticon-refresh7'></span><span class='icon flaticon-right17'></span><span class='icon flaticon-screen'></span><span class='icon flaticon-screwdriver3'></span><span class='icon flaticon-search7'></span><span class='icon flaticon-selected'></span><span class='icon flaticon-share16'></span><span class='icon flaticon-share6'></span><span class='icon flaticon-shopping9'></span><span class='icon flaticon-simple30'></span><span class='icon flaticon-simple31'></span><span class='icon flaticon-single'></span><span class='icon flaticon-small62'></span><span class='icon flaticon-small63'></span><span class='icon flaticon-small64'></span><span class='icon flaticon-small65'></span><span class='icon flaticon-small66'></span><span class='icon flaticon-small67'></span><span class='icon flaticon-small68'></span><span class='icon flaticon-small69'></span><span class='icon flaticon-small70'></span><span class='icon flaticon-small71'></span><span class='icon flaticon-small72'></span><span class='icon flaticon-small73'></span><span class='icon flaticon-small74'></span><span class='icon flaticon-small75'></span><span class='icon flaticon-small77'></span><span class='icon flaticon-social10'></span><span class='icon flaticon-social11'></span><span class='icon flaticon-social16'></span><span class='icon flaticon-social17'></span><span class='icon flaticon-social18'></span><span class='icon flaticon-social19'></span><span class='icon flaticon-social21'></span><span class='icon flaticon-social22'></span><span class='icon flaticon-social23'></span><span class='icon flaticon-social34'></span><span class='icon flaticon-social38'></span><span class='icon flaticon-social39'></span><span class='icon flaticon-social40'></span><span class='icon flaticon-social41'></span><span class='icon flaticon-social4'></span><span class='icon flaticon-social5'></span><span class='icon flaticon-social67'></span><span class='icon flaticon-social68'></span><span class='icon flaticon-social69'></span><span class='icon flaticon-social6'></span><span class='icon flaticon-social70'></span><span class='icon flaticon-social71'></span><span class='icon flaticon-social76'></span><span class='icon flaticon-social77'></span><span class='icon flaticon-social78'></span><span class='icon flaticon-social7'></span><span class='icon flaticon-social8'></span><span class='icon flaticon-social92'></span><span class='icon flaticon-social9'></span><span class='icon flaticon-speech18'></span><span class='icon flaticon-spotify1'></span><span class='icon flaticon-spotify2'></span><span class='icon flaticon-square18'></span><span class='icon flaticon-stop5'></span><span class='icon flaticon-stop8'></span><span class='icon flaticon-stumbleupon3'></span><span class='icon flaticon-stumbleupon5'></span><span class='icon flaticon-tablet3'></span><span class='icon flaticon-tag10'></span><span class='icon flaticon-tags'></span><span class='icon flaticon-target'></span><span class='icon flaticon-telephone1'></span><span class='icon flaticon-thin3'></span><span class='icon flaticon-thin4'></span><span class='icon flaticon-thin5'></span><span class='icon flaticon-thin7'></span><span class='icon flaticon-thin8'></span><span class='icon flaticon-toolbox'></span><span class='icon flaticon-trash8'></span><span class='icon flaticon-triangular1'></span><span class='icon flaticon-tumblr4'></span><span class='icon flaticon-tumblr6'></span><span class='icon flaticon-tumblr7'></span><span class='icon flaticon-two23'></span><span class='icon flaticon-two24'></span><span class='icon flaticon-up11'></span><span class='icon flaticon-vimeo2'></span><span class='icon flaticon-volume10'></span><span class='icon flaticon-volume11'></span><span class='icon flaticon-wheel1'></span><span class='icon flaticon-wordpress2'></span><span class='icon flaticon-youtube4'></span><span class='icon flaticon-zoom14'></span><span class='icon flaticon-zoom16'></span><span class='icon flaticon-zoom17'></span><span class='icon flaticon-appstore'></span><span class='icon flaticon-playstore'></span></div>");

    });


    $(document).on("click",".select-icon span",function() { 

    iconselected = ($(this).attr("class"));
    iconselected = iconselected.replace("icon flaticon","flaticon");
    $('#icon-selector').val(iconselected);
    $('.select-icon').remove();   

    });


    
  


  });
    

