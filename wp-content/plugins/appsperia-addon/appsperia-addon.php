<?php
/**
 * Plugin Name: AppSperia Addon
 * Plugin URI: 
 * Description: Content elements extender for AppSperia Theme
 * Version: 1.0.0
 * Author: Key-Design
 * Author URI: http://themeforest.net/user/Key-Design
 * License: GNU General Public License
 */

add_action('admin_head', 'themetek_shortcodes');

function themetek_shortcodes() {
    global $typenow;
    // check user permissions
    if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
    return;
    }
    // verify the post type
    if( ! in_array( $typenow, array( 'post', 'page' ) ) )
        return;
  // check if WYSIWYG is enabled
  if ( get_user_option('rich_editing') == 'true') {
    add_filter("mce_external_plugins", "themetek_add_tinymce_plugin");
    add_filter('mce_buttons', 'themetek_register_my_tc_button');
  }
}

function themetek_add_tinymce_plugin($plugin_array) {
    $plugin_array['flask_tc_button'] = plugin_dir_url(__FILE__) . 'assets/appsperia-addon.js';
    return $plugin_array;
}

function themetek_register_my_tc_button($buttons) {
   array_push($buttons, "flask_tc_button");
   return $buttons;
}

function themetek_tc_css() {
  wp_enqueue_style('flask-tc', plugin_dir_url(__FILE__) . 'assets/appsperia-addon.css');
}

add_action('admin_enqueue_scripts', 'themetek_tc_css');


// ------------------------------------------------------------------------
// Button ShortCode
// -----------

function appsperia_button( $atts) {
  $btn = shortcode_atts( array(
        'url'	=> 'url',
        'target' => 'target',
        'icon' => 'icon',
        'content' => 'content'
    ), $atts );
  return "<div class='button-wrap'><a href='{$btn['url']}' class='simple-button' target='{$btn['target']}'> <span class='icon {$btn['icon']}'></span> {$btn['content']}</a></div>";
}
add_shortcode( 'appsperia_button', 'appsperia_button' );


// ------------------------------------------------------------------------
// Pricing Table Shortcode
// -----------

function appsperia_pricing( $atts, $content = null) {
	$prc = shortcode_atts( array(
		'title' => 'title',
		'currency' => 'currency',
		'price'		=> 'price',
		'button_title'	=> 'button_title',
		'button_target'	=> 'button_target',
		'button_url'	=> 'button_url'
		), $atts );
	$content = esc_textarea( trim( strip_tags( $content ) ) );
	$tds = explode(',', $content);
  	$result = '';
  	foreach ($tds as $td) {
	    $result .= '<li><span class="icon flaticon-circle10"> </span>'.$td.'</li>';
  	}
	return "
		<div class='price-table normal'>
	        <h3 class='package'>{$prc['title']}</h3>
	        <div class='price'> <span class='currency'>{$prc['currency']}</span> <span class='amount'>{$prc['price']}</span> </div>
	        <ul class='specifications'>
	            ".$result."
	        </ul>                      
	        <div class='pricing-button'><a href='{$prc['button_url']}' target='{$prc['button_target']}'>{$prc['button_title']}</a>
	        </div>
	    </div>
	";
}
add_shortcode( 'appsperia_pricing', 'appsperia_pricing' );

// ------------------------------------------------------------------------
// Tooltip ShortCode
// -----------

function appsperia_tooltip( $atts) {
  $atts = shortcode_atts( array(
        'title'	=> 'title',
        'icon'	=> 'icon',
        'content'	=> 'content'
    ), $atts );
  return "<div class='tooltip-wrap'><a class='tooltip' href='#'><span class='icon {$atts['icon']}'></span> {$atts['title']} <span class='tooltip-content'><span class='tooltip-text'><span class='tooltip-inner'><span class='icon {$atts['icon']}'></span><span class='tooltip-body'>{$atts['content']}</span></span></span></span></a></div>";
}
add_shortcode( 'appsperia_tooltip', 'appsperia_tooltip' );


// ------------------------------------------------------------------------
// Tabs ShortCode
// -----------


function appsperia_tabs( $atts, $content )
{
    $GLOBALS['tab_count'] = 0;

    do_shortcode( $content );

    $rand = rand();

    if( is_array( $GLOBALS['tabs'] ) ){
        foreach( $GLOBALS['tabs'] as $k=>$tab ){
        	if ($k==0) {
            	$tabs[] = '<li class="tab-current"><a href="#section-linemove-'.$k.'"><span class="icon ' . $tab['icon'] .'"></span><span> '.$tab['title'].'</span></a></li>';
        	}else{
            	$tabs[] = '<li><a href="#section-linemove-'.$k.'"><span class="icon ' . $tab['icon'] .'"></span><span> '.$tab['title'].'</span></a></li>';
        	}
        	if ($k==0) {
            	$panes[] = '<section id="section-linemove-'.$k.'" class="content-current">'.$tab['content'].'</section>';
        	}else{
            	$panes[] = '<section id="section-linemove-'.$k.'">'.$tab['content'].'</section>';
        	}
        }
        $return = "\n".'<div class="tabs tabs-style-linemove"><nav><ul>'.implode( "\n", $tabs ).'</ul></nav><div class="content-wrap">'."\n".implode( "\n", $panes ).'</div></div>'."\n";
    }
    return $return;
}
add_shortcode( 'appsperia_tabs', 'appsperia_tabs' );


function appsperia_tab( $atts, $content )
{
    extract(shortcode_atts(array(
            'title' => 'Tab %d',
            'icon' => 'icon',
    ), $atts));

    $x = $GLOBALS['tab_count'];
    $GLOBALS['tabs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['tab_count']), 'icon' =>  $icon ,  'content' =>  $content );

    $GLOBALS['tab_count']++;
}
add_shortcode( 'appsperia_tab', 'appsperia_tab' );


// ------------------------------------------------------------------------
// Clients ShortCode
// -----------



function appsperia_slide( $atts) {
  $btn = shortcode_atts( array(
        'name'	=> 'name',
        'position' => 'position',
        'rating' => 'rating',
        'content' => 'content'
    ), $atts );

  $stars = '';

  if (  $atts['rating'] <= 5 ) {
  for ($i = 1; $i <= $atts['rating']; $i++) { $stars .= "<span class='icon flaticon-little27'></span>"; }
  for ($i = 1; $i <= 5 - $atts['rating']; $i++) { $stars .= "<span class='icon flaticon-fivepointed'></span>"; }
  }

  $return = "
		<div class='slide clients-content' data-anchor=''>
                            <p>{$atts['content']}</p>
                            <span class='client-name'> <strong> {$atts['name']} </strong> - {$atts['position']} </span>
						
                        <span class='client-stars'>
                        " . $stars ."
                        </span>
        </div>
	";
   return $return;
}
add_shortcode( 'appsperia_slide', 'appsperia_slide' );



// ------------------------------------------------------------------------
// Gallery ShortCode
// -----------


add_shortcode('gallery', 'my_gallery_shortcode');    

function my_gallery_shortcode($attr) {
    $post = get_post();

static $instance = 0;
$instance++;

if ( ! empty( $attr['ids'] ) ) {
    // 'ids' is explicitly ordered, unless you specify otherwise.
    if ( empty( $attr['orderby'] ) )
        $attr['orderby'] = 'post__in';
    $attr['include'] = $attr['ids'];
}

// Allow plugins/themes to override the default gallery template.
$output = apply_filters('post_gallery', '', $attr);
if ( $output != '' )
    return $output;

extract(shortcode_atts(array(
    'order'      => 'ASC',
    'orderby'    => 'menu_order ID',
    'id'         => $post->ID,
    'itemtag'    => 'div',
    'icontag'    => 'div',
    'captiontag' => 'dd',
    'columns'    => 3,
    'link'    => 'none',
    'size'       => 'thumbnail',
    'include'    => '',
    'exclude'    => ''
), $attr));



$id = intval($id);
if ( 'RAND' == $order )
    $orderby = 'none';

if ( !empty($include) ) {
    $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

    $attachments = array();
    foreach ( $_attachments as $key => $val ) {
        $attachments[$val->ID] = $_attachments[$key];
    }
} elseif ( !empty($exclude) ) {
    $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
} else {
    $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
}

if ( empty($attachments) )
    return '';

$columns = intval($columns);
$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
$float = is_rtl() ? 'right' : 'left';

$selector = "gallery-{$instance}";

$size_class = sanitize_html_class( $size );

$gallery_div = "<div class='screenshots-wrapper'>";
$output = apply_filters( 'gallery_style', "\n\t\t" . $gallery_div );
$i = 0;
foreach ( $attachments as $id => $attachment ) {
    $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);

  
    $output .= "
        <div class='slide'>
            $link
        </div>";
   
    $output = preg_replace(array('/<a[^>]*>/', '/<\/a>/'), '', $output);
  
}

$output .= "</div>";
return $output;
}
