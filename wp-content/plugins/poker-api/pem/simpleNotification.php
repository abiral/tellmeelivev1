<?php

// Put your device token here (without spaces):
// $deviceToken = 'EFD3C92D2AF582C40DFFBAFF5C8D05B09C3A19BBC6A6191144D9D90D29BAE998';  // neetin iPhone 6s

// $deviceToken = '94C9E44FFB02B76FFAC952E6917A1B52F9187C89310D38D65C15F9A1726A249A'; // 3C ipad mimi
$deviceToken = 'EC7731A19A671D6C1FA0CAF41EC05E394EB01FD33BCC6758AFA2FA6110AB8C3B'; // 3c iPad mini

// Put your private key's passphrase here:
$passphrase = '1234';

// Put your alert message here:
$message = 'This is just a test for Poker app. Message aayo vane malai inform gara hai - Neetin';

////////////////////////////////////////////////////////////////////////////////

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'production_com.pokermagazine.Poker-Magazine.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

//$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,
	// $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
	'alert' => $message,
	'sound' => 'default'
	);

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
	echo 'Message not delivered' . PHP_EOL;
else
	echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);
