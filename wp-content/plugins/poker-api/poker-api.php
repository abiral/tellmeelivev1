<?php
/**
 * Plugin Name: Poker API
 * Plugin URI: http://www.webexpertsnepal.com
 * Description: Poker API
 * Version: 1.0.0
 * Author: eVisionTeam
 * Author URI: http://www.webexpertsnepal.com
 * License: GPL2
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//  Define Constants
define('POKER_API_DIR', plugin_dir_path(__FILE__));
define('POKER_API_URI', plugin_dir_url(__FILE__));

define('POKER_API_DEFAULT_IMAGE_SIZE', "medium");

require_once POKER_API_DIR."controllers/user_devices.php";
require_once POKER_API_DIR."controllers/send_notification.php";
require_once POKER_API_DIR. "controllers/cron.php";

function poker_api_init() {
  
  	require_once (POKER_API_DIR."controllers/users.php");
  	require_once POKER_API_DIR."controllers/event.php";
  	require_once POKER_API_DIR."controllers/media-upload.php";
  	require_once POKER_API_DIR."controllers/spam.php";
  	
  	$POKER_API_MobileNotification = new POKER_API_MobileNotification();
  	
  	$POKER_API_Users = new POKER_API_Users();
  	add_filter( 'json_endpoints', array( $POKER_API_Users, 'register_routes' ) );
      
  	$POKER_API_Event = new POKER_API_Event();
  	add_filter( 'json_endpoints', array( $POKER_API_Event, 'register_routes' ) );
  	
  	// handling medai (upload image)
  	$POKER_API_media = new POKER_API_Media_Controller();
  	add_filter( 'json_endpoints', array( $POKER_API_media, 'register_routes' ) );
  
    // for register user devices
    $POKER_API_devices = new POKER_API_Devices();
    add_filter('json_endpoints', array( $POKER_API_devices, 'register_routes') );
    
    // for cron jobs api
    $POKER_API_CronJob = new POKER_API_CronJob();
    add_filter('json_endpoints', array( $POKER_API_CronJob, 'register_routes') );
    
    $POKER_API_Spam = new POKER_API_Spam();
    add_filter('json_endpoints', array( $POKER_API_Spam, 'register_routes') );
    
    //send notification while comment added
    add_action('wp_insert_comment','comment_inserted', 99, 2);
    function comment_inserted($comment_id, $comment_object) {
      /**
        1. When new posts from players is made
  
        $body[“new_post"] = [“player_id”=> “51”, “event_id”=> “253"]
        $message = “<Player1> has made new post in <Event_name>"
        
        2. New comments from visitors
        
        $body[“new_post"] = [“player_id”=> “0”, “event_id” => “253"]
        $message = “<Visitor name>or Guest has made new post in <Event_name>”
      **/
      
      /*
      $event_players = get_post_meta($comment_object->comment_post_ID, 'event_player',true);
      $current_comment_posted_user = $comment_object->user_id;
      
      if(($key = array_search($current_comment_posted_user, $event_players)) !== false) {
          unset($event_players[$key]);
      }
      */
    
      global $wpdb;
      $args = array(
  	 		  // 'exclude' => array(93),
  	 		  'meta_query' => array( array( 'key' => 'user_device_list') ),
  	 		  'fields' => array( 'ID' )
    	);
     		
    	if( $comment_object->user_id) 
    	  $args['exclude'] = array($comment_object->user_id);
     	  
  	 	$all_users = (array) get_users($args);
  	 //	file_put_contents(dirname(__FILE__)."/test5.txt", $players_list);
  	 	if ( !$all_users ) return ;
  	 	
  	 	$players_list = array();
  	 	foreach ($all_users as $user) { $players_list[] = $user->ID; }
      // file_put_contents(dirname(__FILE__)."/test5.txt", $players_list);
      $notification_message = ucfirst($comment_object->comment_author) . " have post in ".get_the_title( $comment_object->comment_post_ID );
      if(!$players_list) return ;
      
      insert_notification_data($players_list, $notification_message, $comment_object->comment_post_ID);
      
    }
    
    
    
    /**
     * Generate User AuthToken using hash
     **/

    function poker_generateHashWithSalt( $string ) {
 	
        $intermediateSalt = md5(uniqid(rand(), true));
        $salt = substr($intermediateSalt, 0, 6);
        return hash("sha256", $string . $salt);
    }
    add_action('user_register','poker_generate_auth_token');
    
    function poker_generate_auth_token($user_id){
     	update_user_meta( $user_id, 'poker_authToken', poker_generateHashWithSalt($user_id) );
    }

    
}
add_action( 'wp_json_server_before_serve', 'poker_api_init' );


function new_modify_user_table( $column ) {
  $column['authToken'] = 'authToken';
  return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
  switch ($column_name) {
    case 'authToken' :
    return get_user_meta($user_id, 'poker_authToken', true);
    break;
    default:
  }
  return $return;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );



/**
 * send notification while new event created 
 **/ 
add_action( 'save_post', 'previous_players_list' );
function previous_players_list( $post_id ) {
  $players = get_post_meta($post_id, 'event_player',true);
  global $previous_events_players ; 
  $previous_events_players = $players;
}

add_action( "updated_post_meta", 'events_updated_send_notification',10,4 );
function events_updated_send_notification($meta_id, $object_id, $meta_key, $_meta_value){
  if( $meta_key == 'event_player') {
    $post_title =   get_the_title($object_id);
    
    if (!$players) return;
    
    // compare and make diffrent users
    global $previous_events_players ; 
    $previous = $previous_events_players;
    // compare with current and previous return latest users 
    $players = array_diff($_meta_value, $previous); 
    
    $notification_message = "Hello Player, you have been invited to ".$post_title;
    
    insert_notification_data($players, $notification_message);
    return;
  }
}

/**
 * insert into notification table 
 **/ 
function insert_notification_data($players, $notification_message,$event_id=null ) {
  global $wpdb;
  $players_list = serialize($players);
  
  $sql =  "INSERT INTO  {$wpdb->prefix}notification_queue (`players`, `message`, `event_id`) VALUES ('{$players_list}', '{$notification_message}', {$event_id})";
  
  file_put_content( dirname(__FILE__).'/notification-query.txt', $sql );
  
  $wpdb->query( $sql );
}

/**
 * creating table for notification job queue
 **/ 
function notification_table_install() {
	global $wpdb;
	global $jal_db_version;

	$table_name = $wpdb->prefix . 'notification_queue';
	
	$charset_collate = $wpdb->get_charset_collate();
	
  $sql = "CREATE TABLE IF NOT EXISTS $table_name (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `players` text NOT NULL,
    `message` varchar(255) NOT NULL,
    `event_id` int(11) NULL,
    `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `send_time` timestamp NULL DEFAULT NULL,
    `sent` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
  ) $charset_collate;";
  

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

}
register_activation_hook( __FILE__, 'notification_table_install' );

/**
 * delete table while plugins uninstall 
 **/
function notification_table_drop() {
  global $wpdb;
  $wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}notification_queue" );
}
register_uninstall_hook( __FILE__, 'notification_table_drop' );