<?php
class POKER_API_CronJob {    
    public function register_routes( $routes ) {
		
		$routes['/poker-api/send-notification'] = array(
			array( array( $this, 'send_notification'), WP_JSON_Server::READABLE ),
		);
		
		$routes['/poker-api/update-pending-comments'] = array(
			array( array( $this, 'update_pending_comments'), WP_JSON_Server::READABLE ),
		);
		
		$routes['/poker-api/delete-like-dislike-file'] = array(
			array( array( $this, 'delete_like_dislike_file'), WP_JSON_Server::READABLE ),
		);
		
		return $routes;
	}
	
	/**
	 * send mobile notification by cron jobs 
	 **/
	 
	public function send_notification(){
        global $wpdb;
        $sql = "SELECT * FROM {$wpdb->prefix}notification_queue WHERE `sent` = 0 LIMIT 10";
        $results = $wpdb->get_results($sql, OBJECT);
        
        if (!$results) return ;
        
        
        foreach($results as $result){
            $playes = unserialize($result->players);
            $notification_message = $result->message;
            
            if ($result->event_id){
                $Notification = new POKER_API_MobileNotification( $playes, $notification_message,$result->event_id);
            }
            else{
                $Notification = new POKER_API_MobileNotification( $playes, $notification_message);
            }
            
            $Notification->send();
            
            // set flag to notification send
            $this->update_notification_table($result->id);
        }
        
        echo 'send '.count($result). ' notifications';
    }
    
    /**
     * udpate notification table 
     **/ 
    private function update_notification_table($id) {
        
        global $wpdb; 
        $sql = "UPDATE {$wpdb->prefix}notification_queue SET `sent` = 1 WHERE `id` = {$id}";
        $results = $wpdb->query($sql);
        return ;
    }
    
    
    function update_pending_comments(){
        $dir = ABSPATH.'wp-content/themes/appsperia-child/queue/pending/';
        $count = 0 ;
        $current_updated_comments = array();
        foreach (new DirectoryIterator($dir) as $file) {
          if ($file->isFile()) {
              $file_name = $file->getFilename();
              $file_path = $dir."/{$file_name}";
              $file = file_get_contents($file_path, true);
              $data = unserialize($file);
              $comment_id = wp_insert_comment( $data );
              
              $count++;
              
              if(!is_wp_error($comment_id)){
                  update_comment_meta( $comment_id, 'comment_image', $data['comment_image'] );
                  $current_updated_comments[] = $comment_id;
                  unlink($file_path);		
        	  }
          }
        }
        $args = array(
        	'comment__in' => $current_updated_comments
        );
        $current_comments = get_comments($args);
        $loop_count = 1;
        foreach($current_comments as $comment) {
            $json_data = array( 
              "comment_ID" => $comment->comment_ID,
              "comment_author" => $comment->comment_author,
              "user_avatar" => $this->get_user_avator_image_url($comment->user_id),
              "comment_content" => $comment->comment_content,
              "comment_date_gmt" => strtotime($comment->comment_date),
              "comment_image"   => get_comment_meta($comment->comment_ID, 'comment_image', true)
           );
           if($loop_count == 1) {
                file_put_contents($dir."../comments.txt", ", \n".json_encode($json_data) . ", \n", FILE_APPEND);    
            }
           elseif($loop_count == count($current_comments)){
                file_put_contents($dir."../comments.txt", json_encode($json_data) . " \n", FILE_APPEND);
           }
           else{
            file_put_contents($dir."../comments.txt", json_encode($json_data) . ", \n", FILE_APPEND);    
           }
           
           $loop_count++;
        }
        
        
        $msg = "{$count} files are updated on ". date('Y-m-d H:i:s');
        echo $msg;
        
    }
    
    protected function get_user_avator_image_url($id){
		$image_url = user_avatar_fetch_avatar( array( 'html'=>false, 'item_id' => $id, 'width' => '', 'height' => '', 'alt' => '' ) );
		if( $image_url ) return $image_url;
		return '';
	}

	/*
	 * delete comment like and dislike files
	 **/
	 
	 public function delete_like_dislike_file(){
	     $queue_dir = ABSPATH.'wp-content/themes/appsperia-child/queue/';
	     file_put_contents($queue_dir."likejson.txt", '');
	     echo 'sucess';
	 }
    
}
?>