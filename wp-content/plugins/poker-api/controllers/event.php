<?php
   set_time_limit(0);
class POKER_API_Event{
	
	public $validation;
	
	function __construct() {
    	require_once ('validation.php');
		$this->validation = new POKER_API_Validation();
   	}
	
	public function register_routes( $routes ){
		
		/**
		 * Post type events list with with pagination 
		 */
		$routes['/poker-api/events'] = array(
			array( array( $this, 'get_events'), WP_JSON_Server::READABLE ),
			array( array( $this, 'new_events'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON ),
			
		);
		
		/**
		 * single event 
		 **/
		$routes['/poker-api/event/(?P<id>\d+)'] = array(
			array( array( $this, 'get_event'), WP_JSON_Server::READABLE ),
		);
		
		/**
		 * events player list
		 */
		$routes['/poker-api/event/(?P<id>\d+)/players'] = array(
			array( array( $this, 'get_event_players'), WP_JSON_Server::READABLE ),
		);
		
		/**
		 * get single post comments list
		 */ 
		$routes['/poker-api/event/(?P<id>\d+)/comments'] = array(
			array( array( $this, 'get_comments'), WP_JSON_Server::READABLE ),
		);
		
		/**
		 * get single post comments list before and after timestamp
		 */ 
		$routes['/poker-api/event/(?P<event_id>\d+)/(?P<before_after>\D+)/(?P<date>\d+)/comments'] = array(
			array( array( $this, 'get_after_before_comments'), WP_JSON_Server::READABLE ),
		);
		
		/**
		 * add Comments
		 */ 
		$routes['/poker-api/event/comments'] = array(
			array( array( $this, 'addEventCommnets'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON ),
		);
		
		/**
		 * udpate comment like values
		 **/
		 $routes['/poker-api/event/(?P<comment_id>\d+)/comments/like'] = array(
			array( array( $this, 'updateCommentLike'), WP_JSON_Server::CREATABLE ),
		);
		
		/**
		 * udpate comment dislike values
		 **/
		 $routes['/poker-api/event/(?P<comment_id>\d+)/comments/dislike'] = array(
			array( array( $this, 'updateCommentDislike'), WP_JSON_Server::CREATABLE ),
		);
		
		// update notification settings
     	$routes['/poker-api/event/(?P<event_id>\d+)/notification'] = array(
			array( array( $this, 'update_notification_settings'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON ),
		);
		
		return $routes;

	}
	
	
	private function key_value_exists_index($array, $key, $val) {
		if ( ! $array) return false;
   		$loop_count = 0; 
	    foreach ($array as $item){
	    	if (isset($item[$key]) && $item[$key] == $val)
	            return $loop_count;
	        
	        $loop_count += 1;
	    }   
	    return false;
	}
	
	function get_player_event_chipcount($player_id, $event_id) {
		$chip_count_array = get_user_meta($player_id, 'chipcount', true);
		if($chip_count_array){	  	
			$find_index = $this->key_value_exists_index($chip_count_array, 'event_id', $event_id);
			$chipcount = $chip_count_array[$find_index]['chipcount'];
			$chipcount = $chipcount ? $chipcount : 0; 
	  	}else{
			$chipcount = 0;
		}
	  	return (string) $chipcount;
	}
	
	/**
	 * Callback function for events list
	 */ 
	public function get_events(){
		 // validate token 
		$request_user = $this->validation->get_user_info();
	  	if(!$request_user)
	  		return $this->validation->response("Missing header or invalid authtoken", null, 401);
		
	 	$event_args = array( 
		    'post_type' => 'events',  
			//////Pagination Parameters
		    'posts_per_page' => -1,                 //(int) - number of post to show per page (available with Version 2.1). Use 'posts_per_page'=-1 to show all posts. Note if the query is in a feed, wordpress overwrites this parameter with the stored 'posts_per_rss' option. Treimpose the limit, try using the 'post_limits' filter, or filter 'pre_option_posts_per_rss' and return -1
		    'paged' => get_query_var('page'),
		    'order' => 'DESC',                      
		    'orderby' => 'date'												 
		);
	
		$the_query = new WP_Query( $event_args);
		$data = array();
		// The Loop
		if ( $the_query->have_posts() ) :
			$post_count = 0; 
			
			while ( $the_query->have_posts() ) : $the_query->the_post();
				$data[$post_count]['ID'] = get_the_ID();
				$data[$post_count]['title'] = esc_attr(get_the_title());
				$data[$post_count]['excerpt'] = get_the_excerpt();
				$data[$post_count]['content'] = get_the_content();
			  
			   	$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), POKER_API_DEFAULT_IMAGE_SIZE );				
			  
			  if($image){
			  	$data[$post_count]['featured_images'] = $image[0];
			  }else{
			  	$data[$post_count]['featured_images'] = "";
			  }
			  //unset image
			  unset($image);
			  
			  $data[$post_count]['start_date'] = strtotime(get_post_meta(get_the_ID(), 'event_start_date',true) );
			  $data[$post_count]['end_date'] = strtotime( get_post_meta(get_the_ID(), 'event_end_date',true) );
			  
			  $chipcount_enable = get_post_meta ( get_the_ID(), 'chipcount_enable', true );
			  $chipcount_enable = $chipcount_enable ? $chipcount_enable : "0";
			  $data[$post_count]['chipcount_enable'] = $chipcount_enable;
			  // players
			  $event_players = get_post_meta(get_the_ID(), 'event_player',true);
			  $event_players_array = array();
					  
			 /* Removed */

			 if($event_players){
				  foreach ($event_players as $player):
				  	if($player){
				  		$chipcount = $this->get_player_event_chipcount($player, get_the_ID());
				  	}else{
				  		$chipcount =0;
				  	}
				  	$event_players_array[] = array( 'ID' =>$player,  'chipcount'=> $chipcount );
				  	//unset($chip_count_array);
				  endforeach;
				  $data[$post_count]['players'] = $event_players_array;
				}

			  $post_count++;
			endwhile;
			return $this->validation->response("success", $data, 200);
		else:
			return $this->validation->response("No post available till now", null, 400);
			
		endif;
		// Reset Post Data
		wp_reset_postdata();
		
		//reset valeus
		unset($event_players);
		unset($event_players_array);

		return $this->validation->response("success", $data, 200);
		
	 }
	
	// get single event
	public function get_event($id, $context = 'view'){
		// validate token 
		 $request_user = $this->validation->get_user_info();
	  	 if(!$request_user)
	  		return $this->validation->response("Missing header or invalid authtoken", null, 401);
		
		
		$post = get_post($id);

		$data = array();

		if( !empty( $post)):
			  $data['ID'] = $post->ID;
			  $data['title'] = esc_attr($post->post_title);
			  $data['excerpt'] = $post->post_excerpt;
			  $data['content'] = $post->post_content;
			  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), POKER_API_DEFAULT_IMAGE_SIZE );				
			  $data['featured_images'] = $image[0];
			  $data['start_stack'] = get_post_meta($post->ID, 'chipcount', true);
			  $data['post_modified'] = strtotime($post->post_modified);
			  $data['players'] = $this->players_info($post->ID);
			  $data['start_date'] = strtotime(get_post_meta($post->ID, 'event_start_date',true) );
			  $data['end_date'] = strtotime( get_post_meta($post->ID, 'event_end_date',true) );
			  $chipcount_enable = get_post_meta ( $post->ID, 'chipcount_enable', true );
			  $chipcount_enable = $chipcount_enable ? $chipcount_enable : "0";
			  $data['chipcount_enable'] = $chipcount_enable;
			  
			  return $this->validation->response("success", $data, 200);
		else:
			return $this->validation->response("No post available till now", null, 400);
		endif;

		return $this->validation->response("No post available till now", $data, 400);

	}
	
	// players info
	protected function players_info($post_id){
		$players = get_post_meta($post_id, 'event_player',true);
		$time = get_post_time($post_id);

		if( !$players )
			return $data;
		
		if( $players)
			sort($players);
		
		$players_info = array();
		$data = array();
			
		foreach($players as $player){
			$user_info = get_userdata($player);
			if (!$user_info) continue;
			$players_info['user_id'] = (string)$user_info->ID;
			$players_info['user_avatar'] = $this->get_user_avator_image_url($player);
			$players_info['first_name'] = $user_info->first_name;
			$players_info['last_name'] = $user_info->last_name;
			$players_info['user_email'] = $user_info->user_email;
			$players_info['display_name'] = $user_info->display_name;
			$players_info['roles'] = $user_info->roles;
			$status = get_user_meta($user_info->ID, 'is_online', true);
			$status = $status ? $status : 0;
			$players_info['status'] = (string) $status ;
			$chip_count_array = get_user_meta($player, 'chipcount', true);
			$n_s_a = get_user_meta($player, 'notification_settings', true);
			$players_info['chipcount'] = $this->get_player_event_chipcount($player, $post_id);
			
			$players_info['chipcount_last_modified'] = get_user_meta($player, 'chipcount_last_modified', true);
			if(empty($players_info['chipcount_last_modified'])){
				$players_info['chipcount_last_modified'] = $time;
			}


			$notification_settigns =  $n_s_a[$post_id];
			$players_info['notification_settings'] = $notification_settigns == '' ? 0 : $notification_settigns;
			//unset
			unset($chip_count_array);
			unset($n_s_a);
			unset($notification_settigns);
			
			$data[] = $players_info;
		}
		
		//unset valeus
		unset($players);
		unset($players_info);

		function sortByOrder($a, $b) {
		    return $b['chipcount'] - $a['chipcount'];
		}
		
		if($data)
			usort($data, 'sortByOrder');
		return $data;
		
	}
	
	// get playes list of single event
	public function get_event_players($id, $context = 'view'){
		// validate token 
		 $request_user = $this->validation->get_user_info();
	  	 if(!$request_user)
	  		return $this->validation->response("Missing header or invalid authtoken", null, 401);
		
		$post = get_post($id);
		$data = array();

		if( !empty( $post)):
			  $data= $this->players_info($post->ID);
			  if( $data ):
			  	unset($post);
			  	unset($request_user);
			  	return $this->validation->response("sucess", $data, 200);
			  endif;
				
		endif;
		
		//unset
		unset($post);
		unset($request_user);
		
		return $this->validation->response("No post available till now", null , 400);
	}
	
	// add new event
	public function new_events( $data ){
		/**
		 * Content-Type: application/x-www-form-urlencoded
		 * {"post_title":"My post 3 ","post_content":"This is my post3.",
		 * "post_type":"events","post_status":"publish","post_author":1}
		 **/
		 
		 // validate token 
		 $request_user = $this->validation->get_user_info();
	  	 if(!$request_user)
	  		return $this->validation->response("Missing header or invalid authtoken", null, 401);
		
		 // validation of keys
		 $valid_keys = array("post_title", 'post_content', 'post_status', 'post_author', 'post_excerpt', 'start_date', 'end_date' );
		 
		 if(!$this->validation->validateDataFormat($valid_keys, $data) ) {
		 	return $this->validation->response("Invalid data format");
		 }
		 
		 if(!array_key_exists('start_date', $data) || empty($data['start_date'])){
		 	return $this->validation->response("Required start_date with value");
		 }
		 
		 if(!array_key_exists('start_date', $data) || !empty($data['start_date'])){
		 	if( !$this->validation->validateDate( $data['start_date'], 'Y-m-d' ) )
		 		return $this->validation->response("Incorrect start_date value");
		 }
		 
		 
		 if(!array_key_exists('end_date', $data) || empty($data['end_date'])){
		 	return $this->validation->response("Required end_date with value");
		 }
		 
		 if(!array_key_exists('end_date', $data) || !empty($data['end_date'])){
		 	if( !$this->validation->validateDate( $data['end_date'], 'Y-m-d' ) )
		 		return $this->validation->response("Incorrect end_date value");
		 }
		 
		//meta values for events
		$event_start_date = $data["start_date"];
		$event_end_date = $data['end_date'];
		
		//remove start_date and end_date from array (data)
		unset($data['start_date']);
		unset($data['end_date']);
		 
		$data['post_type']   = "events";
		
		if(!array_key_exists('post_status', $data) ) $data['post_status'] = "publish";
		if(!array_key_exists('post_author', $data) ) $data['post_author'] = 1;
		
		if( $post_id = wp_insert_post( $data, false ) ):
			//start date
			update_post_meta($post_id, "event_start_date", $event_start_date);
			//end date
			update_post_meta($post_id, 'event_end_date', $event_end_date);
			
			$data= array(
				"response" => "Event created successfully.",
				"ID"       => $post_id,
			);
			return $this->validation->response("sucess", $data, 200 );
				
		else:
			return $this->validation->response("Error creating events");
		endif;

	}
	
	/**
	 * List of comments
	 **/
	public function get_comments($id, $context = 'view') {
		// validate token 
		 $request_user = $this->validation->get_user_info();
	  	 if(!$request_user)
	  		return $this->validation->response("Missing header or invalid authtoken", null, 401);
		
	 	if(!is_numeric($id)){
	 		return $this->validation->response('No post available till now');
	 	}
	 	
	 	$args = array(
			'status' => 'approve',
			'number' => '',
			'post_id' => $id, // use post_id, not post_ID
		);
		
		$comments = get_comments($args);
		$data = $this->prepare_comments_data($comments, $request_user->ID);
		
	    
	    if ($data)
			return $this->validation->response('sucess', $data, 200);
		
		return $this->validation->response('No post available till now', null, 400);
	
	 }
	
	/**
	 * comments list by before or after date
	 **/
	 
	public function get_after_before_comments($event_id, $before_after, $date) {
		// validate token 
		 $request_user = $this->validation->get_user_info();
		 //if(!$request_user)
	  		// return $this->validation->response("Missing header or invalid authtoken", null, 401);
		
		// set default timezone
		date_default_timezone_set('UTC');
		
		// output
		
		$current_date = date('Y-m-d H:i:s',$date);
		
		$date_query = null;
		if($before_after == 'after'){
			$date_query = array(
                    	// 'after'  => array( 'year'  =>date('Y',$date), 'month' => date('m',$date), 'day'   => date('d',$date), ),
                    	'after' => $current_date,
       					'inclusive' => false,
                	); 
		} elseif($before_after == 'before'){
			$date_query = array(
                    	// 'before'  => array( 'year'  =>date('Y',$date), 'month' => date('m',$date), 'day'   => date('d',$date), ),
       					'before' => $current_date,
       					'inclusive' => false,
                	);
		}else{
			return $this->validation->response('Invalid after or before keyes', null, 400);
		}
		
		$args = array(
			'status' => 'approve', 'number' => '','post_id' => $event_id, // use post_id, not post_ID
			'date_query' => $date_query
		);
		
		// $comments = get_comments($args);
		$comments = new WP_Comment_Query($args); 
		// print_r($comments->request);
		
		// print_r($comments->comments); return;
		$comments = $comments->comments;
		
		$user_id = $request_user ? $request_user->ID : null;
		$data = $this->prepare_comments_data($comments, $user_id);
		
	    if ($data)
			return $this->validation->response('sucess', $data, 200);
		
		return $this->validation->response('No post available till now', null, 200);
	}
	
	/**
	 * check current user is associated to events
	**/
	function is_event_associated_user($event_id, $user_id) {
		$players = get_post_meta($event_id, 'event_player', true);
		// echo "player id ". $user_id;
		// print_r($players);
		if ( ! $players ) return false;
		if( !$user_id ) return false;
		
		return in_array($user_id, $players);
	}
	
	
	/**
	 * Prepare data for commetns list
	 **/
	function prepare_comments_data( $comments, $user_id=null ) {
		$data = array();
		
		foreach($comments as $comment) :
			$comment_id = $comment->comment_ID;
			$comment = (array)$comment;
			$user_info = get_userdata($comment['user_id']);
			if( $user_info ):
				if(in_array('administrator', $user_info->roles)):
			   		$roles = 'player';
			   	else:
			   		$roles = $user_info->roles[0];
			   	endif;
			endif;
			
			$comment['user_role'] = $roles;
			$comment['assoicated_user'] = $this->is_event_associated_user($comment['comment_post_ID'], $comment['user_id']);
		   	$likes = get_comment_meta( $comment_id, 'total_likes', true );
		   	$dislikes = get_comment_meta( $comment_id, 'total_dislikes', true );
		  	$comment['comment_date'] = strtotime($comment['comment_date']);
		  	$comment['comment_date_gmt'] = strtotime($comment['comment_date_gmt']);
		  	
		  	if(!$comment['comment_image']){
		  		// Get the associated comment image
				$comment_image = get_comment_meta( $comment_id, 'comment_image', true );
				if ($comment_image):
					$comment['comment_image'] = $comment_image or "";
				else:
					$comment['comment_image'] = "";
				endif;
		  	}
		  	
		  	if( $user_id ) {
		  		$spam_data = get_comment_meta($comment_id, 'comment_spam_data', true);
		  		$comment['comment_spam'] = 0;
		        if( $spam_data){
		        	$key = array_search($user_id, array_column($spam_data, 'user_id'));
			        if( (string) $key == '0' || (string) $key > '0') {
			            $comment['comment_spam'] = 1;
			        }
		        }
		  	}
			
		  	$comment['comment_likes'] = empty( $likes ) ? '0' : $likes;;
		  	$comment['comment_dislikes'] = empty( $dislikes ) ? '0' : $dislikes;
		  	$comment['user_avatar'] = $this->get_user_avator_image_url($comment['user_id']);
		  	if ( $comment['user_id'] == '0')
		  		$comment['display_name'] = $comment['comment_author'];
		  	else
		  		$comment['display_name'] = trim($user_info->first_name. " ".$user_info->last_name);
	
		  	// remove unwanted data	
		  	unset($comment['comment_parent']);
		  	unset($comment['comment_type']);
		  	unset($comment['comment_agent']);
		  	unset($comment['comment_approved']);
		  	unset($comment['comment_karma']);
		  	unset($comment['comment_author_IP']);
		  	unset($comment['comment_author_url']);
		  	
		  	
		  	$data[] = $comment;
		endforeach;
		
		return $data;
	}
	
	protected function get_user_avator_image_url($id){
		$image_url = user_avatar_fetch_avatar( array( 'html'=>false, 'item_id' => $id, 'width' => '', 'height' => '', 'alt' => '' ) );
		if( $image_url ) return $image_url;
		return '';
	}
	private function write_json_file($post_id, $data) {
      $queue_dir = ABSPATH.'wp-content/themes/appsperia-child/queue/pending/';
      file_put_contents($queue_dir."{$post_id}.txt", print_r($data, true));
    }
	
	/**
	 * add comments
	 **/ 
	
	public function addEventCommnets($data=null) {
		/**
		 * Content-Type: application/x-www-form-urlencoded
		 * {'comment_post_ID':"1","comment_author":"Kishor",  "comment_author_email":"events@event.com",
		 * "comment_author_url":"http://example.com","comment_content":"Comment messsage... Rest Client"}
		 **/ 
		if( !$data)
		$data = $_POST;

		// validation of keys
		 $valid_keys = array("comment_post_ID", "comment_author", "comment_author_email", "comment_author_url", "comment_content", "comment_type", "comment_parent", "user_id");
		 
		 if(!$this->validation->validateDataFormat($valid_keys, $data) ) {
		 	return $this->validation->response("Invalid data format");
		 }
		
		if(!array_key_exists('comment_author', $data) || empty($data['comment_author']) || !array_key_exists('comment_content', $data))
			return $this->validation->response("Required comment_author and comment_author value ");
		
		$data['comment_type'] = '';
		$data['comment_parent'] = 0;
		
		$data['comment_content'] = htmlspecialchars($data['comment_content']);

		if(array_key_exists('user_id', $data)) {
			if( empty($data['user_id']) ) $data['user_id'] = 0;	
		}else{
			$data['user_id'] = 0;
		}
		
		//Insert new comment and get the comment ID
		$comment_id = wp_insert_comment( $data );

		if(is_wp_error($comment_id)){
			$error_string = $result->get_error_message();
		}
		
		if( $comment_id <= 0 )
		 	return $this->validation->response("No post available till now", null, 400);
		
		if ( is_numeric($comment_id) ) :
			$upload_image_url = $this->save_comment_image($comment_id, $data['comment_post_ID'] );
			
			$db_data = get_comment( $comment_id, ARRAY_A );
			
			if( $upload_image_url ): 
				if($upload_image_url['error'])
					$db_data['comment_image'] = $upload_image_url['error'];
				else if($upload_image_url['url'])
					$db_data['comment_image'] = $upload_image_url['url'];
			endif;
			
			$data_r[] = $db_data;
			$data = $this->prepare_comments_data($data_r);
			
			$this->notification_queue_insert( $data['user_id'], $data['comment_post_ID'], $data['comment_author']);
			
			if ($data)
				return $this->validation->response("sucess", $data, 200);
			
		endif;
		
		return $this->validation->response("No post available till now", null, 400);

	}
	
	private function notification_queue_insert($user_id, $comment_post_ID, $comment_author){
		global $wpdb;
		$args = array(
		      'meta_query' => array( array( 'key' => 'user_device_list') ),
		     'fields' => array( 'ID' )
		);
		 
		if( $user_id) 
			$args['exclude'] = array($user_id);
		
		$all_users = (array) get_users($args);

		if ( !$all_users ) return ;
		
		$players_list = array();
		foreach ($all_users as $user) { $players_list[] = $user->ID; }
		$notification_message = ucfirst($comment_author) . " have post in ".get_the_title( $comment_post_ID );
		if(!$players_list) return ;
		
		insert_notification_data($players_list, $notification_message, $comment_post_ID);
      
	}
	
	/**
	 * Adds the comment image upload form to the comment form.
	 *
	 * @param	$comment_id	The ID of the comment to which we're adding the image.
	 */
	function save_comment_image($comment_id, $post_id){
		
	  	$uploadedfile = $_FILES['file'];
      	$upload_overrides = array( 'test_form' => false );
    	if( !$uploadedfile ) return $upload_image_url['url'] = '';
        
        global $comment_id_API;
	  	$comment_id_API = $comment_id;
	  	
	  	
        add_filter('wp_handle_upload_prefilter', 'custom_upload_filter' );

        function custom_upload_filter( $file ){
        	
        	$file['name'] = "/".$file['name'];
            return $file;
        }
        unset($_path_info);
      	unset($path_info);
      	
        add_filter( 'upload_dir', 'wpse_141088_upload_dir' );
        function wpse_141088_upload_dir( $dir ) {
            global $comment_id_API;
            return array(
                'path'  => $dir['basedir'] . '/comments_image/'.$comment_id_API,
                'url'    => $dir['baseurl'] . '/comments_image/'.$comment_id_API,
                'subdir' => '/comments_image/'.$comment_id_API,
            ) + $dir;
            
            unset($user_current_id_API);
        }
        
        //uplod current files
        $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
        
        //remove filter
        remove_filter( 'upload_dir', 'wpse_141088_upload_dir' );
        remove_filter( 'wp_handle_upload_prefilter', 'custom_upload_filter' );
        
        
        if ( $movefile && !isset( $movefile['error'] ) ) {
        	update_comment_meta( $comment_id, 'comment_image', $movefile['url'] );
			$upload_image_url['url'] = $movefile['url'];
			return $upload_image_url;
	
        }
     	
     	$upload_image_url['error'] = $movefile['error'];
		return $upload_image_url;
		
	}
	
	
	
	/**
	 * Determines if the specified type if a valid file type to be uploaded.
	 *
	 * @param	$type	The file type attempting to be uploaded.
	 * @return			Whether or not the specified file type is able to be uploaded.
	 */
	private function is_valid_file_type( $type ) {

		$type = strtolower( trim ( $type ) );
		return 	$type == 'png' ||
				$type == 'gif' ||
				$type == 'jpg' ||
				$type == 'jpeg';

	} // end is_valid_file_type
	
	
	/**
	 * add comment meta fields like
	 **/
	public function updateCommentLike( $comment_id ) {
		$queue_dir = ABSPATH.'wp-content/themes/appsperia-child/queue/';
		
		$likes = get_comment_meta( $comment_id, 'total_likes', true );
	 	$update_likes = $likes + 1;
	 	if( update_comment_meta( $comment_id, 'total_likes', $update_likes, $likes ) ){
	 		$data['total_likes'] = $update_likes;
	 		$json_data['total_likes'] = $update_likes;
	 		$json_data['comment_id'] = $comment_id;
	 		$filesize = filesize($queue_dir.'likejson.txt');
	 		
	 		if($filesize) {
	 			file_put_contents($queue_dir."likejson.txt", ",".json_encode($json_data), FILE_APPEND);    	
	 		}else {
	 			file_put_contents($queue_dir."likejson.txt", json_encode($json_data) , FILE_APPEND);    	
	 		}
	 		
	 		return $this->validation->response('sucess', $data, 200);
		 	
	 	}else{
	 		return $this->validation->response('Update error!', null, 200);
	 	}
	 	
	 }
	
	/**
	 * update comment dislike value
	 **/
	public function updateCommentDislike( $comment_id ) {
		$queue_dir = ABSPATH.'wp-content/themes/appsperia-child/queue/';
		
	 	$likes = get_comment_meta( $comment_id, 'total_dislikes', true );
	 	$update_likes = $likes + 1;
	 	
	 	if( update_comment_meta( $comment_id, 'total_dislikes', $update_likes, $likes ) ){
	 		$data['total_dislikes'] = $update_likes;
	 		$json_data['total_dislikes'] = $update_likes;
	 		$json_data['comment_id'] = $comment_id;
	 		$filesize = filesize($queue_dir.'likejson.txt');
	 		
	 		if($filesize) {
	 			file_put_contents($queue_dir."likejson.txt", ",".json_encode($json_data), FILE_APPEND);    	
	 		}else {
	 			file_put_contents($queue_dir."likejson.txt", json_encode($json_data) , FILE_APPEND);    	
	 		}
	 		
	 		return $this->validation->response('sucess', $data, 200);
		 		
	 	}else{
	 		return $this->validation->response('Update error!', null, 200);
	 	}
	 	
	 	
	 }
	
	
	// update event notification set on or off(0, 1)
	public function update_notification_settings( $event_id, $data ) {
		/**
		 * Content-Type: application/x-www-form-urlencoded
			authToken: 87ea9476c2f1a3ab2b2d8f0c38b3783f1c967019f281a779da61cd246cbd8069
			Cache-Control: no-cache
			{"get_notification":0}
		**/
		
		// validate token 
		 $request_user = $this->validation->get_user_info();
	  	 if(!$request_user)
	  		return $this->validation->response("Missing header or invalid authtoken", null, 401);
		
		if(!array_key_exists('get_notification', $data)) {
		 	return $this->validation->response("Required get_notification with value");
		 }
		 
		 $notification_settigns = get_user_meta($request_user->ID, 'notification_settings', true);
		 $notification_settigns[$event_id] = $data['get_notification'] == 1 ? 1 : 0;
		 
		 if(update_user_meta( $request_user->ID, 'notification_settings', $notification_settigns ))
		 	return $this->validation->response("Successfully updated", null, 200);
		 else
		 	return $this->validation->response("No any changes", null);
		 		
	}
}