<?php
class POKER_API_Devices{
	
	var $validation;
	function __construct() {
    	require_once ('validation.php');
		  $this->validation = new POKER_API_Validation();
   	}	
   	
	public function register_routes( $routes ) {

		$routes['/poker-api/register-device'] = array(
			array( array( $this, 'register_device'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON  ),
		);
		
		$routes['/poker-api/get-device'] = array(
			array( array( $this, 'get_devices'), WP_JSON_Server::READABLE ),
		);
		
		return $routes;

	}
	
    public function get_devices() {
    	$request_user = $this->validation->get_user_info();
		if(!$request_user)
			return $this->validation->response("Missing header or invalid authtoken", null , 400);
		
		$user_id = $request_user->ID;
		// echo $user_id;
		$user_devices_array = get_user_meta($user_id, 'user_device_list', true);
		
		if ($user_devices_array)
        	return $this->validation->response('success', $user_devices_array, 200);
        
        return $this->validation->response('No data found !', null, 400);
    }
    
  	
	public function register_device($data) {
		$request_user = $this->validation->get_user_info();
		if(!$request_user)
			return $this->validation->response("Missing header or invalid authtoken", null , 401);

        // validation of keys
		$valid_keys = array('device_token', 'device_type');
		
		 if(!$this->validation->validateDataFormat($valid_keys, $data) ) {
		 	return $this->validation->response("Invalid data format");
		 }
		 
		if (!array_key_exists('device_token', $data) || empty($data['device_token'])) {
			return $this->validation->response('Rquired device_token key and value');
		}
		if (!array_key_exists('device_type', $data) || empty($data['device_type']) ) {
			return $this->validation->response('Rquired device_type key and value');
		}
		
		$user_id = $request_user->ID;
		// update_user_meta($user_id, 'user_device_list', array());
		$user_devices_array = get_user_meta($user_id, 'user_device_list', true);
		$tokens = null;
		if ($user_devices_array)
			$tokens = array_keys($user_devices_array);
			
		if ($tokens) {
			if ( !in_array( $data['device_token'],  $tokens) ):
					$user_devices_array[$data['device_token']] = $data['device_type'];
			else:
				return $this->validation->response('Device already registered!', null, 400);
			endif;
		}else{
			$user_devices_array[$data['device_token']] = $data['device_type'];
		}
		
		$updated = update_user_meta($user_id, 'user_device_list', ($user_devices_array));
		
        if ($updated)
        	return $this->validation->response('Device successfully registered!',null , 200);
        else
        	return $this->validation->response('Device fail to register, please try again', null, 400);
	}
  
}

?>