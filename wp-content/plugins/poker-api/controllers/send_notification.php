<?php
class POKER_API_MobileNotification {
    protected $users ; 
    protected $notification_message;
    protected $event_id; 
    function __construct($players= null, $message=null, $event_id=null ) {
        if ($players) $this->users = $players;
            
        if( $message ) $this->notification_message = $message;
        
        if( $event_id ) $this->event_id = $event_id;
    }
    
    private function sendNotification( $user_devices_array, $badge ){
        if( $user_devices_array ):
            foreach($user_devices_array as $token=>$device):
                file_put_contents(dirname(__FILE__)."/test4.txt", $token);
                if ( strtolower( $device ) == 'ios') {
                    
                    $this->send_notification( $token, $this->notification_message, $badge );
                }
                
            endforeach;
        else:
            $message = 'Device not found => '.$deviceToken . " time : ". date('m/d/Y h:i:s a', time());
            file_put_contents(dirname(__FILE__)."/notificaiton_log.txt", $message . "\n", FILE_APPEND);
        endif;
    }
    
    private function want_notification( $user_id ) {
        $event_id = $this->event_id;
        $get_notification = get_user_meta( $user_id, 'notification_settings', true);
        
        if( !$get_notification ) return true ;
        $is_active = (string) $get_notification[$event_id];
        
        if( $is_active == 1 ) return true;
        else return false;
        
    }
    
    public function send() {
        if ( !$this->users ) return false;
        foreach ( $this->users as $player ):
            $user_devices_array = get_user_meta($player, 'user_device_list', true);
            $notification_badge = get_user_meta($player, 'notificaiton_badge', true);
            $notification_badge = $notification_badge ? $notification_badge : 0;
            $current_badge = $notification_badge + 1;
            update_user_meta($player, 'notificaiton_badge', $current_badge);
            // $this->send_notification( "EC7731A19A671D6C1FA0CAF41EC05E394EB01FD33BCC6758AFA2FA6110AB8C3B", $this->notification_message );
            if( $this->event_id ) {
                if( $this->want_notification($player)){
                    $this->sendNotification($user_devices_array, $current_badge);
                }
                
            }else{
                $this->sendNotification($user_devices_array, $current_badge);
            }
                
        endforeach;
        
        return true;
        
        
    }
    
     function send_notification( $deviceToken, $message, $badge=1 ) {
        $notificaiton_file = dirname(__FILE__)."/notificaiton_log.txt";
        // Put your device token here (without spaces):
        // $deviceToken = 'EFD3C92D2AF582C40DFFBAFF5C8D05B09C3A19BBC6A6191144D9D90D29BAE998';  // neetin iPhone 6s
        
        // $deviceToken = '94C9E44FFB02B76FFAC952E6917A1B52F9187C89310D38D65C15F9A1726A249A'; // 3C ipad mimi
        // $deviceToken = '1E2ABC17F87D677A16C751F98FBF214A779EE8C3AE11DEE77CE756B552D1E3FF'; // 3c iPad mini
        // 1E2ABC17F87D677A16C751F98FBF214A779EE8C3AE11DEE77CE756B552D1E3FF - abiral
        
        // Put your private key's passphrase here:
        $passphrase = '1234';
        
        // Put your alert message here:
        if( !$message )
            $message = 'Notificaton';
        
        ////////////////////////////////////////////////////////////////////////////////
        
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', POKER_API_DIR.'pem/production_com.pokermagazine.Poker-Magazine.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        
        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,
        	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
        
        if (!$fp){
            $msg = "Failed to connect: $err $errstr" . PHP_EOL;
            file_put_contents($notificaiton_file, $msg . "\n", FILE_APPEND);
            return true;
        }
        
        // Create the payload body
        $body['aps'] = array(
        	'alert' => $message,
        	'sound' => 'default',
        	'badge' => $badge
        	);
        
        // Encode the payload as JSON
        $payload = json_encode($body);
        
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        
        if (!$result){
            $message = 'Message not delivered => '.$deviceToken . " time : ". date('m/d/Y h:i:s a', time());
            file_put_contents($notificaiton_file, $message . "\n", FILE_APPEND);
            
        }
        else{
            $message = 'Message successfully delivered => '.$deviceToken . " time : ". date('m/d/Y h:i:s a', time() );
            file_put_contents($notificaiton_file, $message . "\n", FILE_APPEND);
        }
        	
        
        // Close the connection to the server
        fclose($fp);
        return ;
    }    
    
    public function __destruct() {
        
    }
}