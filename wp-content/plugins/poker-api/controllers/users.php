<?php
class POKER_API_Users{
	
	var $validation;
	function __construct() {
    	require_once ('validation.php');
		$this->validation = new POKER_API_Validation();
   	}	
   	
	public function register_routes( $routes ) {
		
		$routes['/poker-api/player/login'] = array(
			array( array( $this, 'user_login'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON  ),
		);
		
		$routes['/poker-api/player/(?P<id>\d+)/logout']  = array(
			array( array( $this, 'user_logout'), WP_JSON_Server::READABLE  ),
		);
		
		$routes['/poker-api/player/is-online'] = array(
			array( array( $this, 'is_online_user'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON  ),
		);
		
		$routes['/poker-api/player/set-offline'] = array(
			array( array( $this, 'set_offline_all'), WP_JSON_Server::READABLE ),
		);
		
		
		$routes['/poker-api/player/register'] = array(
			array( array( $this, 'user_register'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON  ),
		);
		
		$routes['/poker-api/player/update'] = array(
			array( array( $this, 'update_user_meta'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON  ),
		);
		
		
		$routes['/poker-api/player/(?P<id>\d+)/events'] = array(
			array( array( $this, 'user_events'), WP_JSON_Server::READABLE ),
		);
		
		$routes['/poker-api/player/(?P<id>\d+)/chipcount'] = array(
			array( array( $this, 'get_user_chipcount'), WP_JSON_Server::READABLE ),
		);
		
		$routes['/poker-api/player/(?P<id>\d+)/update/chipcount'] = array(
			array( array( $this, 'update_chipcount'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON  ),
		);
		
		$routes['/poker-api/users/(?P<role>\D+)'] = array(
			array( array( $this, 'pokerUsers'), WP_JSON_Server::READABLE  ),
		);
		
		$routes['/poker-api/resetpswd'] = array(
			array( array( $this, 'generateResetPassowrdToken'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON  ),
		);
    
		$routes['/poker-api/setpaswd'] = array(
			array( array( $this, 'setUserPasswrod'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON  ),
		);
    	
    	return $routes;

	}
	
	protected function get_user_avator_image_url($id){
		$image_url = user_avatar_fetch_avatar( array( 'html'=>false, 'item_id' => $id, 'width' => '', 'height' => '', 'alt' => '' ) );
		if( $image_url ) return $image_url;
		return '';
	}
	
	/**
	 * user login reuqest call back 
	 **/ 
	public function user_login( $data= array() ) {
		/**
		 * Content-Type: application/x-www-form-urlencoded
		 * {"user_login":"admin ","user_password":"admin", "remember":true}
		
		// $creds['user_login'] = 'admin';
		// $creds['user_password'] = 'admin';
		// $creds['remember'] = true;
		 
		 **/
		 
		 // validation of keys
		 $valid_keys = array("user_login", 'user_password', 'remember');
		 if(!$this->validation->validateDataFormat($valid_keys, $data) ) {
		 	return $this->validation->response("Invalid data format");
		 }
		 
		if( !array_key_exists('user_login', $data) || empty($data['user_login']) || !array_key_exists('user_password', $data) || empty( $data['user_password']) ) {
			return $this->validation->response("Please enter user_login and user_password value");
		} 
		 
		$user = wp_signon( $data, false );
		
		if ( is_wp_error($data) ):
			return $this->validation->response($user->get_error_message(), null, 400);
		else:
			
			if ($user->errors):
				return $this->validation->response('username or password incorrect', null, 400);
				
			else:
				$role = $user->roles;
				if ($role):
					if(in_array('administrator', $role) )
						$role[0] = 'player';
					
				endif;		
				
				$response_data = (array) $user->data;
				$response_data['roles'] = $role;		
				$response_data['avatar'] = $this->get_user_avator_image_url($response_data['ID']);
				// user_avatar_avatar_exists($response_data['ID']);
				$response_data['authToken'] = $this->get_poker_auth_token($response_data['ID']);
				return $this->validation->response('sucess', $response_data, 200);
			endif;	
			
		endif;
		
		return $this->validation->response('unexpected error', null, 400);
		

	}
	
	/**
	 * User logout 
	 **/
	 public function user_logout( $id ) {
	 	if(update_user_meta($response_data['ID'], 'is_online', 0)){
	 		return $this->validation->response("Sucess", null, 200);
	 	}else{
	 		return $this->validation->response("Error", null, 400);
	 	}
	 }
	 
	 /**
	  * Check user is online or not 
	  **/
	  public function is_online_user( $data ) {
	  	// input data : {'user_id': 41}
	  	$request_user = $this->validation->get_user_info();
	  	if( !$request_user )
	  		return $this->validation->response( "Missing header or invalid authtoken", null , 400 );
	  	
	  	// validation of keys
		 $valid_keys = array("user_id");
		 
		 if(!$this->validation->validateDataFormat($valid_keys, $data) ) {
		 	return $this->validation->response("Invalid data format");
		 }
	  	
	  	update_user_meta($data['user_id'], 'is_online', 1);
	  	update_user_meta($data['user_id'], 'notificaiton_badge', 0);

	  	return $this->validation->response("Sucess", (int) 1, 200 );
	  	
	  }
	  
	  /**
	   * set user offline status for cron jobs
	   **/
	   
	   public function set_offline_all(){
	   		$users = get_users();
	   		foreach($users as $user){
	   			update_user_meta($user->ID, 'is_online', 0);
	   		}
	   		
	   		return $this->validation->response("Sucess", null, 200 );
	   }
	   
	  
	/** 
	 * user Register
	 */
	 public function user_register( $data ) {
	 	/**
		 * Content-Type: application/x-www-form-urlencoded
		 * {"user_login":"admin ","user_email":"admin@admin.com", "user_pass":'abcd'}
		 * refrence : https://codex.wordpress.org/Function_Reference/wp_insert_user
		*/
		// validation of keys
		 $valid_keys = array("user_login", 'user_email', 'user_pass', 
		 				'user_nicename', 'user_url', 'display_name', 'nickname', 'first_name', 'last_name', 'description', 'user_role' 
		 			);
		 
		 if(!$this->validation->validateDataFormat($valid_keys, $data) ) {
		 	return $this->validation->response("Invalid data format");
		 }
		 
		if (!array_key_exists('user_login', $data) || empty($data['user_login'])) {
			
			return $this->validation->response('Rquired user_login key and value');
		}
		
		if (!array_key_exists('user_email', $data) || empty($data['user_email']) ) {
			return $this->validation->response('Rquired user_email key and value');
		}
		
		if (filter_var($data['user_email'], FILTER_VALIDATE_EMAIL) === false) {
		  return $this->validation->response('Invalid email address value');
		} 
		
		if (!array_key_exists('user_pass', $data) || empty($data['user_pass'])) {
			return $this->validation->response('Rquired user_pass key and value');
		}
		
		if (!array_key_exists('user_role', $data) || empty($data['user_role'])) {
			return $this->validation->response('Rquired user_role key and value');
		}
		if ( !in_array($data['user_role'] , array('visitor', 'player')) ) {
			return $this->validation->response('Rquired user_role must be visitor or player');
		}
		
		$data['role'] = $data['user_role'];
		$message = array();
	 	$user_id = username_exists( $data['user_login'] );
		if ( !$user_id and email_exists($data['user_email']) == false ) {
			$user_id = wp_insert_user( $data );
			if ($user_id->errors):
				return $this->validation->response($user_id);
			else:
				$user_info = get_userdata($user_id);
				$roles = $user_info->roles;
				
				$user_info = (array) $user_info->data;
				$user_info['avatar'] = $this->get_user_avator_image_url($user_id);;
				$user_info['authToken'] = $this->get_poker_auth_token($user_id);
				$user_info['roles'] = $roles;
				
				return $this->validation->response('User successfully created', $user_info, 200);
			endif;
		} else {
			return $this->validation->response("User already exists.  Password inherited.", null, 400);
		}
	 
	 	return $this->validation->response("Unexpected error");
	 	
	 }
	 
	 /**
	  * get user auth token
	  **/
	  function get_poker_auth_token($user_id){
	  	return get_user_meta($user_id, 'poker_authToken', true);
	  }
	 
	 
	 /**
	  * update user profile with meta
	  **/
	  
	 public function update_user_meta($data) {
	 	$request_user = $this->validation->get_user_info();
	  	if(!$request_user)
	  		return $this->validation->response("Missing header or invalid authtoken", null , 400);
	  	
	  	$user_id = $request_user->ID;
	  	// validation of keys
		 $valid_keys = array("first_name", 'last_name', 'user_email');
		 
		
		 if(!$this->validation->validateDataFormat($valid_keys, $data) ) {
		 	return $this->validation->response("Invalid data format");
		 }

		$data['ID'] = $user_id; 
		$update_user_id = wp_update_user( $data );
		
		$user = get_userdata( $update_user_id );
		
		if($update_user_id){
	  		$role = $user->roles;
			if ($role):
				if(in_array('administrator', $role) )
					$role[0] = 'player';
				
			endif;		
			
			$response_data = (array) $user->data;
			$response_data['first_name'] = $user->first_name;
			$response_data['last_name'] = $user->last_name;
			$response_data['display_name'] = $user->first_name. " ".$user->last_name;
			$response_data['roles'] = $role;		
			$response_data['avatar'] = $this->get_user_avator_image_url($response_data['ID']);
			// user_avatar_avatar_exists($response_data['ID']);
			$response_data['authToken'] = $this->get_poker_auth_token($response_data['ID']);
			return $this->validation->response('sucess', $response_data, 200);
		  		
	  	}
	  		
	  	return $this->validation->response("No result found!", null, 400);
	  	
	 }
	 /**
	  * single user events list
	  **/
	  public function user_events($id) {
	  	$request_user = $this->validation->get_user_info();
	  	if(!$request_user)
	  		return $this->validation->response("Missing header or invalid authtoken", null , 400);
	  	
	  	$args = array( 
		    'post_type' => 'events',  
		    'author' => $id,
			//////Pagination Parameters
		    'posts_per_page' => -1,                 //(int) - number of post to show per page (available with Version 2.1). Use 'posts_per_page'=-1 to show all posts. Note if the query is in a feed, wordpress overwrites this parameter with the stored 'posts_per_rss' option. Treimpose the limit, try using the 'post_limits' filter, or filter 'pre_option_posts_per_rss' and return -1
		    'paged' => get_query_var('page'),
		    
		    'order' => 'DESC',                      
		    'orderby' => 'date'												 
		);
	
		$the_query = new WP_Query( $args);
		$data = array();
		// The Loop
		if ( $the_query->have_posts() ) :
			$post_count = 0; 
			
			while ( $the_query->have_posts() ) : $the_query->the_post();
			  $data[$post_count]['ID'] = get_the_ID();
			  $data[$post_count]['title'] = esc_attr(get_the_title());
			  $data[$post_count]['excerpt'] = get_the_excerpt();
			  $data[$post_count]['content'] = get_the_content();
			  $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), POKER_API_DEFAULT_IMAGE_SIZE );				
			  $data[$post_count]['featured_images'] = $image[0];
			  
			  //$data[$post_count]['events_player'] = get_post_meta(get_the_ID(), 'event_player',true);
			  
			  // players
			  $event_players = get_post_meta(get_the_ID(), 'event_player',true);
			  $event_players_array = array();
			  foreach ($event_players as $player):
			  	
			  	$chip_count_array = get_user_meta($player, 'chipcount', true);
			  	$event_players_array[] = array('ID' =>$player, 'chipcount'=>$chip_count_array[get_the_ID()] ? $chip_count_array[get_the_ID()] : 0);
			  
			  endforeach;
			  
			  $data[$post_count]['players'] = $event_players_array;
			  
			  $post_count++;
			  
			endwhile;
		else:
			return $this->validation->response("No data found!", null , 400);
		endif;
		// Reset Post Data
		wp_reset_postdata();
		
		return $this->validation->response("sucess", $data , 200);
		
	  }
	  
	  
	  /**
	   * list of player chipcounts
	   **/
	   public function get_user_chipcount($id) {
			$request_user = $this->validation->get_user_info();
			if(!$request_user)
			  	return $this->validation->response("Missing header or invalid authtoken", null , 400);
			
			$formated_players_chipcount = get_user_meta($id, 'chipcount',true);
			
			
			if($formated_players_chipcount)
				return $this->validation->response("sucess", $formated_players_chipcount , 200);	
			else
				return $this->validation->response("No data found!", null , 400);	
	  	
	   }
	   
	   //check key and value exits on array or not 
	   private function key_value_exists($array, $key, $val) {
	   		$loop_count = 0;
	   		if ( !$array ) return false;
		    foreach ($array as $item){
		    	if (isset($item[$key]) && $item[$key] == $val)
		            return true;
		    }   
		    return false;
		}
	   	
	   	
	   	private function key_value_exists_index($array, $key, $val) {
	   		$loop_count = 0; 
		    foreach ($array as $item){
		    	if (isset($item[$key]) && $item[$key] == $val)
		            return $loop_count;
		        
		        $loop_count += 1;
		    }   
		    return false;
		}
	   
	   /**
	    * update user chipcount
	    * post data : { "event_id": 191, "chipcount": "250" }
	    **/
	    public function update_chipcount($id, $data) {
	    	$request_user = $this->validation->get_user_info();
	    	
			if(!$request_user)
			  	return $this->validation->response("Missing header or invalid authtoken", null , 400);
			 
			 // validation of keys
			 $valid_keys = array("event_id", 'chipcount');
			 
			
			 if(!$this->validation->validateDataFormat($valid_keys, $data) ) {
			 	return $this->validation->response("Invalid data format");
			 } 	
	    	
			if($data):
				// current value modificaiton 
				$event_players_chipcount = get_user_meta($id, 'chipcount',true);
				// update_user_meta( $id, 'chipcount', '' );
				// print_r($event_players_chipcount); return;
	    		$current_players_chipcount = array();
				
				if ($this->key_value_exists($event_players_chipcount, 'event_id', $data['event_id'])){
						$find_index = $this->key_value_exists_index($event_players_chipcount, 'event_id', $data['event_id']);
						$event_players_chipcount[$find_index]['chipcount'] = $data['chipcount'];
				}else{
					$event_players_chipcount[] = array('event_id'=>$data['event_id'], "chipcount"=> (string)$data['chipcount']);
				}
				
				if(update_user_meta( $id, 'chipcount', $event_players_chipcount )):
					$update_date = new DateTime();
					$update_date = $update_date->format('Y-m-d H:i:s');
					update_user_meta($id, 'chipcount_last_modified', strtotime($update_date));
					
					$notification_message = ucfirst($request_user->user_nicename) . " has updated his/her chips count.";
      
				    $event_players = get_post_meta($data['event_id'], 'event_player',true);
				    if($event_players)
					    if(($key = array_search( $request_user->ID, $event_players)) !== false) {
					        unset($event_players[$key]);
					    }
				    // file_put_contents(dirname(__FILE__)."/firsttest.txt", print_r($event_players,true));
				      
				    $Notification = new POKER_API_MobileNotification($event_players, $notification_message);
				      
				    $Notification->send();
					
					
					
					return $this->validation->response("sucess", $event_players_chipcount , 200);	
				else:
					return $this->validation->response("Update Error", null , 400);	
				endif;
			
			endif;
			
			return $this->validation->response("Update Error", null , 400);	
			
	    }
	    
	    
	    /**
	     * list of a live blogging users by role with these data:  user_id, user_image_url, user_full_name, etc
	     **/
	     public function pokerUsers($role) {
	     	$request_user = $this->validation->get_user_info();
			if(!$request_user)
			  	return $this->validation->response("Missing header or invalid authtoken", null , 400);
			
				if($role == 'visitor') $role = 'subscriber';
	     	$bloging_player = get_users( array('role' => $role) );
	     	if(!$bloging_player)
	     		return $this->validation->response("No data found!", null , 400);
	     	
	     	// list of playes with detail info
	     	$players = array() ;
	     	foreach($bloging_player as $p){
	     		$p = (array)$p;
	     		$user_id = $p['ID'];
	     		$user_info = get_userdata($user_id);
	     	
	     		$player = array(
	     			'user_id' => $user_id,
	     			'user_email' => $user_info->user_email,
	     			'user_avatar' => $this->get_user_avator_image_url($user_id),
	     			'first_name' => $user_info->first_name,
	     			'last_name' => $user_info->last_name,
	     			'nickname' => $user_info->nickname,
	     			'roles' => $user_info->roles,
	     			'display_name' => $user_info->first_name. " ".$user_info->last_name,
	     			
	     			);
	     		$players[] = $player;	
	     		
	     	}
         
	     	if(!$players)	return $this->validation->response("No data found!", null , 400);	
	     	return $this->validation->response("sucess", $players , 200);	
	     	
	     	
	     }
	    
	    
	    /**
	     * generateResetPassowrdToken of players
	     **/
	     public function generateResetPassowrdToken($data){
	     	// validation of keys
			 $valid_keys = array('user_email');
			 
			 if(!$this->validation->validateDataFormat($valid_keys, $data) )
			 	return $this->validation->response("Invalid data format");
			 
			 $user = get_user_by( 'email', $data['user_email'] );
			 if (!$user)
			 	return $this->validation->response("No user exists!", null, 400);
			 	
			$seed = str_split('0123456789');
			shuffle($seed); // probably optional since array_is randomized; this may be redundant
			$rand = '';
			foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
			
			$message = "Your passowrd reset token is : ".$rand . '\r\n Thanks \n Poker Team';

      		$headers[] = 'From: Poker <info@poker.com>';
			$headers[] = 'Content-Type: text/html; charset=UTF-8';
        
         
			$mail = wp_mail( $data['user_email'], 'Password Reset Token', $message, $headers );
			// Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
			
      
         
			if( $mail ){
				update_user_meta( $user->ID, 'API_password_token', $rand );
		        //$rmessage['reset_token'] = $rand;
		        $rmessage['user_email'] = $data['user_email'];
		        $rmessage['authToken'] = $this->get_poker_auth_token($user->ID);
				return $this->validation->response("Sucess", $rmessage, 200);
			}
			else{
				return $this->validation->response("Mail not send try again!", null, 400);
			}
				
		
	     	
	     }
  
  		/* set user password */
  		 public function setUserPasswrod($data) {
	  		$request_user = $this->validation->get_user_info();
			if(!$request_user)
		  	return $this->validation->response("Missing header or invalid authtoken", null , 400);
      
      	
      		// validation of keys
		 	$valid_keys = array('new_password', 'reset_token');
		 
		 	if(!$this->validation->validateDataFormat($valid_keys, $data) )
		 		return $this->validation->response("Invalid data format");
      
	      	if (!array_key_exists('new_password', $data) || empty($data['new_password'])) {
	          return $this->validation->response('Rquired new_password key and value');
	        }
	      	
	      	if (!array_key_exists('reset_token', $data) || empty($data['reset_token']) ) {
	          return $this->validation->response('Rquired reset_token key and value');
	        }
	      	
	      	$user_id = $request_user->ID;
	      	$reset_token = get_user_meta($user_id, 'API_password_token', true);
	      	
	      	if($reset_token != $data['reset_token'])
	          return $this->validation->response('Reset token not valid', null, 401);
	      
		  	  wp_set_password( $data['conform_password'], $user_id );
		      update_user_meta( $user_id, 'API_password_token', '' );
	      	
	      	$data['user_login'] = $request_user-> user_login;
	      	return $this->validation->response('sucess', $data, 200);
	    }
	    
	     
}