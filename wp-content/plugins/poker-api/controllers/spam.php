<?php
class POKER_API_Spam {
    public $validation;
	function __construct() {
    	$this->validation = new POKER_API_Validation();
   	}
   	
    public function register_routes( $routes ) {
		
		$routes['/poker-api/spam-comment'] = array(
			array( array( $this, 'spam_comment'),WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON),
		);
		
		return $routes;
	}
	
	
    public function spam_comment( $data ) {
        $request_user = $this->validation->get_user_info();
        if(!$request_user)
            return $this->validation->response("Missing header or invalid authtoken", null, 401);
	    
	    // validation of keys
	    $valid_keys = array('spam', 'resion', 'comment_id');
        
        if(!$this->validation->validateDataFormat($valid_keys, $data) ) {
            return $this->validation->response("Invalid data format");
        }
        
        if(!array_key_exists('resion', $data) || empty($data['resion'])){
            return $this->validation->response("Required resion with value");
        }
        
        if(!array_key_exists('comment_id', $data) || empty($data['comment_id'])){
            return $this->validation->response("Required comment_id with value");
        }
        $previous_comment_data = get_comment_meta($data['comment_id'], 'comment_spam_data', true);
        $data['user_id'] = $request_user->ID;
        
        $user_ids = array_column($previous_comment_data, 'user_id');
        
        $key = array_search($request_user->ID, $user_ids);
        $save_data = $previous_comment_data;
        
        if( (string) $key == '0' || (string) $key > '0') {
            $save_data = $previous_comment_data;
        }else{
            $save_data[] =$data;
        }
        
        update_comment_meta( $data['comment_id'], 'comment_spam_data', $save_data );
        
        return $this->validation->response("Success", null, 200);
    }
    
    
   
	
}