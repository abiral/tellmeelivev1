<?php

class POKER_API_Validation {
    function response($msg="Required Fields", $data=null, $status=400 ) {
        $response = new WP_JSON_Response();
        $message['status'] = $status;
		$message['data'] = $data;
		$message['message'] = $msg;

		$response->set_data( $message);
		$response->set_status($status);
		return $response;
    }
    
    
    function validateDate($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    
    function validateDataFormat( $valid_keys, $data) {
	 	$current_keys = array_keys($data);
		 
		 foreach ( $current_keys as $key ):
		 	if ( !in_array($key, $valid_keys) ):
		 		return false;
		 	endif;	
		 endforeach;
		 
		 return true;
	 }
	 
	 /**
	  * validate user's token is exits or not 
	  **/
	 function get_user_info() {
     /*
		try{
	 		$headers = apache_request_headers();
	 		$token = $headers['authtoken'];
	 	}catch (Exception $e) {
      $token = $_SERVER['HTTP_AUTHTOKEN'];

    }
    */
     $token = $_SERVER['HTTP_AUTHTOKEN'];
     
	 	if (empty($token)) return false;
	 	
	 	$users = get_users(array(
		    'meta_key'     => 'poker_authToken',
		    'meta_value'   => $token,
		    'meta_compare' => '=',
		));
		
		if (count($users)  == 1 ){
			return $users[0];
		}
		
		return false;
	 }

}

            