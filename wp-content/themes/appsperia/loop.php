<?php
   global $redux_ThemeTek; ?>
<div id="page-<?php  the_ID(); ?>" class="section">
   <div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
            <?php echo '<h2 class="section-title">' . get_the_title() . '</h2>'; ?>
            <div class="page-content"> <?php  the_content(); ?></div>
         </div>
      </div>
   </div>
</div>
<!-- / Page with ID: <?php  the_ID(); ?>-->