<?php
$parse_uri = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
require_once($parse_uri[0] . 'wp-load.php');
header("Content-type: text/css; charset: UTF-8");
?>

input[type="submit"],
.wpcf7-form textarea:focus, .wpcf7-form input:focus,
.light .wpcf7-form textarea:focus, .light .wpcf7-form input:focus,
.tabs-style-linemove nav li:last-child::before,
.sidr ul li:hover>a,
.sidr ul li:hover>span,
.sidr ul li.active>a,
.sidr ul li.active>span,
.sidr ul li.sidr-class-active>a,
.sidr ul li.sidr-class-active>span,
.sidr,
.wpcf7-form textarea:focus,
.wpcf7-form input:focus,
.wpcf7-form .wpcf7-submit,
#comments textarea:focus,
#comments input:focus,
#comments .submit,
.price-table:hover .pricing-button,
.price-table:hover .package,
.light .price-table:hover .package,
.simple-button {
background: <?php echo esc_attr($redux_ThemeTek['tek-main-color']); ?>;
}



.light .tooltip-text {
  border-bottom: 10px solid <?php echo esc_attr($redux_ThemeTek['tek-main-color']); ?>;
 }

.light .tooltip-content::after {border-top-color:<?php echo esc_attr($redux_ThemeTek['tek-main-color']); ?>; }

.entry-content a:hover,
.meta-content a:hover,
.comments-area a:hover,
.entry-meta a:hover,
.light .price-table .pricing-button a,
.clients-content .icon,
.tabs .icon,
.tooltip .icon,
#main-nav:hover span,
.specifications .icon,
#social-icons a:hover i {
	color: <?php echo esc_attr($redux_ThemeTek['tek-main-color']); ?>;
}

#social-icons a:hover i {
	border: 1px solid <?php echo esc_attr($redux_ThemeTek['tek-main-color']); ?>;
}
