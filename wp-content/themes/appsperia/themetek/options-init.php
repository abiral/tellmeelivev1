<?php
/**
ReduxFramework Sample Config File
For full documentation, please visit: https://docs.reduxframework.com
* */
if (!class_exists('Appsperia_Redux_Framework_config')) {
    class Appsperia_Redux_Framework_config
    {
        public $args = array();
        public $sections = array();
        public $theme;
        public $ReduxFramework;
        public function __construct()
        {
            if (!class_exists('ReduxFramework')) {
                return;
            }
            // This is needed. Bah WordPress bugs.  ;)
            if (true == Redux_Helpers::isTheme(__FILE__)) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array(
                    $this,
                    'initSettings'
                ), 10);
            }
        }
        public function initSettings()
        {
            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();
            // Set the default arguments
            $this->setArguments();
            // Set a few help tabs so you can see how it's done
            $this->setSections();
            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }
            // If Redux is running as a plugin, this will remove the demo notice and links
            add_action('redux/loaded', array(
                $this,
                'remove_demo'
            ));
            // Function to test the compiler hook and demo CSS output.
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            add_filter('redux/options/' . $this->args['opt_name'] . '/compiler', array(
                $this,
                'compiler_action'
            ), 10, 2);
            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );
            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );
            // Dynamically add a section. Can be also used to modify sections/fields
            //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));
            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }
        /**
        
        This is a test function that will let you see when the compiler hook occurs.
        It only runs if a field   set with compiler=>true is changed.
        
        * */
        function compiler_action($options, $css)
        {
            //echo '
            //   The compiler hook has run!';
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
            // Demo of how to use the dynamic CSS and write your own static CSS file
          
            $filename  = dirname(__FILE__) . '/dynamic-appsperia.css';
            global $wp_filesystem;
            if (empty($wp_filesystem)) {
                require_once(ABSPATH . '/wp-admin/includes/file.php');
                WP_Filesystem();
            }
            if ($wp_filesystem) {
                $wp_filesystem->put_contents($filename, $css, FS_CHMOD_FILE // predefined mode settings for WP files
                    );
            }
        }
        /**
        
        Custom function for filtering the sections array. Good for child themes to override or add to the sections.
        Simply include this function in the child themes functions.php file.
        
        NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
        so you must use get_template_directory_uri() if you want to use any of the built in icons
        
        * */
        function dynamic_section($sections)
        {
            //$sections = array();
            $sections[] = array(
                'title' => __('Section via hook', 'themetek-framework'),
                'desc' => __('
    <p class="description">
        This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.
    </p>
    ', 'themetek-framework'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );
            return $sections;
        }
        /**
        
        Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
        
        * */
        function change_arguments($args)
        {
            //$args['dev_mode'] = true;
            return $args;
        }
        /**
        
        Filter hook for filtering the default value of any given field. Very useful in development mode.
        
        * */
        function change_defaults($defaults)
        {
            $defaults['str_replace'] = 'Testing filter hook!';
            return $defaults;
        }
        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo()
        {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2);
                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(
                    ReduxFrameworkPlugin::instance(),
                    'admin_notices'
                ));
            }
        }
        public function setSections()
        {
            /**
            Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
            * */
            // Background Patterns Reader
            $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns      = array();
            if (is_dir($sample_patterns_path)):
                if ($sample_patterns_dir = opendir($sample_patterns_path)):
                    $sample_patterns = array();
                    while (($sample_patterns_file = readdir($sample_patterns_dir)) !== false) {
                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name              = explode('.', $sample_patterns_file);
                            $name              = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[] = array(
                                'alt' => $name,
                                'img' => $sample_patterns_url . $sample_patterns_file
                            );
                        }
                    }
                endif;
            endif;
            ob_start();
            $ct              = wp_get_theme();
            $this->theme     = $ct;
            $item_name       = $this->theme->get('Name');
            $tags            = $this->theme->Tags;
            $screenshot      = $this->theme->get_screenshot();
            $class           = $screenshot ? 'has-screenshot' : '';
            $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'themetek-framework'), $this->theme->display('Name'));
?>
    <div id="current-theme" class="<?php
            echo esc_attr($class);
?>
        ">
        <?php
            if ($screenshot):
?>
        <?php
                if (current_user_can('edit_theme_options')):
?>
        <a href="<?php
                    echo esc_url(wp_customize_url());
?>
            " class="load-customize hide-if-no-customize" title="
            <?php
                    echo esc_attr($customize_title);
?>
            ">
            <img src="<?php
                    echo esc_url($screenshot);
?>
            " alt="
            <?php
                    esc_attr_e('Current theme preview');
?>" /></a>
        <?php
                endif;
?>
        <img class="hide-if-customize" src="<?php
                echo esc_url($screenshot);
?>
        " alt="
        <?php
                esc_attr_e('Current theme preview');
?>
        " />
        <?php
            endif;
?>

        <h4>
            <?php
            echo esc_attr($this->theme->display('Name'));
?></h4>

        <div>
            <ul class="theme-info">
                <li>
                    <?php
            printf(__('By %s', 'themetek-framework'), $this->theme->display('Author'));
?></li>
                <li>
                    <?php
            printf(__('Version %s', 'themetek-framework'), $this->theme->display('Version'));
?></li>
                <li>
                    <?php
            echo '<strong>' . __('Tags', 'themetek-framework') . ':</strong>
                ';
?>
                <?php
            printf($this->theme->display('Tags'));
?></li>
        </ul>
        <p class="theme-description">
            <?php
            echo esc_attr($this->theme->display('Description'));
?></p>
        <?php
            if ($this->theme->parent()) {
                printf('
        <p class="howto">
            ' . __('This
            <a href="%1$s">child theme</a>
            requires its parent theme, %2$s.') . '
        </p>
        ', __('http://codex.wordpress.org/Child_Themes', 'themetek-framework'), $this->theme->parent()->display('Name'));
            }
?>
    </div>
</div>

<?php
            $item_info = ob_get_contents();
            ob_end_clean();
            $sampleHTML = '';
            if (file_exists(dirname(__FILE__) . '/info-html.html')) {
                /** @global WP_Filesystem_Direct $wp_filesystem  */
                global $wp_filesystem;
                if (empty($wp_filesystem)) {
                    require_once(ABSPATH . '/wp-admin/includes/file.php');
                    WP_Filesystem();
                }
                $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
            }
            // ACTUAL DECLARATION OF SECTIONS
            $this->sections[] = array(
                'icon' => 'el-icon-globe',
                'title' => __('Global Options', 'themetek-framework'),
                'compiler' => 'true',
                'fields' => array(
                    array(
                        'id' => 'tek-main-color',
                        'type' => 'color',
                        'title' => __('Main theme color', 'themetek-framework'),
                        'default' => '#e02217',
                        'validate' => 'color'
                    ),
                    array(
                        'id' => 'tek-scheme',
                        'type' => 'select',
                        'title' => __('Dark/Light Scheme', 'themetek-framework'),
                        'subtitle' => __('Select dark or light color scheme', 'themetek-framework'),
                        'options' => array(
                            'dark' => 'Dark',
                            'light' => 'Light'
                        ),
                        'default' => 'dark'
                    ),
                    array(
                        'id' => 'tek-logo',
                        'type' => 'media',
                        'url' => true,
                        'title' => __('Logo', 'themetek-framework'),
                        'subtitle' => __('Upload logo image', 'themetek-framework'),
                        'default' => array(
                            'url' => 'http://appsperia-wp.themetek.com/wp-content/uploads/2014/12/logo.png'
                        )
                    ),
                    array(
                        'id' => 'tek-logo-menu',
                        'type' => 'media',
                        'url' => true,
                        'title' => __('Menu Logo', 'themetek-framework'),
                        'subtitle' => __('Upload side menu logo', 'themetek-framework'),
                        'default' => array(
                            'url' => 'http://appsperia-wp.themetek.com/wp-content/uploads/2014/12/menu-logo.png'
                        )
                    ),
                    array(
                        'id' => 'tek-bgimage',
                        'type' => 'media',
                        'url' => true,
                        'title' => __('Background Image', 'themetek-framework'),
                        'subtitle' => __('Upload background image', 'themetek-framework'),
                        'default' => array(
                            'url' => 'http://appsperia-wp.themetek.com/wp-content/uploads/2014/12/bg.jpg'
                        )
                    ),
                    array(
                        'id' => 'tek-bgvideo',
                        'type' => 'media',
                        'mode' => false,
                        'url' => true,
                        'title' => __('Background Video', 'themetek-framework'),
                        'subtitle' => __('Upload background video', 'themetek-framework'),
                        'default' => array(
                            'url' => ''
                        )
                    ),                  
                           
                    array(
                        'id' => 'tek-favicon',
                        'type' => 'media',
                        'preview' => false,
                        'url' => true,
                        'title' => __('Favicon', 'themetek-framework'),
                        'subtitle' => __('Upload favicon image', 'themetek-framework'),
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/favicon.png'
                        )
                    ),
                    array(
                        'id' => 'tek-loading',
                        'type' => 'media',
                        'url' => true,
                        'title' => __('Loading image', 'themetek-framework'),
                        'compiler' => 'true',
                        'subtitle' => __('Upload image for the preloading screen', 'themetek-framework'),
                        'default' => array(
                            'url' => 'http://appsperia-wp.themetek.com/wp-content/uploads/2014/12/logo.png'
                        )
                    ),
                    
                    array(
                        'id' => 'tek-css',
                        'type' => 'ace_editor',
                        'title' => __('Custom CSS', 'themetek-framework'),
                        'subtitle' => __('Paste your CSS code here.', 'themetek-framework'),
                        'mode' => 'css',
                        'theme' => 'chrome'
                    )
                )
            );
            
            
            $this->sections[] = array(
                'icon' => 'el-icon-fontsize',
                'title' => __('Typography', 'themetek-framework'),
                'compiler' => true,
                'fields' => array(
                    array(
                        'id' => 'tek-default-typo',
                        'type' => 'typography',
                        'title' => __('Body font settings', 'themetek-framework'),
                        //'compiler'      => true,  // Use if you want to hook in your own CSS compiler
                        'google' => true, // Disable google fonts. Won't work if you haven't defined your google api key
                        // 'font-backup'   => true,    // Select a backup non-google font in addition to a google font
                        'font-style' => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        //'subsets'       => false, // Only appears if google is true and subsets not set to false
                        'font-size' => true,
                        'line-height' => true,
                        //'word-spacing'  => true,  // Defaults to false
                        //'letter-spacing'=> true,  // Defaults to false
                        'color' => true,
                        'text-align' => true,
                        'preview' => true, // Disable the previewer
                        'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
                        'compiler' => array(
                            'body, .box'
                        ), // An array of CSS selectors to apply this font style to dynamically
                        // 'compiler'      => array('h2.site-description-compiler'), // An array of CSS selectors to apply this font style to dynamically
                        'units' => 'px', // Defaults to px
                        'default' => array(
                            'color' => '#fff',
                            'font-weight' => '300',
                            'font-family' => 'Open Sans',
                            'google' => true,
                            'font-size' => '14px',
                            'text-align' => 'left',
                            'line-height' => '24px'
                        ),
                        'preview' => array(
                            'text' => 'AppSperia Sample Text'
                        )
                    ),
                    array(
                        'id' => 'tek-heading-typo',
                        'type' => 'typography',
                        'title' => __('Headings font settings', 'themetek-framework'),
                        //'compiler'      => true,  // Use if you want to hook in your own CSS compiler
                        'google' => true, // Disable google fonts. Won't work if you haven't defined your google api key
                        // 'font-backup'   => true,    // Select a backup non-google font in addition to a google font
                        'font-style' => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        //'subsets'       => false, // Only appears if google is true and subsets not set to false
                        'font-size' => true,
                        'line-height' => true,
                        //'word-spacing'  => true,  // Defaults to false
                        //'letter-spacing'=> true,  // Defaults to false
                        'color' => true,
                        'text-align' => true,
                        'preview' => true, // Disable the previewer
                        'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
                        'compiler' => array(
                            '.wrap h1,.wrap h2,.wrap h3'
                        ), // An array of CSS selectors to apply this font style to dynamically
                        // 'compiler'      => array('h2.site-description-compiler'), // An array of CSS selectors to apply this font style to dynamically
                        'units' => 'px', // Defaults to px
                        'default' => array(
                            'color' => '#fff',
                            'font-weight' => '400',
                            'font-family' => 'walkway',
                            'google' => true,
                            'font-size' => '50px',
                            'text-align' => 'left',
                            'line-height' => '60px'
                        ),
                        'preview' => array(
                            'text' => 'AppSperia Sample Text'
                        )
                    ),
                    array(
                        'id' => 'tek-heading-bold-typo',
                        'type' => 'typography',
                        'title' => __('Headings bold font setting', 'themetek-framework'),
                        //'compiler'      => true,  // Use if you want to hook in your own CSS compiler
                        'google' => true, // Disable google fonts. Won't work if you haven't defined your google api key
                        // 'font-backup'   => true,    // Select a backup non-google font in addition to a google font
                        'font-style' => false, // Includes font-style and weight. Can use font-style or font-weight to declare
                        //'subsets'       => false, // Only appears if google is true and subsets not set to false
                        'font-size' => false,
                        'line-height' => false,
                        //'word-spacing'  => true,  // Defaults to false
                        //'letter-spacing'=> true,  // Defaults to false
                        'color' => false,
                        'text-align' => false,
                        'preview' => false, // Disable the previewer
                        'all_styles' => false, // Enable all Google Font style/weight variations to be added to the page
                        'compiler' => array(
                            '.wrap h1 strong ,.wrap h2 strong, .wrap h3 strong'
                        ), // An array of CSS selectors to apply this font style to dynamically
                        // 'compiler'      => array('h2.site-description-compiler'), // An array of CSS selectors to apply this font style to dynamically
                        'units' => 'px', // Defaults to px
                        'default' => array(
                                                       'font-family' => 'walkway_bold'
                                                  )
                       
                    )
                    
                )
            );
           
            $this->sections[] = array(
                'icon' => 'el-icon-pencil-alt',
                'title' => __('Blog', 'themetek-framework'),
                'fields' => array(
                    array(
                        'id' => 'tek-blog-default',
                        'type' => 'media',
                        'url' => true,
                        'title' => __('Default Image', 'themetek-framework'),
                        'subtitle' => __('Upload article default featured image', 'themetek-framework'),
                        'default' => array(
                            'url' => 'http://appsperia-wp.themetek.com/wp-content/uploads/2014/12/1.jpg'
                        )
                    ),
                    array(
                        'id' => 'tek-blog-comments',
                        'type' => 'switch',
                        'title' => __('Display comments', 'themetek-framework'),
                        'subtitle' => __('Turn on/off blog comments', 'themetek-framework'),
                        'default' => true
                    )
                )
            );
            $this->sections[] = array(
                'icon' => 'el-icon-error-alt',
                'title' => __('404 Page', 'themetek-framework'),
                'fields' => array(
                    array(
                        'id' => 'tek-404-title',
                        'type' => 'text',
                        'title' => __('Title', 'themetek-framework'),
                        'default' => 'Error 404'
                        //                    
                    ),
                    array(
                        'id' => 'tek-404-subtitle',
                        'type' => 'text',
                        'title' => __('Subtitle', 'themetek-framework'),
                        'default' => 'This page could not be found!'
                        //                    
                    ),
                    array(
                        'id' => 'tek-404-back',
                        'type' => 'text',
                        'title' => __('Back to homepage text', 'themetek-framework'),
                        'default' => 'Back to homepage'
                        //                    
                    )
                )
            );
            $this->sections[] = array(
                'icon' => 'el-icon-thumbs-up',
                'title' => __('Social', 'themetek-framework'),
                'fields' => array(
                   
                    array(
                        'id' => 'tek-social-icons',
                        'type' => 'checkbox',
                        'title' => __('Social Icons', 'themetek-framework'),
                        'subtitle' => __('Select visible social icons', 'themetek-framework'),
                        //Must provide key => value pairs for multi checkbox options
                        'options' => array(
                            '1' => 'Facebook',
                            '2' => 'Twitter',
                            '3' => 'Google+',
                            '4' => 'Pinterest',
                            '5' => 'Youtube',
                            '6' => 'Vimeo'
                        ),
                        //See how std has changed? you also don't need to specify opts that are 0.
                        'default' => array(
                            '1' => '1',
                            '2' => '1',
                            '3' => '1',
                            '4' => '1',
                            '5' => '1',
                            '6' => '1'
                        )
                    ),
                    array(
                        'id' => 'tek-facebook-url',
                        'type' => 'text',
                        'title' => __('Facebook Link', 'themetek-framework'),
                        'subtitle' => __('Enter Facebook url', 'themetek-framework'),
                        'validate' => 'url',
                        'default' => 'http://facebook.com'
                    ),
                   
                    array(
                        'id' => 'tek-twitter-url',
                        'type' => 'text',
                        'title' => __('Twitter Link', 'themetek-framework'),
                        'subtitle' => __('Enter Twitter url', 'themetek-framework'),
                        'validate' => 'url',
                        'default' => 'http://twitter.com'
                    ),
                    
                    array(
                        'id' => 'tek-google-url',
                        'type' => 'text',
                        'title' => __('Google+ Link', 'themetek-framework'),
                        'subtitle' => __('Enter Google+ url', 'themetek-framework'),
                        'validate' => 'url',
                        'default' => 'http://plus.google.com'
                    ),
                    array(
                        'id' => 'tek-pinterest-url',
                        'type' => 'text',
                        'title' => __('Pinterest Link', 'themetek-framework'),
                        'subtitle' => __('Enter Pinterest url', 'themetek-framework'),
                        'validate' => 'url',
                        'default' => 'http://pinterest.com'
                    ),
                   
                    array(
                        'id' => 'tek-youtube-url',
                        'type' => 'text',
                        'title' => __('Youtube Link', 'themetek-framework'),
                        'subtitle' => __('Enter Youtube url', 'themetek-framework'),
                        'validate' => 'url',
                        'default' => 'http://youtube.com'
                    ),
                    array(
                        'id' => 'tek-vimeo-url',
                        'type' => 'text',
                        'title' => __('Vimeo Link', 'themetek-framework'),
                        'subtitle' => __('Enter Vimeo url', 'themetek-framework'),
                        'validate' => 'url',
                        'default' => 'http://vimeo.com'
                    ),
                    
                )
            );
            $this->sections[] = array(
                'title' => __('Import/Export ', 'themetek-framework'),
                'desc' => __('', 'themetek-framework'),
                'icon' => 'el-icon-magic',
                'fields' => array(
                    array(
                        'id' => 'opt-import-export',
                        'type' => 'import_export',
                        'title' => 'Import and Export AppSperia Options',
                        'subtitle' => '',
                        'full_width' => false
                    )
                )
            );
        }
        /**
        
        All the possible arguments for Redux.
        For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
        
        * */
        public function setArguments()
        {
            $theme                         = wp_get_theme(); // For use with some settings. Not necessary.
            $this->args                    = array(
                'opt_name' => 'redux_ThemeTek',
                'page_slug' => 'options_themetek',
                'page_title' => 'ThemeTek Options',
                'dev_mode' => '0',
                'update_notice' => '1',
                'admin_bar' => '1',
                'menu_type' => 'submenu',
                'page_parent' => 'themes.php',
                'menu_title' => 'Theme Options',
                'allow_sub_menu' => '1',
                'page_parent_post_type' => 'your_post_type',
                'customizer' => '1',
                'class' => '',
                'hints' => array(
                    'icon' => 'el-icon-question-sign',
                    'icon_position' => 'right',
                    'icon_size' => 'normal',
                    'tip_style' => array(
                        'color' => 'light'
                    ),
                    'tip_position' => array(
                        'my' => 'top left',
                        'at' => 'bottom right'
                    ),
                    'tip_effect' => array(
                        'show' => array(
                            'duration' => '500',
                            'event' => 'mouseover'
                        ),
                        'hide' => array(
                            'duration' => '500',
                            'event' => 'mouseleave unfocus'
                        )
                    )
                ),
                'output' => '1',
                'output_tag' => '1',
                'compiler' => '1',
                'page_icon' => 'icon-themes',
                'page_permissions' => 'manage_options',
                'save_defaults' => '1',
                'show_import_export' => '1',
                'transient_time' => '3600',
                'network_sites' => '1'
            );
            $theme                         = wp_get_theme(); // For use with some settings. Not necessary.
            $this->args["display_name"]    = $theme->get("Name");
            $this->args["display_version"] = $theme->get("Version");
         
        }
    }
    global $reduxConfig;
    $reduxConfig = new Appsperia_Redux_Framework_config();
}
/**
Custom function for the callback referenced above
*/
if (!function_exists('Appsperia_my_custom_field')):
    function Appsperia_my_custom_field($field, $value)
    {
        print_r($field);
        echo '
<br/>
';
        print_r($value);
    }
endif;
/**
Custom function for the callback validation referenced above
* */
if (!function_exists('Appsperia_validate_callback_function')):
    function Appsperia_validate_callback_function($field, $value, $existing_value)
    {
        $error           = false;
        $value           = 'just testing';
        /*
        do your validation
        
        if(something) {
        $value = $value;
        } elseif(something else) {
        $error = true;
        $value = $existing_value;
        $field['msg'] = 'your custom error message';
        }
        */
        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;