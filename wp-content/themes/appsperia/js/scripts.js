jQuery(document).ready(function($) {
    $('#main-nav').sidr();


$(document).mousemove(function() { 
    if($('.entry-content:hover').length > 0) {
        $('#fullpage').fullpage.setAllowScrolling(false);        
    }
    else {
        $('#fullpage').fullpage.setAllowScrolling(true);        
    }
    if($('.entry-content.no-scroll:hover').length > 0) {
        $('#fullpage').fullpage.setAllowScrolling(true);        
    }
});


       if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
         $('body').addClass('browser-mobile');
    }


$(".tooltip-wrap").parents(".page-content").addClass('with-tooltip');


$(".searchform label").each(function() {
    var label = $(this);
    var placeholder = label.text();
    label.closest("div").find("#s").attr("placeholder", placeholder).val("").focus().blur();
});
$('#searchsubmit').val('GO');


    $( '<div class="separator"></div>').insertBefore( ".section .tooltip-wrap:nth-of-type(1)" );
    $('.fp-tableCell').css('height', $(window).height());
    $('#menu-main-menu li a').click(function() {
        $('#menu-main-menu li').removeClass('active');
        $(this).parent().addClass('active');
    });
    $('h2.section-title').each(function(){
     var me = $(this);
     me.html(me.html().replace(/^(\w+)/, '<strong>$1</strong>'));
    });
    $(document).mouseup(function(e) {
        if ($(".sidr-open ")[0]) {
            var container = $("#sidr");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                $(".sidr-open #main-nav").click();
            }
        }
    });
    $('.tabs-style-linemove').each(function() {
        var tabs_count = $(this).find("li").length;
        $(this).addClass('tabs' + tabs_count);
    });

    $('#menu-main-menu li.one-page-link > a:first-child').each(function() {
        var href = $(this).attr("href");
        link = href.replace(/\/$/, '');
        if ($("body").hasClass("home")) {
            anchor = link.substr(link.lastIndexOf('/') + 1);
            $(this).attr("href", '#' + anchor);
        } else {
            if (!$(this).hasClass("first")) {
                var pos = link.lastIndexOf('/');
                link = link.substring(0, pos) + '#' + link.substring(pos + 1)
                $(this).attr("href", link);
            }
        }
    });
});

jQuery(window).load(function() {
    jQuery('#loading').fadeOut();


    jQuery(".entry-content").each(function() {
    var minHeight = 205;
    if ( jQuery(this).height() < minHeight) {
    jQuery(this).addClass('no-scroll');
    }
    });


    jQuery(".page .entry-content").each(function() {
    var minHeight = 315;
    if ( jQuery(this).height() < minHeight) {
    jQuery(this).addClass('no-scroll');
    }
    });




});
(function() {
    [].slice.call(document.querySelectorAll('.tabs')).forEach(function(el) {
        new CBPFWTabs(el);
    });
})();