<?php
   /*
     * Appsperia Theme
     * Author: ThemeTek.com
     * File: header.php
     */global $redux_ThemeTek; ?>
<!DOCTYPE html>
<html style="background-image: url(<?php  echo esc_url($redux_ThemeTek['tek-bgimage']['url']); ?>); " <?php  language_attributes(); ?>>
   <head>
      <meta charset="<?php  bloginfo( 'charset' ); ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php  wp_title(''); ?></title>
      <link href=" <?php  echo esc_url($redux_ThemeTek['tek-favicon']['url']); ?>" rel="icon">
      <link rel="pingback" href="<?php  bloginfo( 'pingback_url' ); ?>" />
      <?php  wp_head(); ?>
      <style type="text/css"><?php
      if (isset($redux_ThemeTek['tek-css'] ))   {  echo esc_attr( $redux_ThemeTek['tek-css'] ); }  ?></style>
      <script type='text/javascript'>
        
           jQuery(document).ready(function($) {
            $('#fullpage').fullpage({
            'anchors': [<?php 
            $pagetitle = get_the_title();
            
            if($pagetitle == 'Home') {
              $home =  get_page_by_title( 'Home' );
              $args = array(    'post_type' => 'page',    'post_status' => 'publish',    'parent' => $home->ID,    'sort_column'=>'menu_order');
              $pages = get_pages($args);
              foreach ( $pages as $page  ) {
                $pgs[]="'".str_replace(' ','-',trim(strtolower(get_the_title($page->ID))))."'";
              }
            
              echo str_replace('&#039;',"'",esc_attr(implode(',',$pgs)));
            }
            
            ?>],
                 'verticalCentered': true,
                 'easing': 'easeInOutCirc',
                 'css3': false,
                 'scrollingSpeed': 900,
                 'slidesNavigation': true,
                 'slidesNavPosition': 'bottom',
                 'easingcss3': 'ease',
                 'navigation': true,        
                 'navigationPosition': 'left'
             });
            });
      </script>
   </head>
   <body <?php  body_class($redux_ThemeTek['tek-scheme']); ?> >
      <!-- color customizer panel  -->
      <div id="loading"><img src="<?php  echo esc_url($redux_ThemeTek['tek-loading']['url']);   ?>" alt="" /></div>
      <?php  if ($redux_ThemeTek['tek-bgvideo']['url']): ?> 
      <div class="overlay"><input type="hidden" id="bg-video" value="<?php  echo esc_url($redux_ThemeTek['tek-bgvideo']['url']);   ?>"></div>
      <?php  endif; ?>
      <div class="wrap">
         <div id="logo">
            <a class="logo" href="<?php  echo esc_url(site_url()); ?>">
            <?php  if ($redux_ThemeTek['tek-logo']['url']) { ?>
           <img class="fixed-logo" src="<?php  echo esc_url($redux_ThemeTek['tek-logo']['url']);   ?>" alt="" />
            <?php  } else  bloginfo('name')  ?></a>
         </div>
      </div>
      <a id="main-nav" href="#sidr"><span class="flaticon-menu9"></span></a> 
      <div id="sidr" class="sidr">
         <a href="<?php  echo esc_url(site_url()); ?>" id="menu-logo"><img src="<?php  echo esc_url($redux_ThemeTek['tek-logo-menu']['url']);  ?>" alt=""></a>
         <?php get_search_form(); ?>
         <div class="menu-overlay"></div>
         <nav class="navbar navbar-default navbar-fixed-top" role="navigation" >
            <div class="container">
               <!-- Brand and toggle get grouped for better mobile display -->
               <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="collapse navbar-collapse navbar-themetek">
                  <?php 
                     /* Primary navigation */
                     wp_nav_menu( array(              'menu' =>          'header-menu',              'depth' => 1,              'container' => false,              'menu_class' => 'nav navbar-nav',              'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',              'walker' => new wp_bootstrap_navwalker())            );
                     ?>
               </div>
            </div>
         </nav>
      </div>
      <div id="fullpage">
      <div class="wrap">
         <div class="section-image">
            <?php 
               if (is_page('Home')) {
                 $args = array(    'post_type' => 'page',    'post_status' => 'publish',    'sort_column'=>'menu_order');
                 $pages = get_pages($args);
                 foreach($pages as $page) {
                   echo get_the_post_thumbnail( $page->ID, 'post-thumbnails' );
                 }
               
               } else
               if (is_single()) {
                 
                 if ( has_post_thumbnail() ) {
                   echo get_the_post_thumbnail( $post->ID, 'post-thumbnails' );
                 } else {
                   ?>
            <img src="<?php  echo esc_url($redux_ThemeTek['tek-blog-default']['url']); ?>" alt="" />
            <?php
               }
               
               } else {
              
               $args = array(    'post_type' => 'post');
               $query = new WP_Query($args);

              while($query->have_posts()) {
                 $query->the_post();
                 
                 if ( has_post_thumbnail() ) {
                   the_post_thumbnail('post-thumbnails');
                 } else {
                   ?>
                <img src="<?php  echo esc_url($redux_ThemeTek['tek-blog-default']['url']); ?>" alt="" />
               <?php
               }
               
               }
               
               }
               
               ?>
         </div>
         <div id="hand"></div>
      </div>