<?php  global $redux_ThemeTek; ?>
</div>
  <div class="wrap">
        <div id="social-icons">
            <ul>
               <?php  if ($redux_ThemeTek['tek-social-icons'][1] == 1): ?> <li><a href="<?php  echo esc_url($redux_ThemeTek['tek-facebook-url'])  ?>" target="_blank"><i class="flaticon-facebook6"></i></a> </li><?php  endif  ?>
               <?php  if ($redux_ThemeTek['tek-social-icons'][2] == 1): ?> <li><a href="<?php  echo esc_url($redux_ThemeTek['tek-twitter-url'])  ?>" target="_blank"><i class="flaticon-social19"></i></a> </li><?php  endif  ?>
               <?php  if ($redux_ThemeTek['tek-social-icons'][3] == 1): ?><li><a href="<?php  echo esc_url($redux_ThemeTek['tek-google-url'])  ?>" target="_blank"><i class="flaticon-google16"></i></a> </li><?php  endif  ?>
               <?php  if ($redux_ThemeTek['tek-social-icons'][4] == 1): ?><li><a href="<?php  echo esc_url($redux_ThemeTek['tek-pinterest-url'])  ?>" target="_blank"><i class="flaticon-social40"></i></a> </li><?php  endif  ?>
               <?php  if ($redux_ThemeTek['tek-social-icons'][5] == 1): ?><li><a href="<?php  echo esc_url($redux_ThemeTek['tek-youtube-url'])  ?>" target="_blank"><i class="flaticon-social7"></i></a> </li><?php  endif  ?>
               <?php  if ($redux_ThemeTek['tek-social-icons'][6] == 1): ?> <li><a href="<?php  echo esc_url($redux_ThemeTek['tek-vimeo-url'])  ?>" target="_blank"><i class="flaticon-logo3"></i></a> </li><?php  endif  ?>
            </ul>
        </div>
    </div>
  <?php  wp_footer(); ?>
  </body>
</html>