<?php 
   /*
   * AppSperia Theme
   * Author: Key-Design
   * File: archive.php
   */
   get_header();
   ?>
<?php
   if(get_query_var('author_name')) :
   $curauth = get_user_by('slug', get_query_var('author_name')); else :
   $curauth = get_userdata(get_query_var('author'));
   endif;
   ?>
<!-- blog content -->
<div id="blog-posts" class="container">
   
      <!-- posts -->
      <div id="posts-content" class="col-md-9">
         <?php 
            if (have_posts()) :
            while (have_posts()) :
            the_post();
            ?>
         <div <?php  post_class('section'); ?> id="post-<?php  the_ID(); ?>" >
            <div class="wrap" >
               <div class="fp-tableCell">
                  <div class="box" >
                     <!-- archive.php -->
                     <?php  if ( is_category() ) { ?>
                     <h4 class="category-title">
                     <?php  _e('Currently browsing', 'themetek_lang')  ?>: <?php  single_cat_title(); ?></h4>
                     <?php 
                        }
                        
                        elseif ( is_tag() ) {
                          ?>
                     <h4 class="category-title">
                     <?php  _e('All posts tagged', 'themetek_lang')  ?>: <?php  single_tag_title(); ?></h4>
                     <?php 
                        }
                        
                        elseif ( is_author() ) {
                          ?>
                     <h4 class="category-title">
                     <?php  _e('All posts by', 'themetek_lang')  ?> <?php  echo esc_attr($curauth->display_name); ?></h4>
                     <?php 
                        }
                        
                        elseif ( is_day() ) {
                          ?>
                     <h4 class="category-title">
                     <?php  _e('Posts archive for', 'themetek_lang')  ?> <?php  echo get_the_date('F jS, Y'); ?></h4>
                     <?php 
                        }
                        
                        elseif ( is_month() ) {
                          ?>
                     <h4 class="category-title">
                     <?php  _e('Posts archive for', 'themetek_lang')  ?> <?php  echo get_the_date('F, Y'); ?></h4>
                     <?php 
                        }
                        
                        elseif ( is_year() ) {
                          ?>
                     <h4 class="category-title">
                     <?php  _e('Posts archive for', 'themetek_lang')  ?> <?php  echo get_the_date('Y'); ?></h4>
                     <?php  } ?>
                             
                     <h1 class="blog-title"><?php  the_title(); ?></h1>
                     <div class="entry-meta">
                        <span class="blog-icon fa-user"> </span><span class="author"><?php  the_author_posts_link(); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
                        <span class="blog-icon fa-calendar"></span><span class="published"><?php  the_time( get_option('date_format') ); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
                        <span class="blog-icon fa-comment"></span><span class="comment-count"><?php  comments_popup_link( __('No comments yet', 'themetek_lang'), __('1 comment', 'themetek_lang'), __('% comments', 'themetek_lang') ); ?></span>
                     </div>
                     <div class="entry-content">
                        <?php  if(has_excerpt()) : ?>
                        <?php  the_excerpt(); ?>
                        <?php  else : ?>
                       <div class="page-content"> <?php  the_content(); ?></div>
                        <?php  endif; ?>
                     </div>
                     <div class="more-button"><a href="<?php  the_permalink(); ?>" title="<?php  printf(__('Permanent link to %s', 'themetek_lang'), get_the_title()); ?>" class="simple-button"><span class="icon flaticon-double4"></span>Read more</a></div>
                  </div>
               </div>
            </div>
         </div>
         <?php  endwhile; ?>
           <?php            
                   the_posts_pagination( array(                  
                   'prev_text'          => __( '<span class="icon flaticon-double5"></span>', 'appsperia' ),
                   'next_text'          => __( '<span class="icon flaticon-double4"></span>', 'appsperia' ),
                   'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '&nbsp;', 'appsperia' ) . ' </span>',
  ) ); ?>
         <?php  else : ?>
         <div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
         <div id="post-not-found" <?php  post_class(); ?>>
            <h2 class="entry-title"><?php  _e('Error 404 - Not Found', 'themetek_lang')  ?></h2>
            <div class="entry-content">
               <p><?php  _e("Sorry, no posts matched your criteria.", "themetek_lang")  ?></p>
            </div>
         </div>
            </div>
      </div>
   </div>
         <?php  endif; ?>
      </div>
   
</div>
<!-- archive content end -->
<?php  get_footer(); ?>