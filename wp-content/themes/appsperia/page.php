<?php
   global $redux_ThemeTek;get_header(); 
   $page_title = $wp_query->post->post_title; ?>

<div class="wrap" >
   <div class="fp-tableCell">
      <div class="box" >
        <h2 class="section-title"><?php echo $page_title; ?></h2>
          <?php  if ( have_posts() ) :
            while ( have_posts() ) :
            the_post();
            ?>
         <div class="entry-content"><div class="page-content"> <?php  the_content(); ?></div></div>
         <?php  endwhile; else : ?>
         <p><?php  _e('Sorry, this page does not exist.', 'themetek_lang'); ?></p>
         <?php  endif; ?>
      </div>
   </div>
</div>
<?php  get_footer(); ?>