<?php 
   /*
   * Appsperia Theme
   * Author: ThemeTek.com
   * File: single.php
   */
   get_header();
   ?>
<div class="blog-single">
   <div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
            <?php 
               if (have_posts()) :
               while (have_posts()) :
               the_post();
               ?>
            <div <?php  post_class(); ?> id="post-<?php  the_ID(); ?>">
               <div class="blog-single-content">
                  <h1 class="blog-title"><?php  the_title(); ?></h1>
                  <div class="entry-meta">     
                    <?php if ( is_sticky() ) echo '<span class="flaticon-pushpin1 icon"></span> Sticky &nbsp;&nbsp;|&nbsp;&nbsp;'; ?>                
                     <span class="author"><?php  the_author_posts_link(); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
                     <span class="published"><?php  the_time( get_option('date_format') ); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
                     <span class="comment-count"><?php  comments_popup_link( __('No comments yet', 'themetek_lang'), __('1 comment', 'themetek_lang'), __('% comments', 'themetek_lang') ); ?></span>
                  </div>
                   <div class="meta-content">
                  <div class="tags"><span class="blog-label">Tags:</span> <?php  the_tags(''); ?></div><div class="tags">&nbsp;&nbsp;|&nbsp;&nbsp;</div>
                  <div class="tags"><span class="blog-label">Categories:</span> <?php  the_category(', '); ?></div>
               </div>
               </div>
            </div>
            <div class="tabs tabs-style-linemove tabs2">
               <nav>
                  <ul>
                     <li class="tab-current"><a href="#section-linemove-0"><span class="icon flaticon-documents"></span><span> Article</span></a></li>
                     <li><a href="#section-linemove-1"><span class="icon flaticon-comment2"></span><span> Comments</span></a></li>
                  </ul>
               </nav>
               <div class="content-wrap">
                  <section id="section-linemove-0" class="content-current">
                     <div class="entry-content">
                       <div class="page-content"> <?php  the_content(); wp_link_pages(); ?> </div>

                     </div>
                  </section>
                  <section id="section-linemove-1">
                      <div class="entry-content comments-content">
                     <div class="page-content comments-content">
                     <?php 
                        if ($redux_ThemeTek['tek-blog-comments']) {
                        	comments_template('', true);
                        }
                        
                        ?>
                     </div>
                  </div>
                  </section>
               </div>
            </div>
            <?php  endwhile; ?>
            <?php  else : ?>
            <div id="post-not-found" <?php  post_class()   ?>>
               <h1 class="entry-title"><?php  _e('Error 404 - Not Found', 'themetek_lang')   ?></h1>
               <div class="entry-content">
                  <p><?php  _e("Sorry, but you are looking for something that isn't here.", "themetek_lang")   ?></p>
               </div>
            </div>
            <?php  endif; ?>
         </div>
      </div>
   </div>
</div>

<?php  get_footer(); ?>