<?php 
  global $redux_ThemeTek;
  get_header();
  ?>
<?php  if( is_home() ) : ?>
<div id="posts-content">
<?php 
  
  if (have_posts()) :
  while (have_posts()) :
  the_post();
  ?>
<div <?php  post_class('section'); ?> id="post-<?php  the_ID(); ?>" >
   <div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
            <h2 class="section-title"><?php  the_title(); ?></h2>
            <div class="entry-meta">
               <span class="blog-icon fa-user"> </span><span class="author"><?php  the_author_posts_link(); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
               <span class="blog-icon fa-calendar"></span><span class="published"><?php  the_time( get_option('date_format') ); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
               <span class="blog-icon fa-comment"></span><span class="comment-count"><?php  comments_popup_link( __('No comments yet', 'themetek_lang'), __('1 comment', 'themetek_lang'), __('% comments', 'themetek_lang') ); ?></span>
            </div>
            <div class="entry-content">
               <?php  if(has_excerpt()) : ?>
               <?php  the_excerpt(); ?>
               <?php  else : ?>
               <div class="page-content"><?php  the_content(); ?></div>
               
               <?php  endif; ?>
            </div>
            <div class="more-button"><a href="<?php  the_permalink(); ?>" title="<?php  printf(__('Permanent link to %s', 'themetek_lang'), get_the_title()); ?>" class="simple-button"><span class="icon flaticon-double4"></span>Read more</a></div>
           </div>
      </div>
   </div>
</div>
<?php  endwhile; ?>
  <?php            
                   the_posts_pagination( array(                  
                   'prev_text'          => __( '<span class="icon flaticon-double5"></span>', 'appsperia' ),
                   'next_text'          => __( '<span class="icon flaticon-double4"></span>', 'appsperia' ),
                   'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '&nbsp;', 'appsperia' ) . ' </span>',
  ) ); ?>
<?php  else : ?>
<div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
<div id="post-not-found" <?php  post_class(); ?>>
   <h2 class="entry-title"><?php  _e('Error 404 - Not Found', 'themetek_lang')     ?></h2>
   <div class="entry-content">
      <p><?php  _e("Sorry, no posts matched your criteria.", "themetek_lang")     ?></p>
   </div>
</div>
   </div>
      </div>
   </div>
<?php  endif; ?>
<!-- / posts --> 
<?php  endif; ?>


<?php  get_footer(); ?>