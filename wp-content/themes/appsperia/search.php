<?php
   get_header(); ?>
<div id="posts-content">
   <?php if ( have_posts() ) : ?>
   <?php while ( have_posts() ) : the_post(); ?>
   <div <?php  post_class('section'); ?> id="post-<?php  the_ID(); ?>" >
      <div class="wrap" >
         <div class="fp-tableCell">
            <div class="box" >
               <strong class="search-title"><?php printf( __( 'Search Results for: %s', 'appsperia' ), get_search_query() ); ?></strong>
               <h2 class="section-title"><?php  the_title(); ?></h2>
               <div class="entry-meta">
                  <?php if ( is_sticky() ) echo '<span class="flaticon-pushpin1 icon"></span> Sticky &nbsp;&nbsp;|&nbsp;&nbsp;'; ?>
                  <span class="author"><?php  the_author_posts_link(); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
                  <span class="published"><?php  the_time( get_option('date_format') ); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
                  <span class="comment-count"><?php  comments_popup_link( __('No comments yet', 'themetek_lang'), __('1 comment', 'themetek_lang'), __('% comments', 'themetek_lang') ); ?></span>
               </div>
               <div class="entry-content">
                  <?php  if(has_excerpt()) : ?>
                  <?php  the_excerpt(); ?>
                  <?php  else : ?>
                  <div class="page-content"><?php  the_content(); ?></div>
                  <?php  endif; ?>
               </div>
               <div class="more-button"><a href="<?php  the_permalink(); ?>" title="<?php  printf(__('Permanent link to %s', 'themetek_lang'), get_the_title()); ?>" class="simple-button"><span class="icon flaticon-double4"></span>Read more</a></div>
            </div>
         </div>
      </div>
   </div>
   <?php 
      endwhile;  
           
                   the_posts_pagination( array(                  
                   'prev_text'          => __( '<span class="icon flaticon-double5"></span>', 'appsperia' ),
                   'next_text'          => __( '<span class="icon flaticon-double4"></span>', 'appsperia' ),
                   'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '&nbsp;', 'appsperia' ) . ' </span>',
      ) );    
      
      else : ?>
   <div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
            <h2 class="section-title"><?php _e( 'Nothing Found', 'appsperia' ); ?></h2>
            <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
            <p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'appsperia' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>
            <?php elseif ( is_search() ) : ?>
            <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'appsperia' ); ?></p>
            <?php get_search_form(); ?>
            <?php else : ?>
            <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'appsperia' ); ?></p>
            <?php get_search_form(); ?>
            <?php endif; ?>
         </div>
      </div>
   </div>
   <?php endif;
      ?>
</div>
<?php get_footer(); ?>