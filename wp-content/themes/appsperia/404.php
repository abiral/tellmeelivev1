<?php
   /**
   * The template for displaying 404 pages (Not Found)
   */
   get_header(); ?>
   
<div id="page-<?php the_ID(); ?>" class="section">
   <div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
            <h1><?php  echo esc_attr($redux_ThemeTek['tek-404-title'])  ?></h1>
            <h4><?php  echo esc_attr($redux_ThemeTek['tek-404-subtitle']) ?></h4>
            <div class="button-wrap"><a href="<?php echo get_site_url(); ?>" class="simple-button" target="_self"> <span class="icon flaticon-arrow72"></span> <?php  echo esc_attr($redux_ThemeTek['tek-404-back'])  ?></a></div>
         </div>
         <!-- .page-content -->
      </div>
      <!-- .page-wrapper -->
   </div>
   <!-- #content -->
</div>
<!-- #primary -->
<?php  get_footer(); ?>