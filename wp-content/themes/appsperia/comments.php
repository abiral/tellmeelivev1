<?php
   if (post_password_required()) {
       return;
   }
   
   ?>
<div id="comments" class="comments-area">
   <?php
      if (!have_comments()) {echo "There are no comments yet.";}
      if (have_comments()):
      ?>
   <?php
      if (get_comment_pages_count() > 1 && get_option('page_comments')):
      ?>
   <nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
      <h1 class="screen-reader-text"><?php
         _e('Comment navigation', 'themetek_lang');
         ?></h1>
      <div class="nav-previous"><?php
         previous_comments_link(__('&larr; Older Comments', 'themetek_lang'));
         ?></div>
      <div class="nav-next"><?php
         next_comments_link(__('Newer Comments &rarr;', 'themetek_lang'));
         ?></div>
   </nav>
   <!-- #comment-nav-above -->
   <?php
      endif; // Check for comment navigation.  
      ?>
   <ul class="comment-list">
      <?php
         wp_list_comments(array(
             'style' => 'ul',
             'short_ping' => true,
             'avatar_size' => 84
         ));
         ?>
   </ul>
   <!-- .comment-list -->
   <?php
      if (get_comment_pages_count() > 1 && get_option('page_comments')):
      ?>
   <nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
      <h1 class="screen-reader-text"><?php
         _e('Comment navigation', 'themetek_lang');
         ?></h1>
      <div class="nav-previous"><?php
         previous_comments_link(__('&larr; Older Comments', 'themetek_lang'));
         ?></div>
      <div class="nav-next"><?php
         next_comments_link(__('Newer Comments &rarr;', 'themetek_lang'));
         ?></div>
   </nav>
   <!-- #comment-nav-below -->
   <?php
      endif; // Check for comment navigation.  
      ?>
   <?php
      if (!comments_open()):
      ?>
   <p class="no-comments"><?php
      _e('Comments are closed.', 'themetek_lang');
      ?></p>
   <?php
      endif;
      ?>
   <?php
      endif; // have_comments()  
      ?>
   <?php
      $commenter     = wp_get_current_commenter();
      $args          = array(
          'id_form' => 'commentform',
          'id_submit' => 'submit',
          'title_reply' => __('', 'themetek_lang'),
          'title_reply_to' => __('Leave a Reply to %s', 'themetek_lang'),
          'cancel_reply_link' => __('Cancel Reply', 'themetek_lang'),
          'label_submit' => __('Post Comment', 'themetek_lang'),
          'comment_field' => '<p class="comment-form-comment"><textarea placeholder="Comment" id="comment" name="comment" cols="45" rows="8" aria-required="true">' . '</textarea></p>',
          'comment_notes_after' => '',
          'fields' => apply_filters('comment_form_default_fields', array(
              'author' => '<p class="comment-form-author"><input placeholder="Name" id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30" /></p>',
              'email' => '<p class="comment-form-email"><input placeholder="Email" id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" /></p>'
          ))
      );
      comment_form($args, get_the_ID());
      ?> 
</div>
<!-- #comments -->