<div <?php  post_class('section'); ?> id="post-<?php  the_ID(); ?>" >
   <div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
            <h2 class="section-title"><?php  the_title(); ?></h2>
            <div class="entry-meta">
            <?php if ( is_sticky() ) echo '<span class="flaticon-pushpin1 icon"></span> Sticky &nbsp;&nbsp;|&nbsp;&nbsp;'; ?>
            <span class="author"><?php  the_author_posts_link(); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
            <span class="published"><?php  the_time( get_option('date_format') ); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
            <span class="comment-count"><?php  comments_popup_link( __('No comments yet', 'themetek_lang'), __('1 comment', 'themetek_lang'), __('% comments', 'themetek_lang') ); ?></span>
            </div>
            <div class="entry-content">
               <?php  if(has_excerpt()) : ?>
               <?php  the_excerpt(); ?>
               <?php  else : ?>
               <div class="page-content"><?php  the_content(); ?></div>
               <?php  endif; ?>
            </div>
            <div class="more-button"><a href="<?php  the_permalink(); ?>" title="<?php  printf(__('Permanent link to %s', 'themetek_lang'), get_the_title()); ?>" class="simple-button"><span class="icon flaticon-double4"></span>Read more</a></div>
          </div>
      </div>
   </div>
</div>