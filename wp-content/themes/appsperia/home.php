<?php 
  global $redux_ThemeTek;
  get_header();
  ?>
<?php  if( is_home() ) : ?>
<div id="posts-content">
<?php 
  
  if (have_posts()) :
  while (have_posts()) :
  the_post();
  ?>
<div <?php  post_class('section'); ?> id="post-<?php  the_ID(); ?>" >
   <div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
            <h2 class="section-title"><?php  the_title(); ?></h2>
            <div class="entry-meta">
              <?php if ( is_sticky() ) echo '<span class="flaticon-pushpin1 icon"></span> Sticky &nbsp;&nbsp;|&nbsp;&nbsp;'; ?>
              <span class="author"><?php  the_author_posts_link(); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
              <span class="published"><?php  the_time( get_option('date_format') ); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;
              <span class="comment-count"><?php  comments_popup_link( __('No comments yet', 'themetek_lang'), __('1 comment', 'themetek_lang'), __('% comments', 'themetek_lang') ); ?></span>
            </div>
            <div class="entry-content">
               <?php  if(has_excerpt()) : ?>
               <?php  the_excerpt(); ?>
               <?php  else : ?>
               <div class="page-content"><?php  the_content(); ?></div>
               <?php  endif; ?>
            </div>
            <div class="more-button"><a href="<?php  the_permalink(); ?>" title="<?php  printf(__('Permanent link to %s', 'themetek_lang'), get_the_title()); ?>" class="simple-button"><span class="icon flaticon-double4"></span>Read more</a></div>
          </div>
      </div>
   </div>
</div>
<?php  endwhile; ?>
  <?php            
                   the_posts_pagination( array(                  
                   'prev_text'          => __( '<span class="icon flaticon-double5"></span>', 'appsperia' ),
                   'next_text'          => __( '<span class="icon flaticon-double4"></span>', 'appsperia' ),
                   'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '&nbsp;', 'appsperia' ) . ' </span>',
  ) ); ?>
<?php  else : ?>
<div class="wrap" >
      <div class="fp-tableCell">
         <div class="box" >
<div id="post-not-found" <?php  post_class(); ?>>
   <h2 class="entry-title"><?php  _e('Error 404 - Not Found', 'themetek_lang')     ?></h2>
   <div class="entry-content">
      <p><?php  _e("Sorry, no posts matched your criteria.", "themetek_lang")     ?></p>
   </div>
</div>
   </div>
      </div>
   </div>
<?php  endif; ?>
<?php  else:?>
  <?php
 $homePageID=get_the_ID();$args=array('post_type'=>'page','posts_per_page'=>-1,'post_parent'=>$homePageID,'post__not_in'=>array($homePageID),'order'=>'ASC','orderby'=>'menu_order');$parent=new WP_Query($args);?>
  <?php  if($parent->have_posts()):?>
  <?php  while($parent->have_posts()):$parent->the_post();?>
  <?php  $page_template=str_replace('page_','',str_replace('.php','',basename(get_page_template())));?>
  <?php  if($page_template&&$page_template!='page'):?>
  <?php  get_template_part($page_template,get_post_format());?>
  <?php  wp_reset_postdata();?>
  <?php  else:?>
  <?php  get_template_part('loop',get_post_format());?>
  <?php  wp_reset_postdata();?>
  <?php  endif;?>
  <?php  endwhile;?>
  <?php  endif;?>
<?php  endif; ?>

<?php  get_footer(); ?>