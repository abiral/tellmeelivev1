<?php
/*
 * Appsperia Theme
 * Author: ThemeTek.com
  File: functions.php
 */
define('TEMPLATE_DIR_URI', get_template_directory_uri() );
define('TEMPLATE_DIR', get_template_directory() );


// ------------------------------------------------------------------------
// Add Redux Framework & extras
// ------------------------------------------------------------------------
require get_template_directory() . '/themetek/themetek-init.php';
global $redux_ThemeTek;

// ------------------------------------------------------------------------
// Theme includes
// ------------------------------------------------------------------------
// Socials section
function themetek_socials() { require_once ( TEMPLATE_DIR . '/inc/socials.php');  }
// Wordpress Bootstrap Menu
require_once ( TEMPLATE_DIR . '/themetek/assets/extra/wp_bootstrap_navwalker.php');



// ------------------------------------------------------------------------
// Enqueue scripts and styles front and admin
// ------------------------------------------------------------------------
	if( !function_exists('themetek_enqueue_front') ) {
		function themetek_enqueue_front() {	

			global $redux_ThemeTek;
			// ------------
			// CSS
			// ------------
			// Theme main style CSS
			wp_enqueue_style( 'themetek-style', get_stylesheet_uri() );

		
			wp_enqueue_style( 'themetek-dynamic-styles', TEMPLATE_DIR_URI . '/themetek/dynamic-appsperia.css', '', '' );

			// Dynamic colors CSS
			wp_enqueue_style( 'themetek-dynamic-colors', TEMPLATE_DIR_URI . '/themetek/colors-appsperia.css.php', '', '' );

			// Dynamic colors CSS
			wp_enqueue_style( 'themetek-icons', TEMPLATE_DIR_URI . '/fonts/flaticon.css', '', '' );

							
							
			// ------------
			// JavaScript
			// ------------
			
			// jQuery 
			wp_enqueue_script( 'jquery' );			
			
	     	// Theme full page navigation
			wp_enqueue_script( 'themetek-easing', TEMPLATE_DIR_URI . '/js/jquery.easings.min.js', array(), '', true );	
			wp_enqueue_script( 'themetek-fullpage', TEMPLATE_DIR_URI . '/js/jquery.fullPage.js', array(), '', true );	
			wp_enqueue_script( 'themetek-sidr', TEMPLATE_DIR_URI . '/js/jquery.sidr.min.js', array(), '', true );	
			wp_enqueue_script( 'themetek-tabs', TEMPLATE_DIR_URI . '/js/cbpFWTabs.js', array(), '', true );	
		


			// Theme main scripts
			wp_enqueue_script( 'themetek-scripts', TEMPLATE_DIR_URI . '/js/scripts.js', array(), '', true );
			// Theme video background scripts
			if ($redux_ThemeTek['tek-bgvideo']['url']) {
			wp_enqueue_script( 'themetek-video-bg', TEMPLATE_DIR_URI . '/js/video.js', array(), '', true );
			}
		}
	}
	add_action( 'wp_enqueue_scripts', 'themetek_enqueue_front' );
	
	if( !function_exists('themetek_enqueue_admin') ) {
		function themetek_enqueue_admin() {
	        wp_enqueue_style( 'themetek-icons', TEMPLATE_DIR_URI . '/fonts/flaticon.css', '', '' );

	        wp_register_script( 'custom_wp_admin_js', TEMPLATE_DIR_URI . '/js/admin-scripts.js', '', '1.0.0' );
	        wp_enqueue_script( 'custom_wp_admin_js' );
		}
	}
	add_action( 'admin_enqueue_scripts', 'themetek_enqueue_admin' );

// ------------------------------------------------------------------------
// Theme Setup
// ------------------------------------------------------------------------
	function themetek_setup(){
		if ( function_exists( 'add_theme_support' ) ) {

			// Add multilanguage support
			load_theme_textdomain( 'themetek_lang', TEMPLATE_DIR . '/themetek/languages' );
		
			// Add theme support for feed links
			add_theme_support( 'automatic-feed-links' );

			add_theme_support( 'title-tag' );

			add_theme_support( 'custom-header', array() );
			add_theme_support( 'custom-background', array() );
			
			// Add theme support for menus
			if ( function_exists( 'register_nav_menus' ) ) {
				register_nav_menus(
					array(
					  'header-menu' => 'Header Menu'
					)
				);
			}

			// Enable support for Blog Posts Thumbnails
			add_theme_support( 'post-thumbnails' );

			}
	}
	add_action( 'after_setup_theme', 'themetek_setup' );

// ------------------------------------------------------------------------
// Include plugin check, meta boxes, widgets, custom posts
// ------------------------------------------------------------------------	
	// Theme activation and plugin check
	include("themetek/theme-activation.php");

// ------------------------------------------------------------------------
// Include shortcodes
// ------------------------------------------------------------------------	

	// include("themetek/shortcodes.php");

// ------------------------------------------------------------------------
// Comment reply script enqueued
// ------------------------------------------------------------------------

function themetek_enqueue_comments_reply() {
	if( get_option( 'thread_comments' ) )  {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'comment_form_before', 'themetek_enqueue_comments_reply' );



    
// ------------------------------------------------------------------------
// Set the content width based on the theme's design and stylesheet.
// ------------------------------------------------------------------------
	if ( ! isset( $content_width ) ) {
		$content_width = 1040;
	}

// ------------------------------------------------------------------------
// Workaround for wp-title in front-page or home pages
// ------------------------------------------------------------------------
	function themetek_wp_title_for_home( $title )
	{
	  if( empty( $title ) && ( is_home() || is_front_page() ) ) {
		return __( 'Home', 'themetek_lang' );
	  }
	  return $title;
	}
	add_filter( 'wp_title', 'themetek_wp_title_for_home' );


// ------------------------------------------------------------------------
// Main menu custom child pages attribute
// ------------------------------------------------------------------------
 
	function themetek_special_nav_class($classes, $item){
    	$menu_locations = get_nav_menu_locations();
  
		$pageid = get_post_meta( $item->ID, '_menu_item_object_id', true );
		$page_data = get_page( $pageid );
		$parrent_bool = $page_data->post_parent;
		if($parrent_bool) {
			$classes[] = 'one-page-link';
		}
   
    	return $classes;
	}
	add_filter('nav_menu_css_class' , 'themetek_special_nav_class' , 10 , 2);


// ------------------------------------------------------------------------
// Search only blog posts
// ------------------------------------------------------------------------
	function themetek_search_filter($query) {
	    if ($query->is_search) {
	        $query->set('post_type', 'post');
	    }
	    return $query;
	}
	add_filter('pre_get_posts','themetek_search_filter');


// ------------------------------------------------------------------------
// Set mock-up image size
// ------------------------------------------------------------------------


	if ( function_exists( 'add_theme_support' ) ) {
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 232, 407, true );
		add_image_size('post-thumbnails', 232, 407, true);
		}


// ------------------------------------------------------------------------
// Import Page
// -----------


function appsperia_importer() {
	add_theme_page('import-demo-full-custom', 'import-demo-full-custom', 'manage_options', 'import-demo', 'import_demo' );
	//add_action( 'wp_ajax_import_demo', 'import_demo_callback' );
	//add_submenu_page('Import Demo', 'Import Demo', 'manage_options', 'import-demo', 'import_demo');
}
add_action( 'admin_menu', 'appsperia_importer' );


function import_demo() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.','themetek_lang' ) );
	}
	require get_template_directory() . '/import.php';
}



function check_import_files($demo_content) {
	$response = array();

	$file_headers = @get_headers( $demo_content );
	if( !strpos( $file_headers[0], '200' ) ) {
	    $response['errors'][] = 'demo_content.xml';
	}
	$file_headers = @get_headers( $rev_slider );
	if( !strpos( $file_headers[0], '200' ) ) {
	    $response['errors'][] = 'revolution_slider.zip';
	}

	return $response;
}

function import_theme_files($upload_dir, $demo_content) {

wp_remote_get( 
    str_replace(" ","%20",$demo_content), 
    array( 
        'timeout'  => 300, 
        'stream'   => true, 
        'filename' => $upload_dir.'/demo_content.xml_.txt'
    ) 
);


	return array(	
		'demo' => $upload_dir.'/demo_content.xml_.txt', 
		'slider' => $upload_dir.'/revolution_slider.zip' //, 'theme' => $upload_dir.'/theme_options.json'
	);
}

function change_permalinks() {
    global $wp_rewrite;
    $wp_rewrite->set_permalink_structure('/%postname%/');
    $wp_rewrite->flush_rules();
}
add_action('init', 'change_permalinks');


function wpshock_search_filter( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', array('post','page') );
    }
    return $query;
}
add_filter('pre_get_posts','wpshock_search_filter');





