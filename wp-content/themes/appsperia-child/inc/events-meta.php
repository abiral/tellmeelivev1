<?php

add_action( 'add_meta_boxes', 'add_meta_box_events' );

function add_meta_box_events(){
	
	add_meta_box( 'players', __( 'Event Details', 'poker' ), 'populate_players_meta_box', 'events' );
	add_meta_box( 'like-dislike', __( 'Likes and Dislikes', 'poker' ), 'populate_like_dislike_meta_box', 'comment','normal' );

}

function populate_players_meta_box( $post ){

	$players = get_users('role=player');
	
	$db_players = array();
	
 	$start_date = get_post_meta( $post->ID,'event_start_date',true );
 	$end_date = get_post_meta( $post->ID,'event_end_date',true ); 
	$chipcount_enable = get_post_meta ( $post->ID, 'chipcount_enable', true );
	
	if( isset($_GET['action']) && 'edit' === $_GET['action'] ):
		
		$post_meta = get_post_meta( $post->ID,'event_player',true );
	
		if( is_array( $post_meta ) )
			$db_players = $post_meta;
	
	endif;
	?>
	<div class="ajaxplayeradd">
		<p>
			<select name="poker_player_id" id="poker-player-id">
				<option value="0">Select a Player</option>
				<?php
					if( is_array($players) & count($players) > 0 ):
						foreach( $players as $user ):
							
							if( !in_array( $user->data->ID,$db_players ) ):
								$user_display_name = trim($user->data->display_name);
								?>
								
								<option value="<?php echo $user->data->ID; ?>" data-name="<?php echo $user_display_name; ?>"><?php echo $user_display_name; ?></option>
				<?php
						endif;
				 		endforeach;
					 endif;
				?>
			</select>
			<button type="button" id="player-add-btn" class="button tagadd">Add</button>
		</p>
	</div>
	
	
	<div id="player-checklist">
	<?php
	
		if( count( $db_players ) > 0 ):
			foreach ( $db_players as $player_id ):
				$user = get_user_by('id',$player_id);
				$player_name = $user->user_nicename;
				echo '<span><input type="hidden" name="player_id[]" value="'.$player_id.'" ><a data-id="'.$player_id.'" data-name="'.$player_name.'" class="ntdelbutton" >X</a>&nbsp;<a href="/wp-admin/user-edit.php?user_id='.$player_id.'" target="_blank" title="Edit User">'.$player_name.'</a></span>';
		
			endforeach;
		endif;
		
	?>
	</div>
	
	<table class="form-table">
		<tbody>
			<tr>
				<th>Start Date:</th>
				<td><input class="date-picker" type="text" name="start_date" value="<?php echo $start_date;  ?>" ></td>
			</tr>
			<tr>
				<th>End Date:</th>
				<td><input class="date-picker" type="text" name="end_date" value="<?php echo $end_date;  ?>" ></td>
			</tr>
			<?php
				$post_chipcount = get_post_meta( $post->ID,'chipcount',true );
			?>
			<tr>
				<th>Enable Chipscount:</th>
				<td><input name="chipcount_enable" value="1" type="checkbox" id="chipcount" data-chipcount = "<?php echo $post_chipcount; ?>" data-target="#chipcount-field" <?php checked( $chipcount_enable, 1 ); ?> ></td>
			</tr>
			<tr id="chipcount-field">
				<th>Chipscount Amount</th>
				<td><input type="text" name="chipcount" value="<?php echo $post_chipcount; ?>" ></td>
			</tr>
		</tbody>
	</table>
	<?php
}

function populate_like_dislike_meta_box ( $post ){
	
  	$comment_id = $post->comment_ID;
   	$likes = get_comment_meta( $comment_id, 'total_likes', true );
  	
  	$dislikes = get_comment_meta( $comment_id, 'total_dislikes', true );
	?>
	
	<p><strong>Total Likes: </strong> <span><?php echo empty( $likes ) ? '0' : $likes; ?></span></p>
	<p><strong>Total Disikes: </strong> <span><?php echo empty( $dislikes ) ? '0' : $dislikes; ?></span></p>
	<?php
}

function add_players_event( $post_id ){

	$players = array();
	
	// Delete user meta if admin removes players from the event.
	
	if( isset($_POST['removed_player_id']) ):
		
		$removed_player_ids = $_POST[ 'removed_player_id' ];
		
		foreach( $removed_player_ids as $removed_player_id ):
			
			$user_meta = get_user_meta( $removed_player_id, 'event', true );
			
			if( is_array( $user_meta ) ):
				
				$updated_user_meta = array_diff( $user_meta, array( $post_id ) );
				update_user_meta( $removed_player_id, 'event', $updated_user_meta );
				
			endif;
			
		endforeach;
	endif;
	
	//Deleting all the associated users on this event 
	delete_post_meta( $post_id, 'associated_users' );
	
	// Update user meta / post meta for newly added Players.
	if(isset( $_POST['player_id'] )):
		
		$players = $_POST['player_id'];
		
		//saving all the associated users on this event 
		update_post_meta( $post_id,'associated_users',$players );
		
		foreach( $players as $user_id ):
			
			$user_meta = get_user_meta( $user_id, 'event', true );
			
			if(is_array($user_meta)){
				
				if( !in_array( $post_id,$user_meta ) ){
					
					$user_meta[] = $post_id;
				}
				
			}else{
				
				$user_meta = array();
			}
			
			update_user_meta( $user_id, 'event', $user_meta );
			
			if( isset($_POST['chipcount']) )
			{
				// Add chip count for each player.
				
				$chipcount = $_POST[ 'chipcount' ];
				
				 if( is_numeric( $chipcount ) ):
				
				 	$user_meta = get_user_meta( $user_id, 'chipcount', true );
					
				 	if( is_array($user_meta) ):
				 		$user_meta[] = array("event_id"=>$post_id, 'chipcount'=>$chipcount);
						// $user_meta[ $post_id ] = $chipcount;
					else:
						$user_meta[] = array("event_id"=>$post_id, 'chipcount'=>$chipcount);
				 	endif;	

				 	update_user_meta( $user_id, 'chipcount',$user_meta);
				endif;
			}
		
		endforeach;	
		
		if( isset($_POST['chipcount']) )
		{
			// Add chip count for each player.
	
			 if( is_numeric( $chipcount ) ):
			 	update_post_meta( $post_id, 'chipcount',$chipcount );
		     endif;
		}
	
	endif;
	
	update_post_meta( $post_id, 'event_player', $players ); 
	
	if( isset($_POST['start_date']) ){
		$start_date = $_POST['start_date'];
		update_post_meta( $post_id, 'event_start_date', $start_date);
	}
	
	if( isset($_POST['end_date']) )
	{
		$end_date = $_POST['end_date'];
		update_post_meta( $post_id, 'event_end_date', $end_date);
	}
	
	// chipcount_enabled and disabled 
	if (isset($_POST['chipcount_enable'])) {
		// Checkbox is selected
		update_post_meta( $post_id, 'chipcount_enable', 1);
	} else {
		// Alternate code
		update_post_meta( $post_id, 'chipcount_enable', 0);
	}
	

}

add_action( 'save_post','add_players_event', 10, 2 );