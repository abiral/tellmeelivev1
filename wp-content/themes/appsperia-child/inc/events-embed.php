<?php

add_action( 'add_meta_boxes', 'add_events_embed_box' );

function add_events_embed_box(){
	add_meta_box( 'event-embed', __( 'Event Details', 'poker' ), 'embed_events_meta_box', 'events' );
}

function embed_events_meta_box( $post ){ 
	$script_code = get_script_code($post->ID);
	$iframe_width = get_post_meta( $post->ID, 'iframe_width', true );
	$iframe_height = get_post_meta( $post->ID, 'iframe_height', true );
	
	$iframe_width = empty($iframe_width)?'320':$iframe_width;
	$iframe_height = empty($iframe_height)?'96%':$iframe_height;
	
	$iframe_code = '<iframe src="'.get_permalink($post->ID).'embed/" width="'.$iframe_width.'" height="'.$iframe_height.'" ></iframe>';
?>
	
	<script type="text/javascript" >
		jQuery(document).ready(function($){
			var iframe = document.createElement('iframe');
			
			iframe.src = jQuery('#iframe-src').val();
			iframe.width = 320;
			iframe.height = 400;
			
			jQuery('#iframe-width').on('input',function(){
				iframe.width = $(this).val();
				$('#iframe-embed').val(iframe.outerHTML);
			});
			
			jQuery('#iframe-height').on('input',function(){
				iframe.height = $(this).val();
				$('#iframe-embed').val(iframe.outerHTML);
			});
			
		});
	</script>
	<div class="embeds-row embed-iframe">
		<p><label for="iframe-embed"><strong><?php _e('Embed IFrame','poker'); ?></strong></label></p>
		
		<input type="hidden" id="iframe-src"  name="iframe_src" value="<?php echo get_permalink($post->ID).'embed/'; ?>" />
		<input type="number" id="iframe-width" name="iframe_width" placeholder="IFrame Width" value="<?php echo $iframe_width; ?>" />
		<input type="number" id="iframe-height" name="iframe_height" placeholder="IFrame Height" value="<?php echo $iframe_height; ?>" />
		<div class="form-field" >
			<textarea name="iframe_embed" id= "iframe-embed" rows="2" style="white-space: pre;resize: vertical;background: #ececec;"/><?php echo esc_html($iframe_code); ?></textarea>
		</div>
	</div>
	
	<div class="embeds-row embed-script form-field">
		<p><label for="script-embed"><strong><?php _e('Embed Script','poker'); ?></strong></label></p>
		<div>
			<textarea name="script_embed" id= "script-embed" rows="5" /><?php echo esc_html($script_code); ?></textarea>
		</div>
	</div>
<?php }


function save_events_embed( $post_id ){

	$players = array();
	
	// Delete user meta if admin removes players from the event.
	
	if( isset($_POST['removed_player_id']) ):
		
		$removed_player_ids = $_POST[ 'removed_player_id' ];
		
		foreach( $removed_player_ids as $removed_player_id ):
			
			$user_meta = get_user_meta( $removed_player_id, 'event', true );
			
			if( is_array( $user_meta ) ):
				
				$updated_user_meta = array_diff( $user_meta, array( $post_id ) );
				update_user_meta( $removed_player_id, 'event', $updated_user_meta );
				
			endif;
			
		endforeach;
	endif;
	
	// Update user meta / post meta for newly added Players.
	
	if(isset( $_POST['player_id'] )):
		
		$players = $_POST['player_id'];
		
		foreach( $players as $user_id ):
			
			$user_meta = get_user_meta( $user_id, 'event', true );
			
			if(is_array($user_meta)){
				
				if( !in_array( $post_id,$user_meta ) ){
					
					$user_meta[] = $post_id;
				}
				
			}else{
				
				$user_meta = array();
			}
			
			update_user_meta( $user_id, 'event', $user_meta );
			
		endforeach;	
	endif;
	
	update_post_meta( $post_id, 'event_player', $players ); 
	
	if( isset($_POST['start_date']) ){
		$start_date = $_POST['start_date'];
		update_post_meta( $post_id, 'event_start_date', $start_date);
	}
	
	if( isset($_POST['end_date']) )
	{
		$end_date = $_POST['end_date'];
		update_post_meta( $post_id, 'event_end_date', $end_date);
	}
	
	 
	update_post_meta( $post_id, 'iframe_width', $_POST['iframe_width'] );
	update_post_meta( $post_id, 'iframe_height', $_POST['iframe_height'] );
	
	
}

add_action( 'save_post','save_events_embed', 10, 2 );

function get_script_code($post_id){
	$output = '';
	$output .='<div id="liveblog"></div>';
	$output .='<script type = "text/javascript">var ajax_url = "'.admin_url("admin-ajax.php").'";</script>';
	$output .= '<script type="text/javascript" src="'.get_template_directory_uri().'/js/liveblog.js"></script>';
	$output .= '<script type="text/javascript">jQuery(document).ready(function(){ new liveBlog({"id":"'.$post_id.'"}); });</script>';
	
	return $output;
}