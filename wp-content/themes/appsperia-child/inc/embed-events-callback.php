<?php header('Access-Control-Allow-Origin: *') ?>
<?php
$dir = 0;
while (!file_exists(str_repeat('../', $dir).'wp-load.php'))
    if (++$dir > 16) exit;
        $load_url = str_repeat('../', $dir).'wp-load.php';
include_once($load_url);
get_event_block();

function get_event_block(){
    $args = array(
    	'post_type'        => 'events',
    	'post_status'      => 'publish',
    	'p'                => 130 
    );
    query_posts($args);
?>
    <div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		    <?php while ( have_posts() ) : the_post(); ?>
		    <?php 
                get_template_part( 'template-parts/content', 'single' ); 
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>
            <?php 
        		endwhile; // End of the loop. 
        	?>
		</main>
	</div>
<?php
}
?>