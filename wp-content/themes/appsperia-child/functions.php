<?php

if ( ! function_exists( 'poker_setup' ) ) :

function poker_setup() {

	load_theme_textdomain( 'poker', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'poker' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );
	
	add_theme_support( 'custom-background', apply_filters( 'poker_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	

}
endif; // poker_setup
add_action( 'after_setup_theme', 'poker_setup' );

function poker_switch_theme () {
 
  	/** Added role for player **/
	$subscriber = get_role('subscriber');
	$capabilities = $subscriber->capabilities;
   
   	add_role( 'company','Company',$capabilities );
	add_role( 'visitor','Visitor',$capabilities );
	add_role( 'player','Player',$capabilities );

}

add_action( 'init', 'poker_switch_theme' );

function poker_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'poker_content_width', 640 );
}
add_action( 'after_setup_theme', 'poker_content_width', 0 );

function poker_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'poker' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'poker_widgets_init' );

function poker_scripts() {
	global $post;
	
	wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() . '/font-awesome-4.4.0/css/font-awesome.min.css' );
	
	wp_register_script( 'play-sound-js', get_stylesheet_directory_uri(). '/js/vendor/notification/js/jquery.playSound.js', array( 'jquery' ) );
	wp_register_script( 'title-alert-js', get_stylesheet_directory_uri(). '/js/vendor/notification/js/jquery.titlealert.min.js', array( 'jquery' ) );
	wp_register_script( 'bootstrap', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
	
	wp_register_script( 'inview-js', get_stylesheet_directory_uri() . '/js/jquery.inview.min.js', array( 'jquery' ) );

	wp_register_script( 'like-dislike-js', get_stylesheet_directory_uri() . '/js/like-dislike.js', array( 'jquery' ) );
	wp_register_script( 'ajax-uploader-js', get_stylesheet_directory_uri() . '/js/ajax-uploader.js', array( 'jquery' ) );

	wp_register_script( 'caroufredsel-js', get_stylesheet_directory_uri() . '/js/vendor/carouFredSel-6.0.0/jquery.carouFredSel-6.0.0.js', array( 'jquery' ) );
	wp_register_script( 'mousewheel-js', get_stylesheet_directory_uri() . '/js/vendor/carouFredSel-6.0.0/helper-plugins/jquery.mousewheel.min.js', array( 'jquery' ) );
	wp_register_script( 'touch-swipe-js', get_stylesheet_directory_uri() . '/js/vendor/carouFredSel-6.0.0/helper-plugins/jquery.touchSwipe.min.js', array( 'jquery' ) );

	wp_register_script( 'nicescroll', get_stylesheet_directory_uri() . '/js/jquery.nicescroll.min.js', array( 'jquery' ) );

	wp_register_script( 'tellme-browsernotify', get_stylesheet_directory_uri() . '/js/browser-notify.js', array( 'jquery' ) );
	wp_register_style('open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' );
	
	if( is_singular( 'events' )){
		wp_dequeue_script('themetek-fullpage');
		wp_dequeue_script('themetek-scripts');
		
		wp_enqueue_style( 'open-sans' );
		
		wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );
		
		$embeded = get_query_var('embed');
		
		if( $embeded ){
			wp_enqueue_style( 'client-style', get_stylesheet_directory_uri() . '/css/tellme-embed.css' );
		}else{
			wp_enqueue_style( 'client-style', get_stylesheet_directory_uri() . '/css/tellme.css' );	
		}
		
		wp_enqueue_script( 'poker-navigation', get_stylesheet_directory_uri() . '/js/navigation.js', array(), '20120206', true );

		// wp_enqueue_script( 'poker-skip-link-focus-fix', get_stylesheet_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

		wp_enqueue_style( 'icomotion-css', get_stylesheet_directory_uri() . '/css/icomoon.css' );
	
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		wp_enqueue_script( 'emotions', get_stylesheet_directory_uri() . '/js/jquery.emotions.js', array( 'jquery' ) );
	
		wp_enqueue_script( 'commenter-js', get_stylesheet_directory_uri() . '/js/commenter.js', array( 'jquery','ajax-uploader-js','inview-js','nicescroll','play-sound-js','title-alert-js','tellme-browsernotify' ) );
	
		wp_enqueue_script( 'client', get_stylesheet_directory_uri() . '/js/client.js', 
			array( 'jquery','bootstrap','like-dislike-js','mousewheel-js','touch-swipe-js','caroufredsel-js' )
			);
	
		wp_localize_script( 'like-dislike-js', 'POKER_API', array(
			'root' => esc_url_raw( site_url() ),
			'template_directory_uri' => get_stylesheet_directory_uri(),
			'nonce' => wp_create_nonce( 'wp_rest' ),
			'current_user_id' => get_current_user_id(),
			'comment_errors' => array(
				'author_missing' => __( '<strong>Error!</strong> Enter your name','poker' ),
				'default'=> __( '<strong>Error!</strong> Failed to post your comment','poker' )
			),
			'event_uri' =>get_permalink($post->ID)
		));
		
		wp_localize_script( 'wp-api', 'WP_API_Settings', array( 'root' => esc_url_raw( get_json_url() ), 'nonce' => wp_create_nonce( 'wp_json' ) ) );

	}else{
		wp_register_style('parent-style',get_template_directory_uri().'/style.css' );
		wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/css/child-style.css', array('parent-style') );
	}

}

add_action( 'wp_enqueue_scripts', 'poker_scripts',11 );

function poker_admin_scripts(){
	
	wp_enqueue_script( 'admin-js', get_stylesheet_directory_uri() . '/js/admin.js', array( 'jquery' ) );
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_style('jquery-style', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');

	wp_enqueue_style( 'admin-style', get_stylesheet_directory_uri() . '/css/admin.css' );
	
}

add_action( 'admin_enqueue_scripts', 'poker_admin_scripts' );

require get_stylesheet_directory() . '/inc/events-meta.php';

require get_stylesheet_directory() . '/inc/events-embed.php';

require get_stylesheet_directory() . '/inc/extras.php';

add_action( 'init', 'create_post_type_event' );

function create_post_type_event() {
    
 $capabilities = array(
	'edit_post'             => 'edit_events',
	'read_post'             => 'read_events',
	'delete_post'           => 'delete_events',
	'edit_posts'            => 'edit_events',
	'publish_posts'         => 'publish_events',
);
 
  register_post_type( 'events',
    array(
      'labels' => array(
        'name' => __( 'Events' ),
        'singular_name' => __( 'Event' ),
        'add_new_item' => 'Add New Event',
        'edit_item' => 'Edit Event'
      ),
      'public' => true,
      'has_archive' => true,
      'capability_type' => 'post',
      'supports' => array( 'title', 'editor', 'thumbnail', 'revisions','excerpt','comments', 'author' ),
      'rewrite' => array( 'slug' => 'events' ),
    )
  );
}

function get_user_avator_image_url($id){
		$image_url = user_avatar_fetch_avatar( array( 'html'=>false, 'item_id' => $id, 'width' => '', 'height' => '', 'alt' => '' ) );
		if( $image_url ) return $image_url;
		return '';
}
/**
*  check current user is associated to events
**/
function tellme_is_event_associated_user($event_id, $user_id) {
	
        $players = get_post_meta($event_id, 'event_player', true);
       
        if ( ! $players ) return false;
        if( !$user_id ) return false;

        return in_array($user_id, $players);
}

function poker_comment($comment, $args, $depth) {

	$profile_url = get_user_avator_image_url( $comment->user_id );
	if( empty($profile_url) ){
		$profile_picture = '<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/anonymous.png" alt="" >';
	}
	else{
		$profile_picture = '<img class="img-responsive" src="'.$profile_url.'" alt="" >';
	}
	
	$user_info = get_userdata($comment->user_id);
	
	if ( $comment->user_id == '0'){
		$display_name = $comment->comment_author;
	}
	else{
		$display_name = trim($user_info->first_name." ".$user_info->last_name);
		
		
		if( empty($display_name)){
			$display_name = $comment->comment_author;
		}
	}

	$event_id = $comment->comment_post_ID;
	$comment_id = $comment->comment_ID;
	$comment_image = get_comment_meta( $comment_id, 'comment_image', true );
	$likes = get_comment_meta( $comment_id, 'total_likes', true );
	$dislikes = get_comment_meta( $comment_id, 'total_dislikes', true );
	$time = get_comment_time('M d, H:i');
	$timestamp = strtotime( $comment->comment_date_gmt );
?>
	<section data-time="<?php echo $timestamp; ?>" data-id="<?php comment_ID(); ?>" class="poker-comment-wrapper <?php echo ( $comment->user_id == 0 ) ? 'anonymous': ''; ?>">
		<div class="container">
			<div class="commenter-pic" data-toggle="tooltip" data-placement="bottom" title="<?php echo ucfirst($comment->comment_author); ?>">	
				<?php  echo $profile_picture; ?>
			</div>
			<div class="commenter-info">
				<div class="name">
					<span class="author">
						<?php
							$name_prefix = '';
							if( !tellme_is_event_associated_user( $event_id, $comment->user_id ) ){
								$name_prefix =  'Comment from ';
							}
							echo $name_prefix.'<span class="">'.ucwords($display_name).'</span>'; 
						?>
					</span> 
					<span class="date">
						<?php echo $time; ?>
					</span>
				</div>
				<div class="comment"> <?php echo $comment->comment_content; ?> </div>
				
				<?php if( !empty( $comment_image ) ): ?>
					<div class="comment-image"><img src="<?php echo $comment_image; ?>" alt="comment image"></div>
				<?php endif; ?>
				
				<div class="comment-like-dislike">
					<p id="like"><a href="#" data-toggle="popover" data-content="You have already liked/disliked this comment" data-placement="top" ><i class="fa fa-thumbs-up" ></i></a><span><?php echo empty( $likes ) ? 0 : $likes; ?></span></p>
					<p id="dislike"><a href="#" data-toggle="popover" data-content="You have already liked/disliked this comment" data-placement="top" ><i class="fa fa-thumbs-down" ></i></a><span><?php echo empty( $dislikes ) ? 0 : $dislikes; ?></span></p>
				</div>
			</div>
		</div><!-- /container -->
	</section>
<?php
}

function custom_comment_form_btn ($button) {
	return ;
}
add_filter('comment_form_submit_button', 'custom_comment_form_btn');

function poker_emoji(){
	
	$output  = '<div class="poker-emoticons">';
	
	$output .= '<a href=""><span class="icon-home3"></span></a>
	<a href="#"><span class="icon-happy"></span></a>
	<a href="#"><span class="icon-smile"></span></a>
	<a href="#"><span class="icon-tongue"></span></a>
	<a href="#"><span class="icon-sad"></span></a>
	<a href="#"><span class="icon-wink"></span></a>
	<a href="#"><span class="icon-grin"></span></a>
	<a href="#"><span class="icon-cool"></span></a>
	<a href="#"><span class="icon-angry"></span></a>
	<a href="#"><span class="icon-evil"></span></a>
	<a href="#"><span class="icon-shocked"></span></a>
	<a href="#"><span class="icon-baffled"></span></a>
	<a href="#"><span class="icon-confused"></span></a>
	<a href="#"><span class="icon-neutral"></span></a>
	<a href="#"><span class="icon-hipster"></span></a>
	<a href="#"><span class="icon-wondering"></span></a>
	<a href="#"><span class="icon-sleepy"></span></a>
	<a href="#"><span class="icon-frustrated"></span></a>
	<a href="#"><span class="icon-crying"></span></a>
	<a href="#"><span class="icon-point-up"></span></a>             
	';
	
	$output .= '</div>';
	
	return $output;
}


add_action( 'admin_menu', 'tellme_check_role' );
function tellme_check_role() {
    $current_user = wp_get_current_user();
    if ( in_array( 'company', $current_user->roles ) ){
    	remove_menu_page( 'wpcf7' );
    	remove_menu_page( 'tools.php' );
    	remove_menu_page( 'edit.php' );
    	remove_menu_page( 'post-new.php' );
    }
}

function poker_extra_profile_fields( $user ){

	if( 'player' == $user->roles[0] ):
?>
<table class="form-table">
	<tr>
		<th>
			<label for="company_code"><?php   _e( 'Company Code','poker' ); ?></label>
		</th>
		<td>
			<input type="text" name="company_code" id="company_code" value="<?php echo esc_attr( get_the_author_meta( 'company_code', $user->ID ) ); ?>" class="regular-text" />
		</td>
	</tr>
</table>
<?php
	endif;
}

add_action( 'show_user_profile', 'poker_extra_profile_fields' );
add_action( 'edit_user_profile', 'poker_extra_profile_fields' );

function poker_save_extra_user_field( $user_id, $old_user_data )
{
	$user = get_user_by( 'id', $user_id );
	if( 'player' == $user->roles[0] ):
		if( isset( $_POST['company_code']) ):
			
			$company_code = strip_tags( trim($_POST['company_code']) );
			
			update_user_meta( $user_id, 'company_code' , $company_code );
			
		endif;	
 	endif;
}

add_action( 'profile_update', 'poker_save_extra_user_field', 10, 2 );

function poker_key_value_exists_index($array, $key, $val) {
	if ( ! $array) return false;
   	$loop_count = 0; 
    foreach ($array as $item){
    	if (isset($item[$key]) && $item[$key] == $val)
            return $loop_count;
        
        $loop_count += 1;
    }   
    return false;
}

function poker_get_player_event_chipcount($player_id, $event_id) {
	
	$chip_count_array = get_user_meta($player_id, 'chipcount', true);
	if($chip_count_array){	
		$find_index = poker_key_value_exists_index($chip_count_array, 'event_id', $event_id);
		$chipcount = $chip_count_array[$find_index]['chipcount'];
		$chipcount = $chipcount ? $chipcount : 0; 
  	}else{
		$chipcount = 0;
	}
  	return (string) $chipcount;
}

function sortByChipcountOrder($a, $b) {
    return $b['chipcount'] - $a['chipcount'];
}

function get_chipcount_by_event_id( $event_id ){
	
	$data = array();
	$user_ids = get_post_meta( $event_id,'associated_users',true );
	
	if( is_array($user_ids) ){
		
		foreach( $user_ids as $key=>$u_id ){
			
			$user = get_user_by( 'id',$u_id );
			
			$data[$key]['name'] = $user->user_nicename;
			$data[$key]['avatar'] = get_avatar( $u_id );
			$data[$key]['user_id'] = $u_id;
			
			$user_chipcount = get_user_meta( $u_id,'chipcount',true );
			
			$data[$key]['chipcount'] = poker_get_player_event_chipcount( $u_id, $event_id );
		}
		
		if($data){
			usort($data, 'sortByChipcountOrder');
		}
		
	}
	
	return $data;
}

add_action('init','create_virtual_embed_url');

function create_virtual_embed_url(){
   add_rewrite_rule("^events/([^/]+)/embed/?",'index.php?events=$matches[1]&embed=true','top');
   add_rewrite_tag( '%embed%', 'true' );
}