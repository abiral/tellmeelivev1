<?php

  $event_id = get_the_ID();
  $event_start_date = get_post_meta( $event_id, 'event_start_date', true );
  $event_end_date = get_post_meta( $event_id, 'event_end_date', true );
  $today = date('Y-m-d');
  $event_expired = true;
  
  if( $today >= $event_start_date && $event_end_date >= $today ){
    $event_expired = false;
  }
  
?>

<div id="comments" class="comments-area">

		<h2 class="comments-title">
		  <div class="container">
		    <i class="fa fa-rss poker-rss"></i>	<span>LIVE BLOG</span> <span class="circle-num" id="comment-counter"><strong></strong></span>
		  </div>
		</h2>

		<div id="poker-commentlist" data-total-comment="<?php echo get_comments_number( $event_id ); ?>" data-event-id="<?php the_ID(); ?>">
	        <?php wp_list_comments( 'reverse_top_level=true&type=comment&callback=poker_comment' ); ?>
		</div>

	<?php 
	
   $disabled = 'disabled';
 
    
    $smiley_image = get_stylesheet_directory_uri().'/images/img-smiley.png';
    $comment_image = get_stylesheet_directory_uri().'/images/chat-img.png';
    $name_image = get_stylesheet_directory_uri(). '/images/messanger-img.png';
    
    $author_email = '';
    $author_name = '';
    $current_user = wp_get_current_user();
    
    if( $current_user ){
      $author_email = $current_user->user_email; 
      $author_name = $current_user->display_name;
      $author_id = $current_user->ID;
    }else{
      $author_id = 0;
    } 
?>

</div><!-- #comments -->
</div><!-- event-section-1 -->

<div class="event-section-2">
    <?php
    
      if( $event_expired ):
        
        echo '<div class="event-error"><div class="container"><span>';
        if( $today <= $event_start_date )
        {
          echo 'Live blog not started yet';
        }
        elseif( $event_end_date <= $today )
        {
          echo "Live blog is ended";
        }
        echo "</span></div></div>";
        else:
         
          ?>
          <div class="contaidner">
            <div class="comment-respond">
              <form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="poker-comment-form" class="comment-form" novalidate="" enctype="multipart/form-data">
    							<div class="form-row">
    							  <a href="#" class="comment-camera" id="comment-camera" data-trigger="#comment-image" >
      							  <div class="uploaded-image">
    							       <i class="fa fa-times-circle poker-remove-pic"></i>
    							        <span id="uploaded-image"></span>
      							   </div>
      							    <span class="upload-image"> <i class="fa fa-pencil"></i> </span>
      							</a>
    							  
    							  <input type="text" id="poker-comment" placeholder="Write Comment" name="comment_content" aria-required="true" required="required">
    							  <button type="submit" id="submit" class="submit <?php echo $disabled; ?>" <?php echo $disabled; ?> data-proceses="<?php _e('POSTING','poker'); ?>" >
    							     <i class="fa fa-send-o"></i>
    							  </button>
                    <a href="#" id="comment-smiley" class="comment-smiley"></a><?php echo poker_emoji(); ?>
                  </div>
                  
                  <input type="file" class="hidden" name="file" id="comment-image" accept="image/gif, image/jpeg, image/png">
                  <div class="form-row pok-nam">
                    <span class="upload-image img-auth"><i class="fa fa-user"></i></span>
            	      <input id="poker-author" name="comment_author" placeholder="Name" type="text" value="<?php echo ucfirst($author_name); ?>" <?php echo ( $author_name ) ? "disabled":"" ?> required="required">
          	      </div>	
          	      <input type="hidden" id="comment_author_email" name="comment_author_email" value="<?php echo $author_email; ?>">
    						  <input type="hidden" name="comment_post_ID" value="<?php echo $event_id; ?>" id="comment_post_ID">
    						  <input type="hidden" name="user_id" value="<?php echo $author_id; ?>" id="user-id">
            </form>
          </div>
        </div>
          <?php
      endif;
    ?>
</div><!-- /event-seciton-2 -->
</article><!-- #post-## -->