jQuery(document).ready(function(){
    
    jQuery('#player-add-btn ').on('click',function(){
        
        var pokerSelector = '#poker-player-id';
        var playerId = jQuery(pokerSelector).val();
       
        if( undefined != playerId && playerId > 0)
        {
            var playerName = jQuery(pokerSelector+' option:selected').text();
            
            jQuery('#player-checklist').append('<span><input type="hidden" name="player_id[]" value="'+playerId+'" ><a data-id="'+playerId+'" data-name="'+playerName+'" class="ntdelbutton" >X</a>&nbsp; <a target="_blank" title="Edit User" href="/wp-admin/edit-user.php?user_id='+playerId+'">'+playerName+'</a></span>');
            jQuery(pokerSelector+' option[value="'+playerId+'"]').remove();
            
            jQuery( "#player-checklist #removed-player-id-"+playerId ).remove();
        }
        
    });
    
    jQuery(document).on('click','#player-checklist .ntdelbutton',function(){
      
       var pokerSelector = '#poker-player-id';
       var playerId = jQuery(this).attr('data-id');
       var playerName = jQuery(this).attr('data-name');
       
       jQuery( '#player-checklist' ).append( '<input type="hidden" id="removed-player-id-'+playerId+'" name="removed_player_id[]" value="'+playerId+'" > ' );
       
       jQuery(this).parent().remove();
       jQuery(pokerSelector).append('<option value="'+playerId+'" data-name="'+playerName+'">'+playerName+'</option>')
       
    });
    
     jQuery('.date-picker').datepicker({
            dateFormat: 'yy-mm-dd'
        });
   
   
});

(function ($){
    
    var chipcount = {
      target : '#chipcount',
      init : function (){
          
          var elem = jQuery( this.target ).attr( 'data-target' );
          var chipcount = jQuery( this.target ).attr( 'data-chipcount' );
          
          if( undefined != chipcount ){
              if( chipcount.length == 0 )
                jQuery( elem ).hide();
          }
          
          jQuery( this.target ).on( 'click',function(){
           
            jQuery( elem ).toggle();
            
        });
      }
    };
    
    jQuery( document ).ready(function(){
        chipcount.init();
    });
    
})(jQuery);
