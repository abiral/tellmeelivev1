var tellUser = function(notify_title, notify_message, notify_icon) {
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }

  else if (Notification.permission === "granted") {
    
    var notification = new Notification(notify_title,{
      icon: notify_icon,
      body: notify_message                                    
    }).onclick = function() {
        window.open(POKER_API.event_uri, "tellme").focus();
    };
  }else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      if (permission === "granted") {
        var notification = new Notification("Welcome to Tell me");
      }
    });
  }
}

Notification.requestPermission();

function spawnNotification(theBody,theIcon,theTitle) {
    var options = {
        body: theBody,
        icon: theIcon
    }
    
    var n = new Notification(theTitle,options);
}

