(function( $ ){
    
     var commentCounter = {
         
         wrapper : "#poker-commentlist",
         counter : "#comment-counter",
         store : '',
         totalComment : '',
         
         init : function(){
            
            this.loadCounter();
            
            var that = this;
            
            jQuery(document).on("inview","#comments .poker-comment-wrapper", function() {
                
                var $this = jQuery(this),
                
                 seenCommentId = JSON.parse( localStorage.getItem( that.store ) ),
        
                 commentId= $this.attr('data-id'),
                 
                 temp = [],
                 
                 currentCommentCount = parseInt( jQuery(that.counter).find("strong").text() );
                 
                 temp.push( commentId );
                 
                 setTimeout( function(){
                    if( null == seenCommentId )
                    {
                        localStorage.setItem( that.store, JSON.stringify( temp ) );
                    
                        jQuery( that.counter ).find("strong").text( (currentCommentCount - 1) > 0 ? (currentCommentCount - 1) : 0 );
                    
                    }
                    else
                    {
                        if(jQuery.inArray(commentId, seenCommentId) === -1)
                        {
                            seenCommentId.push( commentId );
                            localStorage.setItem( that.store, JSON.stringify( seenCommentId ) );
                        
                            jQuery( that.counter ).find("strong").text( (currentCommentCount - 1) > 0 ? (currentCommentCount - 1): 0 );
                        }
                    }
                },1000);
                
            });
         },
         loadCounter: function(){
             
            var eventId = jQuery(this.wrapper).attr("data-event-id");
            
            this.store = "seenComment"+eventId;
            
            var seenCommentId = JSON.parse( localStorage.getItem( this.store ) );
            
            this.totalComment = parseInt(jQuery(this.wrapper).attr("data-total-comment"));

            if( seenCommentId == null )
            {
                jQuery( this.counter ).find( "strong" ).text( (this.totalComment - 1) > 0 ? (this.totalComment - 1) : 0 );
            }
            else
            {
                var num = this.totalComment - seenCommentId.length ;
                jQuery( this.counter ).find( "strong" ).text( ( num > 0 )? num:0 );
            }
         }
         
     };
     
     jQuery(document).ready(function(){
        // commentCounter.init();
     });
     
})( jQuery );