(function ($) {

    
    jQuery(document).ready(function($){
        
        $('[data-toggle="tooltip"]').tooltip();
        
        //toggling chipcount in event page
        jQuery( '#chipcount' ).on( 'click',function(){
            var target = jQuery(this).attr( 'data-target' ); 
            jQuery(target + '> div').slideToggle();
            
            if(!jQuery(this).hasClass('fired')){
               
                jQuery( this ).addClass('fired');
                chipcountClose( target );
                
            }
            
        });
        
        var chipcountClose = function( target ){
            jQuery( '#chipcount-close' ).on( 'click',function(){
                jQuery( target + '> div' ).slideUp();
            });
        }
 
    });
    
    jQuery( window ).load(function(){
         $("#players-avatar").caroufredsel(
            {
                width : '100%',
                responsive : true,
                auto 	: false,
                circular :  true,
                items : {
                     width: 90,
                    visible : {
                        min : 1,
                        max : 12
                    }
                },
                scroll : {
                    items : 1,
                    fx : "directscroll",
                    duration: 100
                },
                swipe : {
                    onTouch : true,
                    onMouse : true
                }
            }
        );
    });

})( jQuery );


 