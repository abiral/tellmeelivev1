function liveBlog(arg){
    this.data = {'post_id':arg.id};
    this.init();
    jQuery('[data-toggle="tooltip"]').tooltip();
}

liveBlog.prototype.init = function(){
    jQuery.get('https://web-api-cyberkishor.c9.io/wp-content/themes/poker/inc/embed-events-callback.php',this.data, function( success ) {
        
        var link = document.createElement("link"), script = document.createElement("script");
        var html = jQuery('html').html();
        
        if( (html.indexOf('bootstrap.min.css') < 0) ||  (html.indexOf('bootstrap.css') < 0 ) ){
            link = document.createElement("link")
            link.rel = "stylesheet";
            link.href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css";
            jQuery('head').append(link);
        }
        
        if(html.indexOf('font-awesome') < 0){
            link = document.createElement("link")
            link.rel = "stylesheet";
            link.href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css";
            jQuery('head').append(link);
        }
        
        link = document.createElement("link")
        link.rel = "stylesheet";
        link.href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038";
        jQuery('head').append(link);

        link = document.createElement("link")
        link.rel = "stylesheet";
        link.href="https://web-api-cyberkishor.c9.io/wp-content/themes/poker/style.css";
        jQuery('head').append(link);
        

        link = document.createElement("link")
        link.rel = "stylesheet";
        link.href="https://web-api-cyberkishor.c9.io/wp-content/themes/poker/css/style.css";
        jQuery('head').append(link);
        
        jQuery('body').addClass('single-events');
        jQuery('#liveblog').html(success);
        jQuery('[data-toggle="tooltip"]').tooltip();
        jQuery("#poker-commentlist").niceScroll({cursorcolor:"#b8b8b8",boxzoom:true});
       
        jQuery('#liveblog header.entry-header .container').removeClass('container');
        jQuery('#liveblog .event-desc .container').removeClass('container');
        jQuery('#liveblog #comments .comments-title > .container').removeClass('container');
        jQuery('#liveblog').find('.poker-comment-wrapper > .container').removeClass('container');
        
    });
}

liveBlog.prototype.comment = function(){

    
}
liveBlog.prototype.likes = function(){
    
     $( document ).on( 'click',"#like a[href^='#'], #dislike a[href^='#']", function(e){
            
            e.preventDefault();
            
            var that = this,
            commentId = jQuery( this ).parents('section.poker-comment-wrapper').attr( 'data-id' ),
            ajaxurl = POKER_API.root + '/wp-json/poker-api/event/'+commentId+'/comments/',
            id = jQuery( this ).parent().attr('id'),
            alreadyLikedorDisliked = false;
                        
            jQuery('[data-toggle="popover"]').not( this ).popover('hide');
            
            if( id == 'like' )
            {
                 ajaxurl += 'like';
                 var pokerStorage = JSON.parse( localStorage.getItem( "likedComment" ) );
                 
                 if(jQuery.inArray(commentId, pokerStorage) !== -1)
                 {
                     alreadyLikedorDisliked = true;
                 }
            }
            
            if( id == 'dislike' )
            {
                 ajaxurl += 'dislike';
                
                 var pokerStorage = JSON.parse( localStorage.getItem( "dislikedComment" ) );
                 
                 if(jQuery.inArray(commentId, pokerStorage) !== -1)
                 {
                     alreadyLikedorDisliked = true;
                 }
            } 
            
            if( alreadyLikedorDisliked == false )
            {
                jQuery.ajax({
                method: "POST",
                url: ajaxurl,
                data: '',
                beforeSend: function ( xhr ) {
                     
                        
                    },
                success : function( response ) {
                    
                    if( id == 'like' )
                    {
                        var temp = [];
                        
                        temp[0] = commentId;
                        
                        var likedComment = JSON.parse( localStorage.getItem("likedComment") );
                         
                        if( null == likedComment )
                        {
                            var newLikedComment = temp;
                             
                        }
                        else
                        {
                           var newLikedComment = likedComment.concat( temp ); 
                             
                         }
                         
                         localStorage.setItem("likedComment", JSON.stringify( newLikedComment ));
                                 
                        jQuery( that ).next("span").text( response.comments_likes.total_likes );

                    }
                    if( id == 'dislike' )
                    {
                        var temp = [];
                        
                        temp[0] = commentId;
                        
                        var dislikedComment = JSON.parse( localStorage.getItem("dislikedComment") );
                         
                        if( null == dislikedComment )
                        {
                            var newdisLikedComment = temp;
                             
                        }
                        else
                        {
                             var newdisLikedComment = dislikedComment.concat( temp );
                             
                         }
                         
                         localStorage.setItem("dislikedComment", JSON.stringify( newdisLikedComment ));
                         
                        jQuery( that ).next("span").text( response.comments_dislikes.total_dislikes );
                    }
                    },
                fail : function( response ) {
                       
                    }
            });
            }else{
                
                jQuery(this).popover('show');
               
                setTimeout(function(){
                    jQuery('[data-toggle="popover"]').popover('hide');
                },3000);
                
            }
        });
}

liveBlog.prototype.emoji = function(){
}