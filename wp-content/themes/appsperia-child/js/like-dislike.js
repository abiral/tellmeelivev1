(function($) {

	var likeDislike = {

		commentId: '',
		ajaxurl: '',
		alreadyLikedorDisliked: false,
		likeStore: "likedComment",
		dislikeStore: "dislikedComment",
		wrapper: 'section.poker-comment-wrapper',
		isLikeorDislike: '',
		storeData: '',
		elem: '',
		run: function() {

			var $this = this;
			
			//  Get Real time likes or dislikes from the file
			setInterval( function(){
				jQuery.ajax({
						url : 	POKER_API.template_directory_uri + '/queue/likejson.txt',
						success : function( response ){
							var data = JSON.parse('['+response+']');
							 jQuery.each( data, function( key, val){
							 	jQuery( '#poker-commentlist > [data-id = '+val.comment_id+']').find( "#like span").text(val.total_likes);
							 	jQuery( '#poker-commentlist > [data-id = '+val.comment_id+']').find( "#dislike span").text(val.total_dislikes);
							 });
						},
						cache : false
					});
			},5000);
			
			
			jQuery(document).on( 'click', "#like a[href^='#'], #dislike a[href^='#']", function(e) {

				e.preventDefault();
				$this.elem = this;
                $this.alreadyLikedorDisliked = false;
				$this.init();

				if ($this.alreadyLikedorDisliked == false) {
					jQuery.ajax({
						method: "POST",
						url: $this.ajaxurl,
						data: '',
						beforeSend: function(xhr) {
							if( $this.isLikeorDislike == 'like' ){
								
							}
						},
						success: function(response) {
							$this.setResponse(response);
						},
						fail: function(response) {}
					});
				} else {

					jQuery(this).popover('show');

					setTimeout(function() {
						jQuery('[data-toggle="popover"]').popover('hide');
					}, 5000);
				}

			});
		},
		init: function() {

			this.commentId = jQuery(this.elem).parents(this.wrapper).attr('data-id').toString();
			
			this.ajaxurl = POKER_API.root + '/wp-json/poker-api/event/' + this.commentId + '/comments/';

			this.isLikeorDislike = jQuery(this.elem).parent().attr('id');

			jQuery('[data-toggle="popover"]').not(this.elem).popover('hide');
            
			if (this.isLikeorDislike == 'like') {
				this.ajaxurl += 'like';

				this.storeData = JSON.parse(localStorage.getItem(this.likeStore));
				var disLikeStoreDta = JSON.parse(localStorage.getItem(this.dislikeStore));

				if ( jQuery.inArray(this.commentId, this.storeData) != -1 || jQuery.inArray(this.commentId, disLikeStoreDta) != -1 ) {
					this.alreadyLikedorDisliked = true;
				}
				
			} else if (this.isLikeorDislike == 'dislike') {
				this.ajaxurl += 'dislike';

				this.storeData = JSON.parse(localStorage.getItem(this.dislikeStore));
				var likeStoreData = JSON.parse(localStorage.getItem(this.likeStore));

				if (jQuery.inArray(this.commentId, this.storeData) != -1 || jQuery.inArray(this.commentId, likeStoreData) != -1) {
					this.alreadyLikedorDisliked = true;
				}
			}
		},
		setResponse: function(response) {
		    
		    if( response.status == "200" ){
    			if (this.isLikeorDislike == 'like') {
    				var temp = [];
    
    				temp[0] = this.commentId;
    
    				var likedComment = JSON.parse(localStorage.getItem(this.likeStore));
    
    				if (null == likedComment) {
    					var newLikedComment = temp;
    				} else {
    					var newLikedComment = likedComment.concat(temp);
    				}
    
    				localStorage.setItem(this.likeStore, JSON.stringify(newLikedComment));
    
    				jQuery(this.elem).next("span").text(response.data.total_likes);
    
    			} else if (this.isLikeorDislike == 'dislike') {
    				var temp = [];
    
    				temp[0] = this.commentId;
    
    				var dislikedComment = JSON.parse(localStorage.getItem(this.dislikeStore));
    
    				if (null == dislikedComment) {
    					var newdisLikedComment = temp;
    
    				} else {
    					var newdisLikedComment = dislikedComment.concat(temp);
    
    				}
    
    				localStorage.setItem(this.dislikeStore, JSON.stringify(newdisLikedComment));
    
    				jQuery(this.elem).next("span").text(response.data.total_dislikes);
    			}
		    }
		}
	};
	
	jQuery( document ).ready( function(){
	    likeDislike.run();
	});
	

})(jQuery);