(function( $ ){
    //returns true if user's tab is active..
    var vis = (function(){
        var stateKey, eventKey, keys = {
            hidden: "visibilitychange",
            webkitHidden: "webkitvisibilitychange",
            mozHidden: "mozvisibilitychange",
            msHidden: "msvisibilitychange"
        };
        for (stateKey in keys) {
            if (stateKey in document) {
                eventKey = keys[stateKey];
                break;
            }
        }
        return function(c) {
            if (c) document.addEventListener(eventKey, c);
            return !document[stateKey];
        }
    })();

   function ajaxComment( arg ){
       
        this.form = arg.form,
        this.commentId = arg.commentId,
        this.nameId = arg.nameId,
        this.postId = arg.postId,
        this.userId  = arg.userId,
        this.btnId  = arg.btnId,
        this.file   = arg.file,
        this.commentListId = arg.commentListId,
        this.loader = ' <i id="pokerLoader" class="fa fa-spinner fa-spin"></i>',
        this.commentWrapper = arg.commentWrapper,
        this.commentIdArr = [],
        this.commentCookie = '',
        this.totalComment = '',
        this.eventId = '',
        this.counter = arg.counter,
        this.run();
        
    }
    
    ajaxComment.prototype.getSeenComment = function(){
        
        var seenCommentId = JSON.parse( localStorage.getItem( this.commentCookie ) );
        
        if( typeof seenCommentId == "undefined" || seenCommentId == null ){
            return false;
        }
        
        return seenCommentId;
    }
        
    ajaxComment.prototype.run = function (){

        this.initCounter();
        this.runCounter();
        
        var that = this;
        var seenCommentId = this.getSeenComment();
        
        jQuery( this.commentWrapper ).each(function( key, val ){
            
            var dataID = jQuery( this ).attr( "data-id" );
            
            if( jQuery.inArray( dataID, seenCommentId ) == -1 ){
                //This is unseen comment
                jQuery( this ).addClass( "unseen" );
                jQuery( this ).removeClass( "seen" );
            }else{
                jQuery( this ).addClass( "seen" );
                jQuery( this ).removeClass( "unseen" );
            }
            
        });
        
        //Submit form on enter key press
        
        $(this.form + ' textarea').keydown(function(e) {
            if (e.keyCode == 13) {
                $(that.form).submit();
            }
        });
        
        // Pushing all existing comment IDs and run realtime comment 
        
        jQuery(this.commentWrapper).each(function(){
            
            var ID = jQuery(this).attr( 'data-id' ); 
            that.commentIdArr.push(ID);
            
        }).promise().done( function(){
            
            that.realTimeComment();
            
        });
        
        // Toggle Button on Key press
        jQuery( this.commentId + " ," + this.nameId ).on('keyup',function(){
            
           that.toggleButton();
          
        });
        
        jQuery(".poker-remove-pic").on("click",function(e){
            e.stopPropagation();
            e.preventDefault();
            that.removeFile();
            
            var name = jQuery(that.nameId).val();
            var comment = jQuery(that.commentId).val();
            
            if( name == "" || comment == "" ){
                that.disableBtn();
            }
        })
        
        $(this.file).on("change",function(event){
            
            var fakeFilePath = $(this).val();
            var ext = fakeFilePath.substr( (fakeFilePath.lastIndexOf('.') +1) );
            var validExt = ["jpg","gif","png","jpeg","JPG","PNG"];
            
            if(jQuery.inArray(ext, validExt) == -1){
                //if Invalid file type
                
                that.removeFile();
                that.showError( "Invalid File Type." );
                
            }else{
                
                var  tmppath = URL.createObjectURL(event.target.files[0]); 
                var markUp = '<img src="'+tmppath+'" alt="">';
            
                $( that.commentId ).animate({paddingLeft: 30 });
                
                $('#uploaded-image').html(markUp);
                jQuery(".uploaded-image").show().animate({opacity:1},1000);

            }
            
            that.toggleButton();
   
        });
        
        jQuery(this.form).submit( function(e){
                
            e.preventDefault();
           
            that.data =  {
                'comment_post_ID':jQuery(that.postId).val(),
                'comment_author': jQuery(that.nameId).val(),
                'comment_content': jQuery(that.commentId).val(),
                'user_id'        : jQuery(that.userId).val()
            };
            
            if( that.validate() ){
                
                that.activateLoader();
            
                var afterUpload = function( response  ){
                    
                    if( response.status == 200 ){
                            
                        that.updateCommentCounter( response );
                        
                        that.commentIdArr.push( response.data.comment_ID);
    
                        var commentHtml = that.getCommentList( response.data );
                        
                        jQuery( that.commentListId ).prepend( commentHtml ).scrollTop(0);
                        
                        if(that.userId == 0)
                        {
                            jQuery( that.nameId ).val( '' );
                        }
                        
                        jQuery( that.commentId ).val( '' );
                        
                        that.disableBtn();
                        
                        that.removeFile();
                    }else{
                        that.showError( "Commenting Failed." );
                    }
                    
                    that.deactivateLoader();
                }

                jQuery(that.file).upload(POKER_API.root + '/wp-json/poker-api/event/comments', that.data, afterUpload );
   
                return false;
            }else{
                that.showError( "Missing required Field." );
            }
            
            that.toggleButton();
 
        });
  
    }
    
    ajaxComment.prototype.validate = function( data ){
        
        var name = jQuery( this.nameId ).val();
        var comment = jQuery( this.commentId ).val();
        var file = jQuery( this.file ).val();
        
        if( name == "" || ( comment == "" && file == "" ) )
            return false;
        
        if( comment !== "" || file !== "" )
            return true;
            
        return true;    
    }
    
    ajaxComment.prototype.toggleButton = function(){
        
        if( this.validate() ){
            this.enableBtn();
        }else{
            this.disableBtn();
        }
        
    }
    
    ajaxComment.prototype.initCounter = function(){
        
        var eventId = jQuery(this.commentListId).attr("data-event-id");
        this.eventId = eventId;
            
        this.commentCookie = "seenComment"+eventId;
        
        var seenCommentId = this.getSeenComment();
        
        this.totalComment = parseInt(jQuery(this.commentListId).attr("data-total-comment"));

        if( seenCommentId )
        {
            var num = this.totalComment - seenCommentId.length ;
            jQuery( this.counter ).find( "strong" ).text( ( num > 0 )? num:0 );
            
        }
        else
        {
            jQuery( this.counter ).find( "strong" ).text( (this.totalComment - 1) > 0 ? (this.totalComment - 1) : 0 );
        }
    }
    
    ajaxComment.prototype.runCounter = function(){
        var that = this;
            
        jQuery(document).on("inview","#comments .poker-comment-wrapper", function() {
            
            var $this = jQuery(this),
            
             seenCommentId = JSON.parse( localStorage.getItem( that.commentCookie ) ),
    
             commentId= $this.attr('data-id'),
             
             temp = [],
             
             currentCommentCount = parseInt( jQuery(that.counter).find("strong").text() );
             
             if( !$this.hasClass("seen")){
                 //Remove unseen and add seen
                 $this.delay(1000).switchClass( "unseen", "seen", 1000, "easeInOutQuad" );
                
             }
             
             temp.push( commentId );

            if( typeof seenCommentId == "undefined" || null == seenCommentId )
            {
                localStorage.setItem( that.commentCookie, JSON.stringify( temp ) );
            
                jQuery( that.counter ).find("strong").text( (currentCommentCount - 1) > 0 ? (currentCommentCount - 1) : 0 );
            
            }
            else
            {
                if(jQuery.inArray(commentId, seenCommentId) === -1)
                {
                    seenCommentId.push( commentId );
                    localStorage.setItem( that.commentCookie, JSON.stringify( seenCommentId ) );
                
                    jQuery( that.counter ).find("strong").text( (currentCommentCount - 1) > 0 ? (currentCommentCount - 1): 0 );
                }
            }

        });
    }
    
    ajaxComment.prototype.activateLoader =  function(){
        jQuery('#commentError').remove();
        this.disableBtn();
        jQuery( this.btnId ).html(this.loader);
    }
    
    ajaxComment.prototype.enableBtn = function(){
        $(this.btnId).removeClass('disabled');
        $(this.btnId).prop('disabled',false);
    }
    
    ajaxComment.prototype.disableBtn = function(){
       $(this.btnId).addClass('disabled');
       $(this.btnId).prop('disabled',true);
    }
    
    ajaxComment.prototype.deactivateLoader = function(){

        jQuery( this.btnId ).html( "<i class='fa fa-send-o'></i>" );
        
    }
    
    ajaxComment.prototype.realTimeComment = function(){
        
        var that =  this;
          
        setInterval(function(){
            
            var latestTime = jQuery( that.commentListId + ' > section').first().attr( "data-time" );
            if( typeof latestTime !== "undefined" ){
                jQuery.ajax({
                url : POKER_API.root + '/wp-json/poker-api/event/'+that.eventId+'/after/'+latestTime+'/comments',
                cache : false,
                success : function( response ){
                     if( response.data !== null)
                     {
                        jQuery.each( response.data, function( key, val){
                            
                          if( !that.commentExist( val.comment_ID ) ){
                              
                              that.commentIdArr.push( val.comment_ID );
                              var comment = that.getCommentList( val );
                              jQuery( that.commentListId ).prepend( comment );
                              
                              var comment_count = parseInt(jQuery(that.commentListId).attr("data-total-comment"));
                              
                              jQuery( that.commentListId ).attr( "data-total-comment", comment_count + 1);
                              that.initCounter();
                              
                              var visible = vis(); 
                             
                              if( !visible ){
                                var counterVal = parseInt( jQuery( that.counter ).find( "strong" ).text() );
                                var message = '1 Comment from '+that.capitalize(val.comment_author);
                                $.titleAlert( counterVal + 1 + ' New Message', { interval:1000 } );
                                tellUser( message, val.comment_content, that.getAvatar(val.user_avatar,true) );
                                
                              }
                              
                              $.playSound(POKER_API.template_directory_uri + '/js/vendor/notification/sound/notify');
                          }
                          
                        });
                    }
                }
            });
            }
        },5000);
    }
    
    ajaxComment.prototype.commentExist =  function( ID ){
        
        if(jQuery.inArray( ID, this.commentIdArr ) == -1){
            return false;
        }
        
        return true;
    }
    
    ajaxComment.prototype.getAvatar = function( profilePic, url ){
        if( typeof url != "undefined" ){
            if( !profilePic)
                return POKER_API.template_directory_uri + '/images/anonymous.png';
            else
                return profilePic;
        }
        
        if( !profilePic){
            return '<img class="img-responsive" src="'+POKER_API.template_directory_uri + '/images/anonymous.png" alt="" >';
        }
   
        return '<img class=" avatar  avatar-32  photo user-1-avatar" src="'+profilePic+'" alt="" height="32" width="32" />';
            
    }
    ajaxComment.prototype.getCommentList = function ( response ){
        
        var month = [ "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" ];
          
        var  now = new Date( response.comment_date_gmt * 1000 );
        
        var date = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
        
        var  formattedDate  = month[ date.getMonth() ] + ' ' + date.getDate() + ', ' + this.n( date.getHours() )+":"+this.n( date.getMinutes() );
        
        var anonymousClass = '';
        var profilePic = response.user_avatar;
        
        var temp = '';
       
        if( response.assoicated_user == false ){
            anonymousClass = 'anonymous';
            temp = "Comment from ";
        }
        
        var author = '';
        if(response.display_name == ''){
            author = temp + this.ucwords(response.comment_author);
        }else{
             author = temp + this.ucwords(response.display_name);
        }
        
        
        var temp = '<section data-time="'+response.comment_date_gmt+'" data-id="'+response.comment_ID+'" class="poker-comment-wrapper '+anonymousClass+'">';
            temp += '<div class="container">';
            temp += '<div class="commenter-pic">'+this.getAvatar(response.user_avatar)+'</div>';
            temp += '<div class="commenter-info">';
            temp += '<div class="name"><span class="author">'+author+'</span><span class="date">'+formattedDate+'</span></div>';
			temp += '<div class="comment"><p>'+response.comment_content+'</p></div>';
			if( response.comment_image != '' ){
			    temp += '<div class="comment-image"><img src="'+response.comment_image+'" alt="comment"></div>';
			}
			temp += '<div class="comment-like-dislike">';
			temp += '<p id="like"><a href="#" data-toggle="popover" data-content="You have already liked/disliked this comment" data-placement="top"><i class="fa fa-thumbs-up"></i></a><span>0</span></p>';
			temp += '<p id="dislike"><a href="#" data-toggle="popover" data-content="You have already liked/disliked this comment" data-placement="top" ><i class="fa fa-thumbs-down"></i></a><span>0</span></p>';
			temp += '</div>';
			temp += '</div>';
			temp += '</section>';
			
			return temp;
    }
    
    ajaxComment.prototype.n = function ( n ){
        return n > 9 ? "" + n: "0" + n;
    }
    
    ajaxComment.prototype.ucwords = function( str ){
        
        str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        
        return (str); 
                
    }
    
    ajaxComment.prototype.capitalize = function( string ){
        var str = string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        return str;
    }
    
    ajaxComment.prototype.showError = function ( message ){
        
        this.deactivateLoader();
       
        var errorMsg;
        
        errorMsg = (typeof message !== 'undefined') ? message : POKER_API.comment_errors.default;
        
        jQuery( this.commentId ).parent( 'div' ).before( '<div id="commentError" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+errorMsg+'</div>' );
        
    }

    ajaxComment.prototype.updateCommentCounter = function( response ){
   
        var seenCommentId = JSON.parse( localStorage.getItem( "seenComment"+this.data.comment_post_ID) );
                                                                                      
        var temp = [];
        response.data = response.data[0];
        var commentId = response.data.comment_ID.toString();
        
        var updatedSeenComment = '';
        
        temp.push(commentId);
        
        if( null == seenCommentId )
        {
             updatedSeenComment = temp;
        }
        else
        {
            if(jQuery.inArray( commentId, seenCommentId ) === -1){
                
               seenCommentId.push( commentId );
               
               updatedSeenComment = seenCommentId;
 
            }
        }
         
        localStorage.setItem('seenComment'+this.data.comment_post_ID, JSON.stringify(updatedSeenComment));
    }
    
    ajaxComment.prototype.removeFile = function(data){
        $(this.file).val(null);
        
        $('.uploaded-image').fadeOut(500,function(){
             $("#uploaded-image img").remove();
        });
        
        $(this.commentId).animate({paddingLeft:5});
       
    }

    var adjustCommentWrapperHeight = function(){
            
        var wh = jQuery(window).height();
        var commentWrapperPos = jQuery( "#poker-commentlist" ).offset().top;
        
        var height =  wh - commentWrapperPos - jQuery( ".event-section-2" ).height();
        
        jQuery( "#poker-commentlist" ).css({height : height - 10});
    }
    
    $(document).ready(function(){

        $( '#comment-smiley' ).on( 'click', function(e){
               e.preventDefault();
               $( '.poker-emoticons' ).fadeToggle();
        });
            
        $( '#comment-camera' ).on("click",function(e){
            e.preventDefault();
            var target = $(this).attr("data-trigger");
            
            var userId = $( '#user-id' ).val();
            if( userId !== '0' ){
                $(target).trigger("click");
            $(target).css('filter','alpha(opacity = 0');
                $(target).trigger("click");
            $(target).css('filter','alpha(opacity = 0');
            }
        });
        
        adjustCommentWrapperHeight();
        
        var niceScrollConfig = {
    
            cursorcolor:"#b8b8b8",
            cursorwidth: "10px"
            
        } 
        
        $("#poker-commentlist").niceScroll( niceScrollConfig );
        $(".chipcount-user-wrapper").niceScroll( niceScrollConfig );
    });
    
    $(window).load( function(){
        
        var comment = new ajaxComment({ 
                form: '#poker-comment-form',
                btnId: '#submit', 
                postId: '#comment_post_ID', 
                commentId: '#poker-comment', 
                nameId: '#poker-author',
                userId: '#user-id',
                commentListId: "#poker-commentlist",
                file  : '#comment-image',
                commentWrapper : ".poker-comment-wrapper",
                counter : "#comment-counter"
        });
    });
    
    $(window).resize(function(){
        adjustCommentWrapperHeight();
    });
     
}( jQuery ));