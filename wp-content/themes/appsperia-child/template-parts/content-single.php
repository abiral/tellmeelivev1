<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Poker
 */
 	$event_id = get_the_ID();
   	$event_created_date = get_the_time( 'Y/m/d' );
 	
	$post_meta = get_post_meta( $event_id, 'event_player',true );
	
	if( is_array( $post_meta ) ):
		$players_count = count( $post_meta );
	else:
		$players_count = 0;
	endif;
	
	$event_start_date = get_post_meta( $event_id, 'event_start_date', true );
	$event_start_date = strtotime($event_start_date);           
	$event_start_date = date('m/d', $event_start_date);

  	$event_end_date = get_post_meta( $event_id, 'event_end_date', true );
  	$event_end_date = strtotime($event_end_date);           
	$event_end_date = date('m/d', $event_end_date);
	$chipcount_img = get_stylesheet_directory_uri().'/images/chipcount-img.png';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title"><div class="container"><i class="fa fa-comments"></i>', '</div></h1>' ); ?>
		<div class="entry-meta">
			<div id="chipcount-wrapper">
				<div>
					<?php
						$start_stack = get_post_meta( $event_id, 'chipcount',true );
						if($start_stack):
					?>
					<div class="chipcount-user">
						<div class="chipcount-user-pic"></div>
						<div class="chipcount-user-about">
							<div class="chipcount-user-name">STARTSTACK <span class="user-chipcount"><?php echo $start_stack; ?></span></div>
							<div class="chipcount-date"><?php echo $event_created_date; ?></div>
						</div>
					</div>
					<?php endif; ?>
					
					<div class="chipcount-title"> <img src="<?php echo $chipcount_img; ?>" alt="" ><?php _e( 'CURRENT CHIPSCOUNT','poker' );  ?></div>
					<div class="chipcount-user-wrapper">
						<?php 
							$chipcounts = get_chipcount_by_event_id( $event_id ); 
							if( $chipcounts ):
								foreach( $chipcounts as $chip ):
						?>
								<div class="chipcount-user">
									<div class="chipcount-user-pic"><?php echo $chip['avatar']; ?></div>
									<div class="chipcount-user-about">
										<div class="chipcount-user-name"><?php echo $chip['name']; ?> <span class="user-chipcount"><?php echo $chip['chipcount']; ?></span></div>
										<div class="chipcount-date">
											<?php 
											$timestamp = get_user_meta($chip['user_id'], 'chipcount_last_modified', true); 
											if($timestamp){
												echo date('Y/m/d', $timestamp);;
											}
											?>
											</div>
									</div>
									
								</div>
						<?php
								endforeach;
							endif; 
						?>
					</div>
					<div class="chipcount-close"><button id="chipcount-close" class="bt btn-chipcount-close"><?php _e('CLOSE','poker'); ?></button></div>
				</div>
			</div>
			<div class="container">
				<span> <img class="poker-icon-calendar" src="<?php echo get_stylesheet_directory_uri().'/images/calendar.png'; ?>" alt="" > <?php echo $event_start_date; ?> - <?php echo $event_end_date; ?></span>
				<span> <img class="poker-icon-user" src="<?php echo get_stylesheet_directory_uri().'/images/user.png'; ?>" alt="" ><?php echo $players_count; ?> PLAYERS</span>
				<span id="chipcount" data-target="#chipcount-wrapper"> 
					<img class="poker-icon-chip" src="<?php echo get_stylesheet_directory_uri().'/images/chip.png'; ?>" alt="" > <a href="#" >CHIPSCOUNT</a>
					
				</span>
			</div><!-- /container -->
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="event-section-1">
	<div class="entry-content">
		<div class="event-desc">
			<div class="container">
				<div class="info-icon">
					<i class="fa fa-info"></i>
				</div>
				<div class="info-excerpt">
					<?php the_excerpt(); ?>
				</div>
			</div>
		</div>
			
		<div class="player-avatar-wrapper">
			<div class="container">
			<?php
				//Listing Players
	  
			    if( is_array( $post_meta ) ):	
			    	echo '<ul class="players-avatar" id="players-avatar" >';
			    	foreach( $post_meta as $user_id ):
			    		$user = get_user_by( 'id',$user_id );
			    		$profile_img = get_avatar( $user_id );
			    		echo '<li data-toggle="tooltip" data-placement="right" title="'.ucfirst($user->display_name).'" ><a href="#" >'.$profile_img.'<span class="hidden ava-name">'.ucfirst($user->display_name).'</span></a></li>';
			    	 endforeach;
			    	echo '</ul>';
			    endif;	
			?>
			</div><!-- /container -->
		</div><!-- /player-avatar-wrapper -->
		
	</div><!-- .entry-content -->